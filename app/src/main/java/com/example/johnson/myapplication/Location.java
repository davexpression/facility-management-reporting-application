package com.example.johnson.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

public class Location extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    private  Spinner spinner;
    private  EditText editText;
    private  String selectedTextPosition;


    private static final String[] floor = {"Select Office Floor", "Ground Floor", "1st Floor", "2nd Floor",
            "3rd Floor", "Lecture Theater Area"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        spinner = (Spinner) findViewById(R.id.location_floor);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(Location.this,
                android.R.layout.simple_spinner_item, floor){
            @Override
            public boolean isEnabled(int position){
                if(position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    return true;
                }
            }
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(this);

    }


    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {

        String selectedItemText = (String) parent.getItemAtPosition(position);

        if(position > 0){
            selectedTextPosition = Integer.toString(position);
        }
    }

    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void location_submit (View view) {
        Intent intent = new Intent(this,HomeActivity.class);

        SharedPreferences.Editor editor =
                PreferenceManager.getDefaultSharedPreferences(this).edit();

        EditText editText = (EditText) findViewById(R.id.location_identifier);
        String identifier = editText.getText().toString();
        editor.putString("office_floor", selectedTextPosition);
        editor.putString("office_identifier", identifier);
        editor.commit();

        startActivity(intent);
    }

}
