package com.example.johnson.myapplication;

import android.*;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class TrendingDetail extends AppCompatActivity {

//    private  AlertDialog.Builder builder;
    TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trending_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        builder = new AlertDialog.Builder(this);

        Intent intent = getIntent();

        String complaint = intent.getStringExtra("complaint");
        textView = (TextView) findViewById(R.id.mo_complaint_header);
        textView.setText(complaint);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            super.onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

//    public void join_in_complaining (View view) {
//        builder.setTitle("Confirm");
//        builder.setMessage("You are about to join in complaining");
//        builder.setCancelable(false);
//        builder.setPositiveButton("OK",
//                new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int id) {
////                        Intent intent = new Intent(TrendingDetail.this, HomeActivity.class);
////                        intent.putExtra("startFrag", true);
////
////                        Toast.makeText(TrendingDetail.this, "Compliant Joined",
////                                Toast.LENGTH_SHORT).show();
////
////                        startActivity(intent);
//
//                    }
//                });
//        builder.setNegativeButton("Cancel",
//                new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int id) {
//                        // code to run when Cancel is pressed
//                    }
//                });
//
//        AlertDialog alert = builder.create();
//        alert.show();
//    }


}
