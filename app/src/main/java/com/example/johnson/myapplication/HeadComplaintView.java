package com.example.johnson.myapplication;


import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class HeadComplaintView extends Fragment {


    Button noted;
    private DatabaseReference firebaseDatabase;
    String last_name, fragment;

    public HeadComplaintView() {
        // Required empty public constructor
    }

    public interface OnFragmentInteractionListener {
        public void showDrawerToggle(boolean showDrawerToggle, String lname, String fragment);
    }

    private OnFragmentInteractionListener mListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mListener.showDrawerToggle(false, last_name, fragment);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_head_complaint_view, container, false);
        firebaseDatabase = FirebaseDatabase.getInstance().getReference();
        final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.noted_progress);

        final String complaint = getArguments().getString("complaint");
        final String complaint_id = getArguments().getString("complaint_id");
        final String loc = getArguments().getString("complaint_loc");
        final String name = getArguments().getString("complaint_name");
        final String total = getArguments().getString("complaint_num");
        final String date = getArguments().getString("date");
        final String remarks = getArguments().getString("remark");

        noted = (Button) view.findViewById(R.id.noted_button);

        Date myDate = null;
//

        try {
            java.text.DateFormat format = new SimpleDateFormat("EEE LLL dd HH:mm:ss Z yyyy");
            myDate = format.parse(date);
        }
        catch (ParseException e){ }
        //this code gets references to objects in the listview_row.xml file

        String string = DateUtils.getRelativeDateTimeString(getActivity(), myDate.getTime(), DateUtils.MINUTE_IN_MILLIS,
                DateUtils.DAY_IN_MILLIS * 4, DateUtils.FORMAT_ABBREV_RELATIVE).toString();

        string = string.substring(0, string.indexOf(","));


        last_name = getArguments().getString("last_name");
        fragment = getArguments().getString("fragment");

        final String student = Integer.toString(getArguments().getInt("student"));
        final String staff = Integer.toString(getArguments().getInt("staff"));

        boolean fixed_status = getArguments().getBoolean("fixed");
        boolean noted_status = getArguments().getBoolean("noted");

        TextView textView = (TextView) view.findViewById(R.id.hos_complaint_description);
        TextView textView_remarks = (TextView) view.findViewById(R.id.hos_complaint_remarks);
        TextView textView1 = (TextView) view.findViewById(R.id.hos_complaint_view_loc);
        TextView textView2 = (TextView) view.findViewById(R.id.hos_complaint_view_name);

        TextView total_text = (TextView) view.findViewById(R.id.join_total);
//        TextView student_text = (TextView) view.findViewById(R.id.join_student);
//        TextView staff_text = (TextView) view.findViewById(R.id.join_staff);

        TextView textView_date = (TextView) view.findViewById(R.id.detail_date);

        if (fragment.equals("fixed")) {
            noted.setText("Fixed");
            noted.setBackgroundColor(Color.parseColor("#d6d6fd"));
        }

        else if (fragment.equals("most"))
            noted.setVisibility(View.GONE);

//        if (fragment.equals("most"))  {
//            student_text.setVisibility(View.VISIBLE);
//            staff_text.setVisibility(View.VISIBLE);
//            total_text.setText("Total:" + total + " -->");
//            student_text.setText("Student:" + student);
//            staff_text.setText("Staff:" + staff);
//        }

//        else {
//            student_text.setVisibility(View.GONE);
//            staff_text.setVisibility(View.GONE);

            if (Integer.parseInt(total) > 1)
                total_text.setText("Total:" + total);
            else
                total_text.setText("None");
//        }

        textView.setText(complaint);

        if (remarks.equals("null"))
            textView_remarks.setText("none");
        else
            textView_remarks.setText(remarks);

        textView1.setText(loc);
        textView2.setText(name);
        textView_date.setText(string);


        firebaseDatabase.child("complaints").child(complaint_id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                    Complaint complaint1 = dataSnapshot.getValue(Complaint.class);

                    if (complaint1.noted_by_hos == true) {
                        noted.setBackgroundColor(Color.parseColor("#d6d6fd"));

                    }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

//        if (noted_status == true) {
//            //                    imageView1.setImageTintMode();
//            noted.setBackgroundColor(Color.parseColor("#d6d6fd"));
//        }


        noted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressBar.setVisibility(View.VISIBLE);
                firebaseDatabase.child("complaints").child(complaint_id)
                        .child("noted_by_hos").setValue(true).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        progressBar.setVisibility(View.GONE);
                    }
                });

            }

        });


        return view;
    }



    public void closeFrag() {

//        Toast.makeText(getActivity(), "Authentication Successful.",
//        Toast.LENGTH_SHORT).show();

        HeadComplaintView headComplaintView = (HeadComplaintView) getFragmentManager().findFragmentByTag("view");

        getFragmentManager().beginTransaction()
                //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                .remove(headComplaintView)
                .commit();
    }
}
