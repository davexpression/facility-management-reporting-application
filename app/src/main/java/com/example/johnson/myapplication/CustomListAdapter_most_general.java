package com.example.johnson.myapplication;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Johnson on 15/04/2018.
 */

public class CustomListAdapter_most_general extends ArrayAdapter <ObjectItem2> {

    //to reference the Activity
    private final Activity context;

    private ArrayList<ObjectItem2> data = null;

    public CustomListAdapter_most_general(Activity context, int layout, ArrayList<ObjectItem2> data) {

        super(context, layout, data);

        this.context = context;
        this.data = data;

    }

    public View getView(final int position,  View view, ViewGroup parent) {


        if (view == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            view = inflater.inflate(R.layout.most_lodged_complaints_original, parent, false);
        }


        //this code sets the values of the objects to values from the arrays
        final ObjectItem2 objectItem = data.get(position);

        //this code gets references to objects in the listview_row.xml file

        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.most_lodged_staff);
        TextView headerTextField = (TextView) view.findViewById(R.id.list_header);
        TextView numberTextField = (TextView) view.findViewById(R.id.trending_number);
        TextView locationField = (TextView) view.findViewById(R.id.location_text);

        TextView staff = (TextView) view.findViewById(R.id.trending_lecturer_number);
        TextView student = (TextView) view.findViewById(R.id.trending_student_number);


//        final TextView textView = (TextView) view.findViewById(R.id.my_complaints_list_detail2);
//
//
//        if(viewVisibility[position]) {
//            textView.setVisibility(View.VISIBLE);
//        } else {
//            textView.setVisibility(View.GONE);
//        }
//
//        ImageView imageView = (ImageView) view.findViewById(R.id.my_complaints_expand);

        headerTextField.setText(objectItem.facility + " at " + objectItem.location_floor + "  |  " + objectItem.location_text);
        locationField.setText(objectItem.complaint_nature);

        numberTextField.setText(Integer.toString(objectItem.complaint_number));
        staff.setText(Integer.toString(objectItem.staff));
        student.setText(Integer.toString(objectItem.student));

//        if (objectItem.staff == 0)
//            linearLayout.setVisibility(View.INVISIBLE);
//        else
//            linearLayout.setVisibility(View.VISIBLE);


//        imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                    textView.setVisibility(View.VISIBLE);
//                    viewVisibility[position] = true;
//                }
//        });
        return view;

    };
}
