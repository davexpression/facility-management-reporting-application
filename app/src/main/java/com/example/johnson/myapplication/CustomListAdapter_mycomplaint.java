package com.example.johnson.myapplication;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Johnson on 15/04/2018.
 */

public class CustomListAdapter_mycomplaint extends ArrayAdapter <ObjectItem2> {

    //to reference the Activity
    private final Activity context;

    private ArrayList<ObjectItem2> data = null;
    private boolean[] viewVisibility = null;


    public CustomListAdapter_mycomplaint(Activity context, int layout, ArrayList<ObjectItem2> data) {

        super(context, layout, data);

        this.context = context;
        this.data = data;
        this.viewVisibility = new boolean[data.size()];

    }

    public View getView(final int position,  View view, ViewGroup parent) {


        if (view == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            view = inflater.inflate(R.layout.my_complaints_original, parent, false);
        }


        //this code sets the values of the objects to values from the arrays
        final ObjectItem2 objectItem = data.get(position);

        //this code gets references to objects in the listview_row.xml file


        FacilityColor facilityColor = new FacilityColor();
        final Map<String, String> facilityMap = facilityColor.getFacilityMap();


        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.text_background);
        ImageView imageView = (ImageView) view.findViewById(R.id.complaint_image);
        TextView headerTextField = (TextView) view.findViewById(R.id.my_complaints_list_header);
        TextView numberTextField = (TextView) view.findViewById(R.id.my_complaints_number);
        TextView locationField = (TextView) view.findViewById(R.id.my_complaints_location_text);

        TextView textView_joined = (TextView) view.findViewById(R.id.user_joined);
//        final TextView textView = (TextView) view.findViewById(R.id.my_complaints_list_detail2);
//
//
//        if(viewVisibility[position]) {
//            textView.setVisibility(View.VISIBLE);
//        } else {
//            textView.setVisibility(View.GONE);
//        }
//
//        ImageView imageView = (ImageView) view.findViewById(R.id.my_complaints_expand);

        if (objectItem.new_facility == false) {

            imageView.setVisibility(View.VISIBLE);
            imageView.setImageResource(objectItem.getImg_id());
            GradientDrawable drawable = (GradientDrawable) linearLayout.getBackground();
            drawable.setStroke(1, Color.parseColor(facilityMap.get(objectItem.facility))); // set stroke width and stroke color
        }

        else if (objectItem.new_facility == true) {
            imageView.setVisibility(View.INVISIBLE);
            linearLayout.setBackgroundResource(R.drawable.copyborder4_one);
        }

        headerTextField.setText(objectItem.complaint_nature);
        numberTextField.setText(objectItem.join_total);
        locationField.setText(objectItem.location_text);


        if (objectItem.joined == true) {
            textView_joined.setVisibility(View.VISIBLE);
        }
        else
            textView_joined.setVisibility(View.GONE);

//        imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                    textView.setVisibility(View.VISIBLE);
//                    viewVisibility[position] = true;
//                }
//        });
        return view;

    };
}
