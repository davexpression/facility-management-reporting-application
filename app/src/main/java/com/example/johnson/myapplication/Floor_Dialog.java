package com.example.johnson.myapplication;


import android.os.Bundle;
import android.app.DialogFragment;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class Floor_Dialog extends DialogFragment {


    public Floor_Dialog() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View dialog = inflater.inflate(R.layout.fragment_floor__dialog,
                container, false);
            return dialog;

        }

}
