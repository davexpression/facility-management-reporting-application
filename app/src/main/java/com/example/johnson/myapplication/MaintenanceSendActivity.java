package com.example.johnson.myapplication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class MaintenanceSendActivity extends AppCompatActivity {

    private  AlertDialog.Builder builder;
    EditText editText;
    View hideKeyboard;
    TextView textView, textView1, textView2;
    ImageView create, enter;
    private DatabaseReference firebaseDatabase;
    private ProgressDialog progressDialog;
    String complaint, complaint_id, name, loc, join, type;
    boolean new_facility;
    LayoutInflater layoutInflater;

    HashMap<String, Object> map;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maintenance_send);

        Toolbar toolbar = (Toolbar) findViewById(R.id.mo_toolbar_complaint_send);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        layoutInflater = getLayoutInflater();

        Intent intent = getIntent();
        complaint = intent.getStringExtra("complaints");
        complaint_id = intent.getStringExtra("complaints_id");
        loc = intent.getStringExtra("loc");
        name = intent.getStringExtra("name");
        join = intent.getStringExtra("join");
        new_facility = intent.getBooleanExtra("new", false);

        textView1 = (TextView) findViewById(R.id.mo_complaint_description);
        textView = (TextView) findViewById(R.id.mo_toobar_remark);
        textView2 = (TextView) findViewById(R.id.mo_toobar_submit);

        TextView textView_loc, textView_name, textView_join;
        textView_loc = (TextView) findViewById(R.id.send_loc);
        textView_name = (TextView) findViewById(R.id.send_name);
        textView_join = (TextView) findViewById(R.id.send_total);

        create = (ImageView) findViewById(R.id.mo_complaint_enter);
        enter = (ImageView) findViewById(R.id.mo_complaint_outside);

        textView1.setText(complaint);
        textView_name.setText(name);
        textView_loc.setText(loc);
        textView_join.setText(join);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Sending...");

        Log.v("MaintenanceSent", "Towoju " + complaint_id);

        firebaseDatabase = FirebaseDatabase.getInstance().getReference();
        map = new HashMap<>();

        editText = (EditText) findViewById(R.id.mo_complaint_remark_text);
        editText.setCursorVisible(true);

        editText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                hideKeyboard = view;

                textView.setText("Add Remarks");
                editText.setCursorVisible(true);
                textView2.setVisibility(View.GONE);

                create.setVisibility(View.GONE);
                enter.setVisibility(View.VISIBLE);

                return false;
            }
        });

    }

    public void mo_complaint_send_back(View view) {
        super.onBackPressed();

    }

    public void hideKeyboardMethod (View view) {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (view != null)
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

    }

    public void mo_complaint_remarks_enter(View view) {
    }

    public void mo_complaint_remarks_outside(View view) {
        hideKeyboardMethod(hideKeyboard);

        textView.setText("Send Complaint");
        textView2.setVisibility(View.VISIBLE);

        editText.setCursorVisible(false);

        enter.setVisibility(View.GONE);
        create.setVisibility(View.VISIBLE);
    }

    public void mo_complaint_submit(View view) {

//        if (new_facility == false) {

            builder = new AlertDialog.Builder(this);
            builder.setTitle("Confirm");
            builder.setMessage("You are about to send complaint to the Head of Services");
            builder.setCancelable(false);
            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            progressDialog.show();

                            if (!editText.getText().toString().isEmpty()) {
                                firebaseDatabase.child("complaints").child(complaint_id)
                                        .child("mofficer_remark").setValue(editText.getText().toString())
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                progressDialog.dismiss();
                                                Toast.makeText(MaintenanceSendActivity.this, "Successful! Your Compliant has been sent",
                                                        Toast.LENGTH_SHORT).show();
                                                finish();
                                            }
                                        });
                            } else {
                                firebaseDatabase.child("complaints").child(complaint_id)
                                        .child("mofficer_remark").setValue("none")
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                progressDialog.dismiss();
                                                Toast.makeText(MaintenanceSendActivity.this, "Successful! Your Compliant has been sent",
                                                        Toast.LENGTH_SHORT).show();
                                                finish();
                                            }
                                        });
                            }

                        }
                    });
            builder.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            // code to run when Cancel is pressed
                        }
                    });

            AlertDialog alert = builder.create();
            alert.show();

//        else {
//
//            builder = new AlertDialog.Builder(this);
////            final View view1 = layoutInflater.inflate(R.layout.new_facility_send_dialog, null);
//
//            builder.setTitle("Send Compliant to");
//            builder.setView(view);
//            builder.setCancelable(false);
//
//            final RadioGroup radioGroup = (RadioGroup) view1.findViewById(R.id.new_radio_group);
//            radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(RadioGroup group, int checkedId) {
//
//                    switch (checkedId) {
//                        case R.id.new_radio_electrical :
//                            type  = "hos_electrical";
//                            break;
//
//                        case R.id.new_radio_mechanical :
//                            type  = "hos_mechanical";
//                            break;
//
//                        case R.id.new_radio_civil :
//                            type  = "hos_civil";
//                    }
//                }
//            });
//
//
//            builder.setPositiveButton("OK",
//                    new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int id) {
//                            progressDialog.show();
//
//                            map.put("complaints/" + complaint_id + "/facility_type", type);
//
//                            if (!editText.getText().toString().isEmpty())
//                                map.put("complaints/" + complaint_id + "/mofficer_remark", editText.getText().toString());
//                            else
//                                map.put("complaints/" + complaint_id + "/mofficer_remark", "none");
//
//                            firebaseDatabase.updateChildren(map, new DatabaseReference.CompletionListener() {
//                                @Override
//                                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
//
//                                    progressDialog.dismiss();
//                                    Toast.makeText(MaintenanceSendActivity.this, "Successful! Your Compliant has been sent",
//                                            Toast.LENGTH_SHORT).show();
//                                }
//                            });
//
//                        }
//                    });
//
//            builder.setNegativeButton("Cancel",
//                    new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int id) {
//                            // code to run when Cancel is pressed
//                        }
//                    });
//
//
//
//            AlertDialog alert = builder.create();
//            alert.show();
//        }
    }
}
