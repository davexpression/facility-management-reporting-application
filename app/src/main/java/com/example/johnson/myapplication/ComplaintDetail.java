package com.example.johnson.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ComplaintDetail extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaint_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        String complaint = intent.getStringExtra("complaint");
        String location = intent.getStringExtra("location");

        String join_total = intent.getStringExtra("joined_total");
        String name = intent.getStringExtra("name");
        String facility = intent.getStringExtra("facility_name");
        String date = intent.getStringExtra("date");


        boolean joined = intent.getBooleanExtra("joined", false);
        boolean fixed = intent.getBooleanExtra("fixed", false);

        Button button = (Button) findViewById(R.id.joined_complaint);
        TextView textView = (TextView) findViewById(R.id.complaint_description);
        TextView textView1 = (TextView) findViewById(R.id.complaint_location);
        TextView textView2 = (TextView) findViewById(R.id.complaint_detail_name);
        TextView textView3 = (TextView) findViewById(R.id.complaint_detail_join_total);
        TextView textView4 = (TextView) findViewById(R.id.complaint_detail_date);

        textView.setText(complaint);
        textView1.setText(facility + " at " + location);
        textView2.setText(name);

        if (Integer.parseInt(join_total) > 1)
            textView3.setText(join_total);
        else
            textView3.setText("None");

        Date myDate = null;
//

        try {
            java.text.DateFormat format = new SimpleDateFormat("EEE LLL dd HH:mm:ss Z yyyy");
            myDate = format.parse(date);
        }
        catch (ParseException e){ }
        //this code gets references to objects in the listview_row.xml file

        String string = DateUtils.getRelativeDateTimeString(this, myDate.getTime(), DateUtils.MINUTE_IN_MILLIS,
                DateUtils.DAY_IN_MILLIS * 4, DateUtils.FORMAT_ABBREV_RELATIVE).toString();

        string = string.substring(0, string.indexOf(","));
        textView4.setText(string);

        if (joined == true)
            button.setVisibility(View.VISIBLE);

        if (fixed == true) {
            button.setText("Fixed");
            button.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {

            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
