package com.example.johnson.myapplication;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class MaintenanceComplaintsFragment extends Fragment{

    Activity mContext;
    ArrayList<ObjectItem2> objectItems = new ArrayList<>();
    CustomListAdapter_mocomplaint customListAdapter;
    SharedPreferences sharedPreferences;
    String type, location, complaint_desc, complaint_id, complaint_loc, complaint_name, complaint_num, name, last_name;
    ListView listView;
    View view;

    boolean seen = false;

    DatabaseReference fb;
    DatabaseReference complaint;
    Query query;

    private FirebaseAuth mAuth;


    HashMap<String, Object> seen_by;

    MaintenanceComplaintView maintenanceComplaintView;


    public MaintenanceComplaintsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onAttach (Context context) {
        super.onAttach(context);

        if(context instanceof Activity)
            mContext = (Activity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        view = inflater.inflate(R.layout.fragment_maintenance_complaints, container, false);
        mAuth = FirebaseAuth.getInstance();

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        type = sharedPreferences.getString("mo_pref_type", null);
        location = sharedPreferences.getString("pref_location", null);

        final FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.mofficer_complaints);
        fb = FirebaseDatabase.getInstance().getReference();
        final DatabaseReference users = fb.child("users");
        users.keepSynced(true);

        complaint = fb.child("complaints");
        complaint.keepSynced(true);

        FacilityImage facilityImage = new FacilityImage();
        final Map<String, Integer> facilityType = facilityImage.getFacilityMap();

        FacilityColor facilityColor = new FacilityColor();
        final Map<String, String> facilityMap = facilityColor.getFacilityMap();

        final Bundle bundle = new Bundle();

        if(location != null) {
            query = complaint.orderByChild("location_floor").equalTo(location);
        }

        else
            query = complaint.orderByKey();

        customListAdapter = new CustomListAdapter_mocomplaint(mContext, R.layout.fragment_maintenance_complaints, objectItems);
        listView = (ListView) view.findViewById(R.id.mo_complaints_lists);
        listView.setAdapter(customListAdapter);

        seen_by = new HashMap<>();
        seen_by.put(mAuth.getCurrentUser().getUid(), "seen");

//        Date date = new Date();
//        Date myDate = null;
//
//        String d = date.toString();
//
//        try {
//            java.text.DateFormat format = new SimpleDateFormat("EEE LLL dd HH:mm:ss Z yyyy");
//            myDate = format.parse(d);
//        }
//        catch (ParseException e){ }
//
//        Log.v("MaintenanceComplaint", "Towoju " + myDate.toString());

//
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTime(date);
//
//        calendar.add(Calendar.DATE, -4);
//        Date test = calendar.getTime();

//        String string = DateUtils.getRelativeDateTimeString(getActivity(), date.getTime(), DateUtils.MINUTE_IN_MILLIS,
//                DateUtils.DAY_IN_MILLIS * 4, DateUtils.FORMAT_ABBREV_RELATIVE).toString();

//        string = string.substring(0, string.indexOf(","));

//        Log.v("MaintenanceComplaint", "" + string);

        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                final Complaint complaint = dataSnapshot.getValue(Complaint.class);
                final HashMap<String, String> joined = (HashMap<String, String>) complaint.joined;
                final HashMap<String, String> seen_by = (HashMap<String, String>) complaint.seen_by;

                if (complaint.mofficer_remark.equals("null")) {

                    users.orderByKey().equalTo(complaint.user_id).addValueEventListener(new ValueEventListener() {

                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            for (DataSnapshot data : dataSnapshot.getChildren()) {

                                User user = data.getValue(User.class);
                                //
                                if (user.type.equals("student")) {
                                    name = user.first_name + " " + user.last_name;
                                    last_name = user.last_name;
                                } else {
                                    name = user.prefix + " " + user.first_name + " " + user.last_name;
                                    last_name = user.last_name;
                                }
//                            Log.v("ComplaintFragment", "towoju " + name);


                                if (type != null) {

//                    fb.child("users/" + complaint.getUser_id()).addValueEventListener(new ValueEventListener() {
//                        @Override
//                        public void onDataChange(DataSnapshot dataSnapshot) {

//                            if ( (dataSnapshot.child("type").getValue().toString()).equals(type) ) {

                                    if (complaint.lodged_by.equals(type)) {

                                        final ObjectItem2 objectItem2 = new ObjectItem2(complaint.complaint_nature, joined.size(), complaint.location_area);

                                        objectItem2.setDate_text(complaint.date);
                                        objectItem2.setName(name);

                                        if (seen_by.containsKey(mAuth.getCurrentUser().getUid()))
                                            seen = true;
                                        else
                                            seen = false;


                                        objectItem2.setSeen_by(seen);
                                        objectItem2.setComplaint_id(complaint.complaint_id);
                                        objectItem2.setLocation_floor(complaint.location_floor);
                                        objectItem2.setFacility(complaint.facility_name);
                                        objectItem2.setNew_facility(complaint.new_facility);
                                        objectItem2.setLast_name(last_name);

                                        objectItem2.setDate_text(complaint.date);
                                        objectItem2.setNoted(complaint.noted_by_mofficer);
                                        objectItem2.setFixed(complaint.fixed);

                                        objectItem2.setComplaint_remark(complaint.mofficer_remark);
                                        objectItem2.setJoin_total(Integer.toString(joined.size()));

                                        if (complaint.new_facility == false)
                                            objectItem2.setImg_id(facilityType.get(complaint.facility_name));

                                        objectItems.add(0, objectItem2);
                                        customListAdapter.notifyDataSetChanged();
                                    }


//                        @Override
//                        public void onCancelled(DatabaseError databaseError) {
//
//                        }
//                    });
                                } else {
                                    ObjectItem2 objectItem2 = new ObjectItem2(complaint.complaint_nature, joined.size(), complaint.location_area);

                                    objectItem2.setDate_text(complaint.date);
                                    objectItem2.setName(name);

                                    if (seen_by.containsKey(mAuth.getCurrentUser().getUid()))
                                        seen = true;
                                    else
                                        seen = false;


                                    objectItem2.setSeen_by(seen);
                                    objectItem2.setComplaint_id(complaint.complaint_id);
                                    objectItem2.setLocation_floor(complaint.location_floor);
                                    objectItem2.setFacility(complaint.facility_name);
                                    objectItem2.setNew_facility(complaint.new_facility);
                                    objectItem2.setJoin_total(Integer.toString(joined.size()));
                                    objectItem2.setDate_text(complaint.date);

                                    objectItem2.setNoted(complaint.noted_by_mofficer);
                                    objectItem2.setFixed(complaint.fixed);

                                    objectItem2.setLast_name(last_name);
                                    objectItem2.setComplaint_remark(complaint.mofficer_remark);

                                    if (complaint.new_facility == false)
                                        objectItem2.setImg_id(facilityType.get(complaint.facility_name));


                                    objectItems.add(0, objectItem2);
                                    customListAdapter.notifyDataSetChanged();
                                }

                            }

                        }


                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                }



            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

//                final Complaint complaint = dataSnapshot.getValue(Complaint.class);
//                final HashMap<String, String> seen_by = (HashMap<String, String>) complaint.seen_by;
//
//                for (String key : seen_by.keySet()) {
//                    if (key.equals(mode))
//                        seen = true;
//                }

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

//        query.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//                for (DataSnapshot child : dataSnapshot.getChildren()) {
//
//                    final Complaint complaint = child.getValue(Complaint.class);
//
//                    if ( type != null) {
//
//                        fb.child("users/" + complaint.getUser_id()).addValueEventListener(new ValueEventListener() {
//                            @Override
//                            public void onDataChange(DataSnapshot dataSnapshot) {
//
//                                ObjectItem2 objectItem2 = null;
//                                if ( (dataSnapshot.child("type").getValue().toString()).equals(type) ) {
//                                    objectItem2 = new ObjectItem2(complaint.complaint_nature, Long.toString(complaint.join_total), complaint.location_area, "date niyi");
//                                    objectItem2.setComplaint_id(complaint.complaint_id);
//                                    objectItems.add(0, objectItem2);
//                                    customListAdapter.notifyDataSetChanged();
//                                }
//                            }
//
//                            @Override
//                            public void onCancelled(DatabaseError databaseError) {
//
//                            }
//                        });
//                    }
//
//                    else {
//                        ObjectItem2 objectItem2 = new ObjectItem2(complaint.complaint_nature, Long.toString(complaint.join_total), complaint.location_area, "date niyi");
//                        objectItem2.setComplaint_id(complaint.complaint_id);
//                        objectItems.add(0, objectItem2);
//                        customListAdapter.notifyDataSetChanged();
//                    }
//
//                }
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });



        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

//                Intent intent = new Intent(getActivity(), MaintenanceComplaintActivity.class);
//                String complaint = objectItems.get(position).complaint_nature;
//                String complaint_id = objectItems.get(position).getComplaint_id();
//
//                intent.putExtra("complaint", complaint);
//                intent.putExtra("complaint_id", complaint_id);
//                startActivity(intent);

                LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.list_highlight);
                linearLayout.setBackgroundColor(Color.WHITE);
                complaint_id = objectItems.get(position).complaint_id;

                if (objectItems.get(position).seen_by == false)
                    complaint.child(complaint_id).child("seen_by").child(mAuth.getCurrentUser().getUid()).setValue("seen");

                String facility_name;
                String loc, date, use_date = null;
                String fragment = "all";
                boolean new_facility;

                complaint_desc = objectItems.get(position).complaint_nature;
                complaint_loc = objectItems.get(position).location_floor + " | " + objectItems.get(position).location_text;
                complaint_name = objectItems.get(position).name;
                complaint_num = objectItems.get(position).join_total;
                last_name = objectItems.get(position).last_name;
                date = objectItems.get(position).date_text;

//                try {
//
//                    java.text.DateFormat format = new SimpleDateFormat("EEE LLL dd yyyy");
//                    Date myDate = format.parse(date);
//                    use_date = myDate.toString();
//
//
//                }
//                catch (ParseException e){ }

                facility_name = objectItems.get(position).facility;
                new_facility = objectItems.get(position).new_facility;

                loc = facility_name + " at " + complaint_loc;

                bundle.putString("complaint", complaint_desc);
                bundle.putString("complaint_id", complaint_id);
                bundle.putString("complaint_loc", loc);
                bundle.putString("complaint_name", complaint_name);
                bundle.putString("complaint_num", complaint_num);
                bundle.putString("last_name", objectItems.get(position).getLast_name());
                bundle.putString("date", date);
                bundle.putBoolean("new", new_facility);
                bundle.putString("fragment", fragment);

                bundle.putBoolean("fixed", objectItems.get(position).fixed);
                bundle.putBoolean("noted", objectItems.get(position).noted);

                maintenanceComplaintView = new MaintenanceComplaintView();
                maintenanceComplaintView.setArguments(bundle);

                listView.setVisibility(View.GONE);

                getFragmentManager().beginTransaction()
                        //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)

                        .add(R.id.mofficer_complaints,maintenanceComplaintView, "mo_view")
                        .addToBackStack("mo_view")
                        .commit();
            }
        });


//        complaint.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot data) {
//                for (DataSnapshot child : data.getChildren()) {
//
//                    Complaint complaint = child.getValue(Complaint.class);
//
////                    HashMap<String, HashMap<String, String>> complaint_description =  (HashMap<String, HashMap<String, String>>) child.child("complaint_description").getValue();
////                    String key = complaint_description.keySet().iterator().next();
//
//
////                    Log.v("ComplaintFragment", "towoju " + complaint_description.get(key).get("user_id"));
//
//                    ObjectItem2 objectItem2 = new ObjectItem2(complaint.complaint_nature, Integer.toString(complaint.join_total), complaint.location_area, "date niyi");
//                    objectItems.add(0, objectItem2);
//                    customListAdapter.notifyDataSetChanged();
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//                Log.v("ComplaintFragment", "Towoju " + databaseError);
//
//            }
//        });


        return  view;


    }

    public void visible() {
        listView.setVisibility(View.VISIBLE);
    }

//    @Override
//    public void sendDataMethod() {
//
//    }
}
