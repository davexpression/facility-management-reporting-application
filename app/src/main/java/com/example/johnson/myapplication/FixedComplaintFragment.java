package com.example.johnson.myapplication;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class FixedComplaintFragment extends Fragment {

    Activity mContext;
    //    Object object;
    ArrayList<ObjectItem2> objectItems = new ArrayList<>();
    ListView listView;
    String user_name, join_total, name, facility_name;
    boolean joined;

    private FirebaseAuth mAuth;

    public FixedComplaintFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach (Context context) {
        super.onAttach(context);

        if(context instanceof Activity)
            mContext = (Activity) context;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_fixed_complaint, container, false);

        final CustomListAdapter_mycomplaint customListAdapter = new CustomListAdapter_mycomplaint(mContext, R.layout.my_complaints, objectItems);
        final DatabaseReference fb = FirebaseDatabase.getInstance().getReference();

        mAuth = FirebaseAuth.getInstance();

        DatabaseReference complaint = fb.child("complaints");
        final DatabaseReference users = fb.child("users");

        complaint.keepSynced(true);
        users.keepSynced(true);

        listView = (ListView) view.findViewById(R.id.fixed_complaints_lists);
        listView.setAdapter(customListAdapter);

        FacilityImage facilityImage = new FacilityImage();
        final Map<String, Integer> facilityType = facilityImage.getFacilityMap();

        FacilityColor facilityColor = new FacilityColor();
        final Map<String, String> facilityMap = facilityColor.getFacilityMap();


        Query query;
        query = complaint.orderByChild("user_id").equalTo(mAuth.getCurrentUser().getUid());

        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Complaint complaint = dataSnapshot.getValue(Complaint.class);

                if (complaint.fixed == true){

                    HashMap<String, String> joined = (HashMap<String, String>) complaint.joined;

                    final ObjectItem2 objectItem2 = new ObjectItem2(complaint.complaint_nature, joined.size(), complaint.location_area);
                    objectItem2.setLocation_floor(complaint.location_floor);
                    objectItem2.setDate_text(complaint.date);
                    objectItem2.setComplaint_id(complaint.complaint_id);
                    objectItem2.setFacility(complaint.facility_name);
                    objectItem2.setJoin_total(Integer.toString(joined.size()));
                    objectItem2.setNew_facility(complaint.new_facility);

                    if (complaint.new_facility == false)
                        objectItem2.setImg_id(facilityType.get(complaint.facility_name));


                    if (complaint.user_id.equals(mAuth.getCurrentUser().getUid()))
                        objectItem2.setJoined(false);
                    else
                        objectItem2.setJoined(true);

                    users.orderByKey().equalTo(complaint.user_id).addListenerForSingleValueEvent(new ValueEventListener() {

                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            for (DataSnapshot data : dataSnapshot.getChildren()) {

                                User user = data.getValue(User.class);

                                if (user.type.equals("student"))
                                    objectItem2.setName(user.first_name + " " + user.last_name);
                                else
                                    objectItem2.setName(user.prefix +  " " + user.first_name + " " + user.last_name);

                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                    objectItems.add(0, objectItem2);
                    customListAdapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(getActivity(), ComplaintDetail.class);
                String complaint = objectItems.get(position).complaint_nature;

                String location = objectItems.get(position).location_floor + "  |  " +
                        objectItems.get(position).location_text;

                joined = objectItems.get(position).joined;

                name = objectItems.get(position).name;
                join_total = objectItems.get(position).join_total;
                facility_name = objectItems.get(position).facility;
                String date = objectItems.get(position).date_text;


                intent.putExtra("complaint", complaint);
                intent.putExtra("location", location);
                intent.putExtra("name", name);
                intent.putExtra("date", date);

                intent.putExtra("joined", joined);
                intent.putExtra("fixed", true);
                intent.putExtra("joined_total", join_total);
                intent.putExtra("facility_name", facility_name);

                startActivity(intent);

            }
        });

        return  view;
    }

}
