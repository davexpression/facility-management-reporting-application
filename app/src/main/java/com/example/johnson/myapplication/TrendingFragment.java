package com.example.johnson.myapplication;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.Inflater;


/**
 * A simple {@link Fragment} subclass.
 */
public class TrendingFragment extends Fragment {


    Activity mContext;
    ListView listView;
    ArrayList<ObjectItem2> objectItems = new ArrayList<>();
    Intent intent;
    Query query;
    SharedPreferences sharedPreferences;
    String mode, complaint_desc, complaint_id, complaint_loc, complaint_num, complaint_name;
    Bundle bundle;
    MaintenanceComplaintView maintenanceComplaintView;

    public TrendingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach (Context context) {
        super.onAttach(context);

        if(context instanceof Activity)
            mContext = (Activity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_trending, container, false);

        final CustomListAdapter_most_general customListAdapter2 = new CustomListAdapter_most_general(mContext, R.layout.most_lodged_complaints, objectItems);
        listView = (ListView) view.findViewById(R.id.trending_lists);
        intent = new Intent(getActivity(), MostLodgedComplaint.class);

        final DatabaseReference fb = FirebaseDatabase.getInstance().getReference();
        DatabaseReference complaints = fb.child("complaints");

        final DatabaseReference users = fb.child("users");
        users.keepSynced(true);

        bundle = new Bundle();

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mode = sharedPreferences.getString("mode", null);

        complaints.keepSynced(true);
        listView.setAdapter(customListAdapter2);



        if(mode.equals("mofficer"))
            query = complaints.orderByKey();
        else
            query = complaints.orderByChild("facility_type").equalTo(mode);

        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Complaint complaint = dataSnapshot.getValue(Complaint.class);
                HashMap<String, String> joined = (HashMap<String, String>) complaint.joined;

                    final ObjectItem2 objectItem = new ObjectItem2(complaint.complaint_nature, joined.size(), complaint.location_area);

                    objectItem.setLocation_text(complaint.location_area);
                    objectItem.setLocation_floor(complaint.location_floor);
                    objectItem.setStaff(complaint.staff);
                    objectItem.setFacility(complaint.facility_name);
                    objectItem.setStudent(complaint.student);
                    objectItem.setDate_text(complaint.date);
                    objectItem.setNoted(complaint.noted_by_mofficer);
                    objectItem.setFixed(complaint.fixed);
                    objectItem.setComplaint_remark(complaint.mofficer_remark);
                    objectItem.setComplaint_id(complaint.complaint_id);
                    objectItem.setNew_facility(complaint.new_facility);

                    users.orderByKey().equalTo(complaint.user_id).addListenerForSingleValueEvent(new ValueEventListener() {

                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            for (DataSnapshot data : dataSnapshot.getChildren()) {

                                User user = data.getValue(User.class);

                                if (user.type.equals("student")) {
                                    objectItem.setName(user.first_name + " " + user.last_name);
                                } else {
                                    objectItem.setName(user.prefix + " " + user.first_name + " " + user.last_name);
                                }
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                    objectItems.add(objectItem);
                    Collections.sort(objectItems, objectItem);
                    customListAdapter2.notifyDataSetChanged();
                }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


//        query.addValueEventListener(new ValueEventListener() {
//
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                for (DataSnapshot child : dataSnapshot.getChildren()) {
//                    Most_Lodged most_lodged = child.getValue(Most_Lodged.class);
//
//                    ObjectItem objectItem = new ObjectItem(most_lodged.facility, Long.toString(most_lodged.total_no), Long.toString(most_lodged.staff_no), Long.toString(most_lodged.student_no), most_lodged.location_floor + "  |  " + most_lodged.location_area);
//                    objectItem.setLocation_area(most_lodged.location_area);
//                    objectItem.setLocation_floor(most_lodged.location_floor);
//
//                    objectItems.add(objectItem);
//                    customListAdapter2.notifyDataSetChanged();
//
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {

//                String loc_area = objectItems.get(position).location_text;
//                String facility_name = objectItems.get(position).facility;
//
//                intent.putExtra("loc_area", loc_area);
//                intent.putExtra("facility_name", facility_name);
//
//                startActivity(intent);


                String facility_name;
                String loc;
                String fragment = "most";

                complaint_desc = objectItems.get(position).complaint_nature;
                complaint_id = objectItems.get(position).complaint_id;
                complaint_loc = objectItems.get(position).location_floor + " | " + objectItems.get(position).location_text;
                complaint_name = objectItems.get(position).name;
                complaint_num = Integer.toString(objectItems.get(position).complaint_number);
                facility_name = objectItems.get(position).facility;

                loc = facility_name + " at " + complaint_loc;

                bundle.putString("complaint", complaint_desc);
                bundle.putString("complaint_id", complaint_id);
                bundle.putString("complaint_loc", loc);
                bundle.putString("complaint_name", complaint_name);
                bundle.putString("complaint_num", complaint_num);
                bundle.putInt("student", objectItems.get(position).student);
                bundle.putInt("staff", objectItems.get(position).staff);
                bundle.putString("date", objectItems.get(position).date_text);

                bundle.putString("fragment", fragment);
                bundle.putString("remark", objectItems.get(position).complaint_remark);

                bundle.putBoolean("fixed", objectItems.get(position).fixed);
                bundle.putBoolean("noted", objectItems.get(position).noted);
                maintenanceComplaintView = new MaintenanceComplaintView();
                maintenanceComplaintView.setArguments(bundle);

                HeadComplaintView headComplaintView = new HeadComplaintView();
                headComplaintView.setArguments(bundle);


                listView.setVisibility(View.GONE);

                if (mode.equals("mofficer")) {
                    getFragmentManager().beginTransaction()
                            //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)

                            .add(R.id.mofficer_complaints, maintenanceComplaintView, "mo_view")
                            .addToBackStack("mo_view_sent")
                            .commit();
                }

                else {
                    getFragmentManager().beginTransaction()
                            //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)

                            .add(R.id.mofficer_complaints, headComplaintView, "view")
                            .addToBackStack("mo_view_sent")
                            .commit();
                }
            }
        });
        return view;
    }

    public void visible() {
        listView.setVisibility(View.VISIBLE);
    }
}
