package com.example.johnson.myapplication;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

public class NotificationsService extends Service {

    public static final int id = 1234;

    public NotificationsService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if(intent != null) {
            if (intent.getAction().equals("action")) {
//            final String action = intent.getAction();

                NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                        .setContentTitle("Complaint Update")
                        .setContentText("New User joined in complaining")
                        .setAutoCancel(true)
                        .setSmallIcon(R.drawable.ic_menu_send)
                        .setWhen(System.currentTimeMillis());

                Intent launch = new Intent(this, HomeActivity.class);
                launch.putExtra("start_frag", true);

                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, launch, 0);

                Notification notification = builder.getNotification();
                NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                manager.notify(id, notification);
            }
        }
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
