package com.example.johnson.myapplication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MaintenanceComplaintActivity extends AppCompatActivity {

    private  AlertDialog.Builder builder;
    View hideKeyboard;
    TextView textView, textView1, textView2, fixed_text, noted_text, forward_text;
    Intent intent_send, intent_receive;

    Button button;
    EditText editText;
    private DatabaseReference firebaseDatabase;
    private ProgressDialog progressDialog;
    private ImageView fixed_image, noted_image, forward_image;
    LinearLayout fixed, noted, forward;
    MenuItem item;
    Menu menu;
    String complaint, complaint_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maintenance_complaint);

        Toolbar toolbar = (Toolbar) findViewById(R.id.mo_toolbar_complaint);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        intent_send = new Intent(this, MaintenanceSendActivity.class);
        intent_receive = getIntent();
        complaint = intent_receive.getStringExtra("complaint");
        complaint_id = intent_receive.getStringExtra("complaint_id");

        textView1 = (TextView) findViewById(R.id.mo_complaint_description);
        textView1.setText(complaint);

        fixed_text = (TextView) findViewById(R.id.mo_complaint_fixed_text);
        fixed_image = (ImageView) findViewById(R.id.mo_complaint_fixed_image);

        noted_text = (TextView) findViewById(R.id.mo_complaint_noted_text);
        noted_image = (ImageView) findViewById(R.id.mo_complaint_noted_image);

        forward_text = (TextView) findViewById(R.id.mo_complaint_forward_text);
        forward_image = (ImageView) findViewById(R.id.mo_complaint_forward_image);


        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Sending...");

        firebaseDatabase = FirebaseDatabase.getInstance().getReference();

        builder = new AlertDialog.Builder(this);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.home, menu);

        item = menu.findItem(R.id.action_settings);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity2.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void join_in_complaining (View view) {
        builder.setTitle("Confirm");
        builder.setMessage("You are about to mark this Complaint as Fixed");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
//                        Intent intent = new Intent(TrendingDetail.this, HomeActivity.class);
//                        intent.putExtra("startFrag", true);
//
//                        Toast.makeText(TrendingDetail.this, "Compliant Joined",
//                                Toast.LENGTH_SHORT).show();
//
//                        startActivity(intent);

                    }
                });
        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // code to run when Cancel is pressed
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void mo_complaint_back(View view)  {

        super.onBackPressed();
    }

    public void mo_complaint_send(View view) {

//        builder.setTitle("Confirm");
//        builder.setMessage("You are about to send Complaint to the Head of Services");
//        builder.setCancelable(false);
//        builder.setPositiveButton("OK",
//                new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int id) {
//                        progressDialog.show();
//
//                        firebaseDatabase.child("complaints").child(complaint_id)
//                                .child("sent").setValue(true)
//                                .addOnSuccessListener(new OnSuccessListener<Void>() {
//                            @Override
//                            public void onSuccess(Void aVoid) {
//                                progressDialog.dismiss();
//                                        Toast.makeText(MaintenanceComplaintActivity.this, "Successful! Your Compliant has been sent",
//                                Toast.LENGTH_SHORT).show();
//                            }
//                        });
//
//                    }
//                });
//        builder.setNegativeButton("Cancel",
//                new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int id) {
//                        // code to run when Cancel is pressed
//                    }
//                });
//
//        AlertDialog alert = builder.create();
//        alert.show();

        intent_send.putExtra("complaints", complaint);
        intent_send.putExtra("complaints_id", complaint_id);
        startActivity(intent_send);
    }



//    public void mo_complaint_send2(View view) {
//        send2.setVisibility(View.GONE);
//        String text = editText.getText().toString();
//        textView3.setText(text);
//        linearLayout.setVisibility(View.GONE);
//        linearLayout1.setVisibility(View.VISIBLE);
//        hideKeyboardMethod(hideKeyboard);
//
//        textView.setText("Send Complaint");
//        textView2.setVisibility(View.VISIBLE);
//    }



    public void hideKeyboardMethod (View view) {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (view != null)
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

    }

    public void mo_complaint_noted(View view) {

        firebaseDatabase.child("complaints").child(complaint_id)
                .child("noted_by_mofficer").setValue(true);
    }

    public void mo_complaint_fixed(View view) {

//        if (fixed_text.getTextColors() == null){
//            fixed_image.setTint
//        }

            firebaseDatabase.child("complaints").child(complaint_id)
                .child("fixed").setValue(true);
    }

}
