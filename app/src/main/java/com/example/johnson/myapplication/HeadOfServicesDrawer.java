package com.example.johnson.myapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class HeadOfServicesDrawer extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, PopupMenu.OnMenuItemClickListener, HeadComplaintView.OnFragmentInteractionListener
{


    private FirebaseAuth mAuth;
    private  AlertDialog.Builder builder;
    private SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    ActionBarDrawerToggle toggle;
    ListView listView;
    LinearLayout linearLayout;
    TextView textView, hos_name, hos_email;
    ImageView imageView;
    DatabaseReference fb;
    DatabaseReference user;
    String name;

    HeadComplaintsFragment headComplaintsFragment;
    HeadComplaintView headComplaintView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_head_of_services_drawer);

        mAuth = FirebaseAuth.getInstance();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sharedPreferences.edit();


        View inflatedView = getLayoutInflater().inflate(R.layout.fragment_head_complaints, null);
        headComplaintView = new HeadComplaintView();

//        String fname  = sharedPreferences.getString("hos_first_name", null);
//        String lname  = sharedPreferences.getString("hos_last_name", null);

        fb = FirebaseDatabase.getInstance().getReference();
        user = fb.child("users");
        user.keepSynced(true);


        final NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View view = navigationView.getHeaderView(0);

        hos_name = (TextView) view.findViewById(R.id.hos_name);
        hos_email = (TextView) view.findViewById(R.id.hos_email);

        user.orderByKey().equalTo(mAuth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot data : dataSnapshot.getChildren()) {

                    User user = data.getValue(User.class);

                    name = user.first_name + " " + user.last_name;
                }

                hos_name.setText(name);
                hos_email.setText(mAuth.getCurrentUser().getEmail());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        hos_name.setText(name);
        hos_email.setText(mAuth.getCurrentUser().getEmail());

        final Toolbar toolbar = (Toolbar) findViewById(R.id.head_main1_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        textView = (TextView) findViewById(R.id.head_toobar_title);
        imageView = (ImageView) findViewById(R.id.hos_action_bar);


        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.content_head_of_services_drawer, new HeadComplaintsFragment(), "hos_cover").
                    commit();
            navigationView.getMenu().getItem(0).setChecked(true);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

////        final View.OnClickListener originalToolbarListener = toggle.getToolbarNavigationClickListener();
//
//        final HeadComplaintView headComplaintView = (HeadComplaintView) getSupportFragmentManager().findFragmentByTag("view");
//
//
//
//         getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
//
//            @Override
//            public void onBackStackChanged() {
//                if (getFragmentManager().getBackStackEntryCount() > 0) {
//                    toggle.setDrawerIndicatorEnabled(false);
//                    toggle.setHomeAsUpIndicator(R.drawable.baseline_arrow_back_black_18);
//
//
//                    toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//
////                            HeadComplaintView headComplaintView = (HeadComplaintView) getSupportFragmentManager().findFragmentByTag("view");
////                            headComplaintView.closeFrag();
//                        }
//                    });
//                } else {
//                    toggle.setDrawerIndicatorEnabled(true);
//                }
//            }
//        });

    }


    public void showDrawerToggle(boolean showDrawerIndicator, String lname, final String fragment) {
        toggle.setDrawerIndicatorEnabled(showDrawerIndicator);
        toggle.setHomeAsUpIndicator(R.drawable.baseline_arrow_back_white_18);
        imageView.setVisibility(View.GONE);


        if (fragment.equals("most"))
            textView.setText("Complaint (most lodged)");
        else
            textView.setText("Complaint from " + lname);


        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!toggle.isDrawerIndicatorEnabled()) {

                    toggle.setDrawerIndicatorEnabled(true);

                    HeadComplaintsFragment headComplaintsFragment = (HeadComplaintsFragment) getSupportFragmentManager().findFragmentByTag("hos_cover");
                    HeadNotesFragment headNotesFragment = (HeadNotesFragment) getSupportFragmentManager().findFragmentByTag("hos_noted");
                    HeadFixedFragment headFixedFragment = (HeadFixedFragment) getSupportFragmentManager().findFragmentByTag("hos_fixed");
                    TrendingFragment trendingFragment = (TrendingFragment) getSupportFragmentManager().findFragmentByTag("hos_most");

                    HeadComplaintView headComplaintView = (HeadComplaintView) getSupportFragmentManager().findFragmentByTag("view");
                    headComplaintView.closeFrag();

                    if (fragment.equals("all")) {
                        textView.setText("All Complaints");
                        headComplaintsFragment.visible();
                        imageView.setVisibility(View.VISIBLE);

                    }

                    else if (fragment.equals("fixed")) {
                        textView.setText("Fixed");
                        headFixedFragment.visible();
                        imageView.setVisibility(View.GONE);
                    }

                    else if (fragment.equals("noted")) {

                        headNotesFragment.visible();
                        textView.setText("Noted");
                        imageView.setVisibility(View.GONE);

                    }

                    else{
                        trendingFragment.visible();
                        textView.setText("Most Lodged Complaints");
                        imageView.setVisibility(View.GONE);
                    }


                }
            }
        });
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.head_of_services_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            if (id == R.id.action_settings) {
                startActivity(new Intent(this, SettingsActivity3.class));
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        TextView textView = (TextView) findViewById(R.id.head_toobar_title);


        if (id == R.id.head_complaints) {

            textView.setText("All Complaints");
            imageView.setVisibility(View.VISIBLE);

//            HeadComplaintsFragment headComplaintsFragment = new HeadComplaintsFragment();

            getSupportFragmentManager().beginTransaction()
                    //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                    .replace(R.id.content_head_of_services_drawer,new HeadComplaintsFragment(), "hos_cover")
                    .addToBackStack(null)
                    .commit();

        } else if (id == R.id.head_most_lodged) {

//            textView.setText("Most Lodged Complaints");
//
//            HeadMostLodgedFragment headMostLodgedFragment = new HeadMostLodgedFragment();
//            getSupportFragmentManager().beginTransaction()
//                    //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
//                    .replace(R.id.content_home,headMostLodgedFragment)
//                    .addToBackStack(null)
//                    .commit();

            textView.setText("Most Lodged Complaints");
            imageView.setVisibility(View.GONE);
            getSupportFragmentManager().beginTransaction()
                    //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                    .replace(R.id.content_head_of_services_drawer, new TrendingFragment(), "hos_most")
                    .addToBackStack(null)
                    .commit();

        } else if (id == R.id.head_imp) {

            textView.setText("Noted");
            imageView.setVisibility(View.GONE);

            getSupportFragmentManager().beginTransaction()
                    //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                    .replace(R.id.content_head_of_services_drawer, new HeadNotesFragment(), "hos_noted")
                    .addToBackStack(null)
                    .commit();

        } else if (id == R.id.head_fixed) {

            textView.setText("Fixed");
            imageView.setVisibility(View.GONE);

            HeadFixedFragment headFixedFragment = new HeadFixedFragment();
            getSupportFragmentManager().beginTransaction()
                    //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                    .replace(R.id.content_head_of_services_drawer, new HeadFixedFragment(), "hos_fixed")
                    .addToBackStack(null)
                    .commit();
        }

        else if (id == R.id.head_settings) {
            startActivity(new Intent(this, SettingsActivity3.class));


        } else if (id == R.id.head_logout) {

            builder = new AlertDialog.Builder(this);

            builder.setTitle("Confirm");
            builder.setMessage("Are you sure you want to log out");
            builder.setCancelable(false);
            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {

                            mAuth.signOut();
                            editor.clear();
                            editor.commit();

                            Intent intent = new Intent(HeadOfServicesDrawer.this, MainActivity.class);
                            startActivity(intent);

                        }
                    });
            builder.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            // code to run when Cancel is pressed
                        }
                    });

            AlertDialog alert = builder.create();
            alert.show();


        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void hos_toolbar_click(View view) {

        PopupMenu popup = new PopupMenu(this, view);
        popup.setOnMenuItemClickListener(this);
        popup.inflate(R.menu.mo_actionbar2);
        popup.show();
    }


    @Override
    public boolean onMenuItemClick(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_staff_only:
                textView.setText("Staff only");
                editor.putString("hos_pref_type", "staff");
                editor.commit();

//                headComplaintsFragment = new HeadComplaintsFragment();
                getSupportFragmentManager().beginTransaction()
                        //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                        .replace(R.id.content_head_of_services_drawer,new HeadComplaintsFragment(), "hos_cover")
                        .addToBackStack(null)
                        .commit();


                break;

            case R.id.action_student_only:
                textView.setText("Student only");
                editor.putString("hos_pref_type", "student");
                editor.commit();

                getSupportFragmentManager().beginTransaction()
                        //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                        .replace(R.id.content_head_of_services_drawer, new HeadComplaintsFragment(), "hos_cover")
                        .addToBackStack(null)
                        .commit();


                break;

            case R.id.action_staff_student:
                textView.setText("All Complaints");
                editor.putString("hos_pref_type", null);
                editor.commit();

                getSupportFragmentManager().beginTransaction()
                        //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                        .replace(R.id.content_head_of_services_drawer,new HeadComplaintsFragment(), "hos_cover")
                        .addToBackStack(null)
                        .commit();
        }
        return false;
    }

}
