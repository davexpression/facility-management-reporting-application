package com.example.johnson.myapplication;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class MaintenanceArchiveFragment extends Fragment {

    Activity mContext;
    ArrayList<ObjectItem2> objectItems = new ArrayList<>();
    CustomListAdapter_mycomplaint customListAdapter;
    ListView listView;
    String complaint_desc, complaint_id, complaint_loc, complaint_name, complaint_num, last_name;

    DatabaseReference fb;
    DatabaseReference complaint;

    MaintenanceComplaintView maintenanceComplaintView;


    public MaintenanceArchiveFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach (Context context) {
        super.onAttach(context);

        if(context instanceof Activity)
            mContext = (Activity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_maintenance_archive, container, false);

        fb = FirebaseDatabase.getInstance().getReference();
        complaint = fb.child("complaints");
        complaint.keepSynced(true);

        final FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.mofficer_complaints);

        final DatabaseReference users = fb.child("users");
        users.keepSynced(true);

        FacilityImage facilityImage = new FacilityImage();
        final Map<String, Integer> facilityType = facilityImage.getFacilityMap();

        FacilityColor facilityColor = new FacilityColor();
        final Map<String, String> facilityMap = facilityColor.getFacilityMap();

        final Bundle bundle = new Bundle();

        customListAdapter = new CustomListAdapter_mycomplaint(mContext, R.layout.fragment_maintenance_archive, objectItems);
        listView = (ListView) view.findViewById(R.id.mo_complaints_lists_fixed);
        listView.setAdapter(customListAdapter);

        complaint.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                final Complaint complaint = dataSnapshot.getValue(Complaint.class);
                final HashMap<String, String> joined = (HashMap<String, String>) complaint.joined;


                if (complaint.fixed == true) {

                    final ObjectItem2 objectItem2 = new ObjectItem2(complaint.complaint_nature, joined.size(), complaint.location_area);

                    users.orderByKey().equalTo(complaint.user_id).addListenerForSingleValueEvent(new ValueEventListener() {

                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            for (DataSnapshot data : dataSnapshot.getChildren()) {

                                User user = data.getValue(User.class);

                                if (user.type.equals("student")) {
                                    objectItem2.setName(user.first_name + " " + user.last_name);
                                    last_name = user.last_name;
                                    objectItem2.setLast_name(last_name);
                                }
                                else {
                                    objectItem2.setName(user.prefix + " " + user.first_name + " " + user.last_name);
                                    last_name = user.last_name;
                                    objectItem2.setLast_name(last_name);
                                }

                                objectItem2.setDate_text(complaint.date);
                                objectItem2.setComplaint_id(complaint.complaint_id);

                                objectItem2.setLocation_floor(complaint.location_floor);
                                objectItem2.setFacility(complaint.facility_name);
                                objectItem2.setJoin_total(Integer.toString(joined.size()));
                                objectItem2.setComplaint_remark(complaint.mofficer_remark);
                                objectItem2.setNew_facility(complaint.new_facility);

                                objectItem2.setNoted(complaint.noted_by_mofficer);
                                objectItem2.setFixed(complaint.fixed);

                                if (complaint.new_facility == false)
                                    objectItem2.setImg_id(facilityType.get(complaint.facility_name));


                                objectItems.add(0, objectItem2);
                                customListAdapter.notifyDataSetChanged();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }


            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String facility_name;
                String loc;
                String fragment = "fixed";

                complaint_desc = objectItems.get(position).complaint_nature;
                complaint_id = objectItems.get(position).complaint_id;
                complaint_loc = objectItems.get(position).location_floor + " | " + objectItems.get(position).location_text;
                complaint_name = objectItems.get(position).name;
                complaint_num = objectItems.get(position).join_total;
                facility_name = objectItems.get(position).facility;

                loc = facility_name + " at " + complaint_loc;

                bundle.putString("complaint", complaint_desc);
                bundle.putString("date", objectItems.get(position).date_text);
                bundle.putString("complaint_id", complaint_id);
                bundle.putString("complaint_loc", loc);
                bundle.putString("complaint_name", complaint_name);
                bundle.putString("complaint_num", complaint_num);
                bundle.putString("last_name", objectItems.get(position).getLast_name());
                bundle.putString("fragment", fragment);
                bundle.putString("remark", objectItems.get(position).complaint_remark);

                bundle.putBoolean("fixed", objectItems.get(position).fixed);
                bundle.putBoolean("noted", objectItems.get(position).noted);

                maintenanceComplaintView = new MaintenanceComplaintView();
                maintenanceComplaintView.setArguments(bundle);

                listView.setVisibility(View.GONE);

                getFragmentManager().beginTransaction()
                        //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)

                        .add(R.id.mofficer_complaints,maintenanceComplaintView, "mo_view")
                        .addToBackStack("mo_view_fixed")
                        .commit();
            }
        });

        return view;
    }

    public void visible() {
        listView.setVisibility(View.VISIBLE);
    }


}
