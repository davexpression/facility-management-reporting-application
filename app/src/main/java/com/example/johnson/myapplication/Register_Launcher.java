package com.example.johnson.myapplication;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import stanford.androidlib.SimpleActivity;
import stanford.androidlib.SimpleDialog;

public class Register_Launcher extends SimpleActivity {

    private static final String Staff_id = "cisstaffid";
    private static final String Student_id = "cisstudentid";
    private boolean staff_view = false;
    private boolean student_view = false;
    public static final String TYPE_OF_USER = "MyPrefsFile";
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register__launcher);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sharedPreferences.edit();
    }

    public void staff_register_click (View view) {

        $TV(R.id.id_question).setText("Enter the Faculty Staff ID");
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.category_question);

        if(linearLayout.getVisibility() == View.INVISIBLE)
            linearLayout.setVisibility(View.VISIBLE);

        $ET(R.id.id_input).getText().clear();

        staff_view = true;
        student_view = false;
    }

    public void student_register_click (View view) {
        $TV(R.id.id_question).setText("Enter the Faculty Student ID");
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.category_question);

        if(linearLayout.getVisibility() == View.INVISIBLE)
            linearLayout.setVisibility(View.VISIBLE);

        $ET(R.id.id_input).getText().clear();

        staff_view = false;
        student_view = true;
    }

    public void id_submit (View view) {
        EditText id_password = $ET((R.id.id_input));
        String id = id_password.getText().toString();
        Intent intent = new Intent(this, Registration.class);

        if(id.equals(Staff_id) && staff_view){
            editor.putString("mode", "staff");
            editor.commit();
            startActivity(intent);

        }

        else if(id.equals(Student_id) && student_view){
            editor.putString("mode", "student");
            editor.commit();
            startActivity(intent);
        }
        else{
            //Use dialog box here
            SimpleDialog.with(this).showAlertDialog("Sorry the ID you entered is Incorrect", "Invalid Parameter");
        }




    }
}
