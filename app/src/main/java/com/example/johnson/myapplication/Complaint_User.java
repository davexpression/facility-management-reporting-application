package com.example.johnson.myapplication;

import java.io.Serializable;

/**
 * Created by Johnson on 14/05/2018.
 */

public class Complaint_User implements Serializable  {

    String complaint_id;

    public Complaint_User() {

    }

    public Complaint_User(String complaint_id) {
        this.complaint_id = complaint_id;
    }

    public String getUser_id() {return complaint_id;}
    public void setUser_id(String input) {this.complaint_id = input;}


}


