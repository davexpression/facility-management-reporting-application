package com.example.johnson.myapplication;

import android.content.Intent;

import java.io.Serializable;

/**
 * Created by Johnson on 14/05/2018.
 */

public class Most_Lodged {

    String facility;
    String facility_type;
    String location_area;
    String location_floor;
    Long total_no;
    Long student;
    Long staff;

    public Most_Lodged() {

    }


    public Most_Lodged(String facility, String facility_type, String location_area, String location_floor, Long staff, Long student, Long total_no) {

        this.facility = facility;
        this.facility_type = facility_type;
        this.location_area = location_area;
        this.location_floor = location_floor;
        this.staff = staff;
        this.student = student;
        this.total_no = total_no;

    }

    public String getFacility() {return facility;}
    public void setFacility(String input) {this.facility = input;}

    public String getFacility_type() {return facility_type;}
    public void setFacility_type(String input) {this.facility_type = input;}

    public String getLocation_area() {return location_area;}
    public void setLocation_area(String input) {this.location_area = input;}

    public String getLocation_floor() {return location_floor;}
    public void setLocation_floor(String input) {this.location_floor = input;}

    public Long getTotal_no() {return total_no;}
    public void setTotal_no(Long input) {this.total_no = input;}

}


