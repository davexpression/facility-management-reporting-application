package com.example.johnson.myapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.view.MenuInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MaintenanceDrawer extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, PopupMenu.OnMenuItemClickListener,
        MaintenanceComplaintView.OnFragmentInteractionListener{

    private FirebaseAuth mAuth;
    private  AlertDialog.Builder builder;
    private SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    TextView textView, mo_name, mo_email;
    ListView listView;
    Menu myMenu;
    MenuItem menuItem;
    Toolbar toolbar;
    DatabaseReference fb;
    DatabaseReference user;
    String name;

    ActionBarDrawerToggle toggle;
    ImageView imageView;

    MaintenanceComplaintsFragment maintenanceComplaintsFragment;
    MaintenanceComplaintView maintenanceComplaintView;


    //    private FragmentInterface fragmentInterfaceListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maintenance_drawer);

        mAuth = FirebaseAuth.getInstance();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sharedPreferences.edit();

        toolbar = (Toolbar) findViewById(R.id.mo_main1_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        textView = (TextView) findViewById(R.id.mo_toobar_title);
        imageView = (ImageView) findViewById(R.id.mo_action_bar_image);


        fb = FirebaseDatabase.getInstance().getReference();
        user = fb.child("users");
        user.keepSynced(true);

//        String fname  = sharedPreferences.getString("mo_first_name", null);
//        String lname  = sharedPreferences.getString("mo_last_name", null);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View view = navigationView.getHeaderView(0);

        mo_name = (TextView) view.findViewById(R.id.mofficer_name);
        mo_email = (TextView) view.findViewById(R.id.mofficer_email);

        user.orderByKey().equalTo(mAuth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot data : dataSnapshot.getChildren()) {

                    User user = data.getValue(User.class);

                    name = user.first_name + " " + user.last_name;
                }

                mo_name.setText(name);
                mo_email.setText(mAuth.getCurrentUser().getEmail());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        View inflatedView = getLayoutInflater().inflate(R.layout.fragment_maintenance_complaints, null);
//        listView = (ListView) inflatedView.findViewById(R.id.mo_complaints_lists);
        maintenanceComplaintView = new MaintenanceComplaintView();


        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.content_maintenance_drawer, new MaintenanceComplaintsFragment(), "mo_cover").commit();
            navigationView.getMenu().getItem(0).setChecked(true);

        }

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
    }




    public void showDrawerToggle(boolean showDrawerIndicator, String lname, final String fragment) {
        toggle.setDrawerIndicatorEnabled(showDrawerIndicator);
        toggle.setHomeAsUpIndicator(R.drawable.baseline_arrow_back_white_18);


        if (fragment.equals("most"))
            textView.setText("Complaint (most lodged)");
        else
            textView.setText("Complaint from " + lname);

        imageView.setVisibility(View.GONE);

        if (myMenu != null) {
            myMenu.setGroupVisible(R.id.mo_menu_group, true);
            myMenu.setGroupVisible(R.id.mo_menu_group1, false);
        }

        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!toggle.isDrawerIndicatorEnabled()) {

//                    myMenu.setGroupVisible(R.id.mo_menu_group, false);

//                    myMenu.setGroupVisible(R.id.mo_menu_group, false);
//                    myMenu.setGroupVisible(R.id.mo_menu_group1, true);
//                    textView.setText("All Complaints");
//                    imageView.setVisibility(View.VISIBLE);

                    toggle.setDrawerIndicatorEnabled(true);
//                    listView.setVisibility(View.VISIBLE);

                    MaintenanceComplaintsFragment maintenanceComplaintsFragment = (MaintenanceComplaintsFragment) getSupportFragmentManager().findFragmentByTag("mo_cover");
                    MaintenanceNoteFragment maintenanceNoteFragment = (MaintenanceNoteFragment) getSupportFragmentManager().findFragmentByTag("mo_noted");
                    MaintenanceSentFragment maintenanceSentFragment = (MaintenanceSentFragment) getSupportFragmentManager().findFragmentByTag("mo_sent");
                    MaintenanceArchiveFragment maintenanceArchiveFragment = (MaintenanceArchiveFragment) getSupportFragmentManager().findFragmentByTag("mo_fixed");
                    TrendingFragment trendingFragment = (TrendingFragment) getSupportFragmentManager().findFragmentByTag("mo_most");

                    MaintenanceComplaintView maintenanceComplaintView = (MaintenanceComplaintView) getSupportFragmentManager().findFragmentByTag("mo_view");
                    maintenanceComplaintView.closeFrag();

                    if (fragment.equals("all")) {
                        textView.setText("All Complaints");
                        myMenu.setGroupVisible(R.id.mo_menu_group, false);
                        myMenu.setGroupVisible(R.id.mo_menu_group1, true);
                        imageView.setVisibility(View.VISIBLE);

                        maintenanceComplaintsFragment.visible();
                    }

                    else if (fragment.equals("fixed")) {
                        textView.setText("Fixed");
                        maintenanceArchiveFragment.visible();
                        imageView.setVisibility(View.GONE);
                        }

                    else if (fragment.equals("noted")) {

                        maintenanceNoteFragment.visible();
                        textView.setText("Noted");
                        imageView.setVisibility(View.GONE);

                    }
                    else if (fragment.equals("sent")) {
                        maintenanceSentFragment.visible();
                        textView.setText("Sent");
                        imageView.setVisibility(View.GONE);

                    }
                    else{
                        trendingFragment.visible();
                        textView.setText("Most Lodged");
                        imageView.setVisibility(View.GONE);

                    }

                }

            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
//
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        menuItem = menu.findItem(R.id.action_settings);

        return true;
    }

//    @Override
//    public boolean onPrepareOptionsMenu(Menu menu) {
//        super.onPrepareOptionsMenu(menu);
//
//        if (toggle.isDrawerIndicatorEnabled())
//            menu.setGroupVisible(R.id.mo_menu_group, false);
//        else {
//            menu.setGroupVisible(R.id.mo_menu_group, true);
//            menu.setGroupVisible(R.id.mo_menu_group1, false);
//        }
//
//
//        return true;
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.maintenance_drawer, menu);
        myMenu = menu;
//        menuItem = menu.findItem(R.id.action_settings);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_staff_only) {
            textView.setText("Staff only");
            editor.putString("mo_pref_type", "staff");
            editor.commit();

            getSupportFragmentManager().beginTransaction()
                    //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                    .replace(R.id.content_maintenance_drawer,new MaintenanceComplaintsFragment(), "mo_cover")
                    .addToBackStack(null)
                    .commit();


        }

        if (id == R.id.action_student_only) {
            textView.setText("Student only");
            editor.putString("mo_pref_type", "student");
            editor.commit();

            getSupportFragmentManager().beginTransaction()
                    //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                    .replace(R.id.content_maintenance_drawer,new MaintenanceComplaintsFragment(), "mo_cover")
                    .addToBackStack(null)
                    .commit();
        }

        if (id == R.id.action_staff_student) {
            textView.setText("All Complaints");
            editor.putString("mo_pref_type", null);
            editor.commit();

            getSupportFragmentManager().beginTransaction()
                    //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                    .replace(R.id.content_maintenance_drawer,new MaintenanceComplaintsFragment(), "mo_cover")
                    .addToBackStack(null)
                    .commit();
        }

        if (id == R.id.mo_menu_setting) {
            startActivity(new Intent(this, SettingsActivity2.class));

        }

//        if (id == R.id.action_overview) {
//
//            textView.setText("Overview");
//
//            MaintenanceComplaintOverviewFragment maintenanceComplaintOverviewFragment = new MaintenanceComplaintOverviewFragment();
//            getSupportFragmentManager().beginTransaction()
//                    //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
//                    .replace(R.id.content_maintenance_drawer,maintenanceComplaintOverviewFragment)
//                    .addToBackStack(null)
//                    .commit();
//            return true;
//
//        }
//
//        if (id == R.id.action_all_complaints) {
//
//            textView.setText("All Complaints");
//
//            MaintenanceComplaintsFragment maintenanceComplaintsFragment = new MaintenanceComplaintsFragment();
//            getSupportFragmentManager().beginTransaction()
//                    //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
//                    .replace(R.id.content_maintenance_drawer,maintenanceComplaintsFragment)
//                    .addToBackStack(null)
//                    .commit();
//            return true;
//
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

//        TextView textView = (TextView) findViewById(R.id.mo_toobar_title);


        if (id == R.id.mo_complaint) {

            if (myMenu != null) {
                myMenu.setGroupVisible(R.id.mo_menu_group, false);
                myMenu.setGroupVisible(R.id.mo_menu_group1, true);
            }

            textView.setText("All Complaints");
            imageView.setVisibility(View.VISIBLE);

//            MaintenanceComplaintsFragment maintenanceComplaintsFragment = new MaintenanceComplaintsFragment();

            getSupportFragmentManager().beginTransaction()
                    //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                    .replace(R.id.content_maintenance_drawer,new MaintenanceComplaintsFragment(), "mo_cover")
                    .addToBackStack("mo_cover")
                    .commit();

        } else if (id == R.id.mo_most_lodged) {

//            textView.setText("Most Lodged Complaints");
//
//            MaintenanceMostLodgedFragment maintenanceMostLodgedFragment = new MaintenanceMostLodgedFragment();
//            getSupportFragmentManager().beginTransaction()
//                    //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
//                    .replace(R.id.content_home,maintenanceMostLodgedFragment)
//                    .addToBackStack(null)
//                    .commit();

            if (myMenu != null) {
                myMenu.setGroupVisible(R.id.mo_menu_group, true);
                myMenu.setGroupVisible(R.id.mo_menu_group1, false);
            }

            textView.setText("Most Lodged Complaints");
            imageView.setVisibility(View.GONE);

            getSupportFragmentManager().beginTransaction()
                    //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                    .replace(R.id.content_maintenance_drawer,new TrendingFragment(), "mo_most")
                    .addToBackStack(null)
                    .commit();


        } else if (id == R.id.mo_sent) {

            if (myMenu != null) {
                myMenu.setGroupVisible(R.id.mo_menu_group, true);
                myMenu.setGroupVisible(R.id.mo_menu_group1, false);
            }

            textView.setText("Sent");
            imageView.setVisibility(View.GONE);
            getSupportFragmentManager().beginTransaction()
                    //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                    .replace(R.id.content_maintenance_drawer,new MaintenanceSentFragment(), "mo_sent")
                    .addToBackStack("mo_sent")
                    .commit();


        } else if (id == R.id.mo_imp) {

            if (myMenu != null) {
                myMenu.setGroupVisible(R.id.mo_menu_group, true);
                myMenu.setGroupVisible(R.id.mo_menu_group1, false);
            }

            textView.setText("Noted");
            imageView.setVisibility(View.GONE);
            getSupportFragmentManager().beginTransaction()
                    //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                    .replace(R.id.content_maintenance_drawer, new MaintenanceNoteFragment(), "mo_noted")
                    .addToBackStack("mo_noted")
                    .commit();

        } else if (id == R.id.mo_fixed) {

            if (myMenu != null) {
                myMenu.setGroupVisible(R.id.mo_menu_group, true);
                myMenu.setGroupVisible(R.id.mo_menu_group1, false);
            }

            textView.setText("Fixed");
            imageView.setVisibility(View.GONE);

            getSupportFragmentManager().beginTransaction()
                    //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                    .replace(R.id.content_maintenance_drawer, new MaintenanceArchiveFragment(), "mo_fixed")
                    .addToBackStack("mo_fixed")
                    .commit();

        } else if (id == R.id.mo_settings) {
            startActivity(new Intent(this, SettingsActivity2.class));

        } else if (id == R.id.mo_logout) {

            builder = new AlertDialog.Builder(this);
            builder.setTitle("Confirm");
            builder.setMessage("Are you sure you want to log out");
            builder.setCancelable(false);
            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {

                            mAuth.signOut();
                            editor.clear();
                            editor.commit();

                            Intent intent = new Intent(MaintenanceDrawer.this, MainActivity.class);
                            startActivity(intent);

                        }
                    });
            builder.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            // code to run when Cancel is pressed
                        }
                    });

            AlertDialog alert = builder.create();
            alert.show();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void mo_toolbar_click(View view) {
            PopupMenu popup = new PopupMenu(this, view);
            popup.setOnMenuItemClickListener(this);
            popup.inflate(R.menu.mo_actionbar1);
            popup.show();

    }


    @Override
    public boolean onMenuItemClick(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_ground:
                textView.setText("Ground Floor");
                editor.putString("pref_location", "Ground Floor");
                editor.commit();

                getSupportFragmentManager().beginTransaction()
                //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                        .replace(R.id.content_maintenance_drawer, new MaintenanceComplaintsFragment(), "mo_cover")
                        .addToBackStack(null)
                        .commit();


                break;

            case R.id.action_first:
                textView.setText("1st Floor");
                editor.putString("pref_location", "1st Floor");
                editor.commit();

                getSupportFragmentManager().beginTransaction()
                        //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                        .replace(R.id.content_maintenance_drawer,new MaintenanceComplaintsFragment(), "mo_cover")
                        .addToBackStack(null)
                        .commit();


                break;

            case R.id.action_second:
                textView.setText("2nd Floor");
                editor.putString("pref_location", "2nd Floor");
                editor.commit();

                getSupportFragmentManager().beginTransaction()
                        //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                        .replace(R.id.content_maintenance_drawer, new MaintenanceComplaintsFragment(), "mo_cover")
                        .addToBackStack(null)
                        .commit();

                break;


            case R.id.action_third:
                textView.setText("3rd Floor");
                editor.putString("pref_location", "3rd Floor");
                editor.commit();

                getSupportFragmentManager().beginTransaction()
                        //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                        .replace(R.id.content_maintenance_drawer, new MaintenanceComplaintsFragment(), "mo_cover")
                        .addToBackStack(null)
                        .commit();

                break;


            case R.id.action_theater:
                textView.setText("Lecture Theater Area");
                editor.putString("pref_location", "Lecture Theater Area");
                editor.commit();

                getSupportFragmentManager().beginTransaction()
                        //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                        .replace(R.id.content_maintenance_drawer, new MaintenanceComplaintsFragment(), "mo_cover")
                        .addToBackStack(null)
                        .commit();

                break;


            case R.id.action_surrounding:
                textView.setText("Faculty Surrounding");
                editor.putString("pref_location", "Faculty Surrounding");
                editor.commit();

                getSupportFragmentManager().beginTransaction()
                        //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                        .replace(R.id.content_maintenance_drawer, new MaintenanceComplaintsFragment(), "mo_cover")
                        .addToBackStack(null)
                        .commit();

                break;

            case R.id.action_all:
                textView.setText("All Complaints");
                editor.putString("pref_location", null);
                editor.commit();

                getSupportFragmentManager().beginTransaction()
                        //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                        .replace(R.id.content_maintenance_drawer,new MaintenanceComplaintsFragment(), "mo_cover")
                        .addToBackStack(null)
                        .commit();
        }
        return false;
    }



//    public void setOnDataListener(FragmentInterface fragmentInterface){
//        fragmentInterfaceListener = fragmentInterface;
//    }
//
//    public interface FragmentInterface {
//        void sendDataMethod();
//    }



}
