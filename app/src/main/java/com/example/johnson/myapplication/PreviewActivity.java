package com.example.johnson.myapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.lang.Object;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class PreviewActivity extends AppCompatActivity {

    private SharedPreferences sharedPreferences;
    private  AlertDialog.Builder builder;
    TextView textView_name, textView_nature, textView_facility, textView_location;

    private FirebaseAuth mAuth;
    private DatabaseReference firebaseDatabase;
    Complaint complaint;
    Complaint_User complaint_user;

    Most_Lodged most_lodged;
    private ProgressDialog progressDialog;
    String complaint_nature, complaint_id, location_floor, location_area, lodged_by, facility_name, facility_type, mode, mofficer_remark = "null";
    int staff = 0; int student;


//    HashMap<String, HashMap<String, String>> complaint_description;

    HashMap<String, String> map, joined, user, seen_by;

//    Long total_no = 1L, staff = 0L, student = 0L;
    boolean new_facility, isNew = false;
    HashMap <String, Object> location;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        firebaseDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();

        firebaseDatabase.child("complaints").keepSynced(true);
        firebaseDatabase.child("most_lodged").keepSynced(true);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mode = sharedPreferences.getString("mode", null);

        new_facility = sharedPreferences.getBoolean("new_facility", false);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Sending...");


//        complaint_description = new HashMap<>();
//        map = new HashMap<>();

        location = new HashMap();

        Intent intent = getIntent();

        if (new_facility == false)
            facility_type = intent.getStringExtra("facility_type");
        else {
            facility_type = "null";
            isNew = true;
        }

        facility_name = intent.getStringExtra("facility_name");
        location_floor = intent.getStringExtra("location_floor");
        location_area = intent.getStringExtra("location_area");
        complaint_nature = intent.getStringExtra("complaint_nature");

        if (mode.equals("student"))
            student++;
        else
            staff++;


        String name, prank = null;

        if (sharedPreferences.getString("mode",null).equals("staff")) {

            String prefix = sharedPreferences.getString("staff_rank", null);
            int prefix_value = Integer.parseInt(prefix);
            prefix_value = prefix_value - 1;


            String[] texts = getResources().getStringArray(R.array.pref_staff_rank_lists);
            prank = texts[prefix_value] + " ";

            name = prank + sharedPreferences.getString("first_name", null) + " " +
                    sharedPreferences.getString("last_name", null);
        }
        else
            name = sharedPreferences.getString("first_name", null) + " " +
                sharedPreferences.getString("last_name", null);


        textView_name = (TextView) findViewById(R.id.complainant_name);
        textView_nature = (TextView) findViewById(R.id.complainant_nature);
        textView_facility = (TextView) findViewById(R.id.complaint_facility);
        textView_location = (TextView) findViewById(R.id.complainant_location);

//        map.put("user_id", mAuth.getCurrentUser().getUid());
//        map.put("user_description", complaint_desc);

        textView_name.setText("Name of Complainant : " + name);
        textView_nature.setText("Nature of Complaint : " + complaint_nature);
//        textView_description.setText("Description of Complaint : " + complaint_desc);
        textView_location.setText("Location of Facility : " + location_floor + " | " + location_area);
        textView_facility.setText("Facility : " + facility_name);


        builder = new AlertDialog.Builder(this);

        map = new HashMap<>();
        map.put(mAuth.getCurrentUser().getUid(), "created");

        joined = new HashMap<>();
        seen_by = new HashMap<>();

        joined.put(mAuth.getCurrentUser().getUid(), "created");
        seen_by.put(mAuth.getCurrentUser().getUid(), "seen");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.preview_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }

        if (id == R.id.action_home_screen) {
            startActivity(new Intent(this, HomeActivity.class));
            return true;
        }

        if (id == android.R.id.home) {
            super.onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void preview_cancel (View view) {

        builder.setTitle("Alert");
        builder.setMessage("Are you sure you want to cancel complaint");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        PreviewActivity.this.finish();
                    }
                });
        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // code to run when Cancel is pressed
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public  void preview_proceed (View view) {

//        builder.setTitle("Confirm");
//        builder.setMessage("Are you sure you want to proceed to lodge complaint");
//        builder.setCancelable(false);
//        builder.setPositiveButton("OK",
//                new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int id) {
//
//                        Intent intent = new Intent(PreviewActivity.this, HomeActivity.class);
//                        intent.putExtra("startFrag", true);
//
//                        Toast.makeText(PreviewActivity.this, "Compliant Lodged.",
//                                Toast.LENGTH_SHORT).show();
//
//                        startActivity(intent);
//                    }
//                });
//        builder.setNegativeButton("Cancel",
//                new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int id) {
//                        // code to run when Cancel is pressed
//                    }
//                });
//        AlertDialog alert = builder.create();
//        alert.show();

        if (isOnline() == true) {

            progressDialog.show();

            Date date = new Date();

            complaint = new Complaint(complaint_id, complaint_nature, date.toString(), facility_name, facility_type, false, joined, isNew, false, false, location_area, location_floor, mode, mofficer_remark, seen_by, staff, student, mAuth.getCurrentUser().getUid());
//            most_lodged = new Most_Lodged(facility_name, facility_type, location_area, location_floor, staff, student, total_no);
            complaint_user = new Complaint_User(complaint_id);

            complaint.complaint_id = firebaseDatabase.child("complaints").push().getKey();
            String key = firebaseDatabase.child("user_complaints").child(mAuth.getCurrentUser().getUid()).push().getKey();

            location.put("complaints/" + complaint.complaint_id, complaint);
            location.put("user_complaints/" + mAuth.getCurrentUser().getUid() + "/" + key, complaint.complaint_id);

//
//            if (true) {
//                location.put("most_lodged/" + facility_name + "_" + location_area + "/staff", staff);
//                location.put("most_lodged/" + facility_name + "_" + location_area + "/student", student);
//                location.put("most_lodged/" + facility_name + "_" + location_area + "/total_no", total_no);
//            }
//
//            else
//            location.put("most_lodged/" +  facility_name + "_" + location_area, most_lodged);

//            String complaint_desc_id = firebaseDatabase.child("complaints" + complaint.complaint_id + "complaint_description").push().getKey();
//            complaint_description.put(complaint_desc_id, map);


            firebaseDatabase.updateChildren(location, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    if (databaseError != null) {
                        Log.v("TAG", "Towoju " + databaseError);
                    }


                    builder.setTitle("Successful!");
                    builder.setMessage("Your Complaint has been lodged");
                    builder.setCancelable(false);
                    builder.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {

                                    Intent intent = new Intent(PreviewActivity.this, HomeActivity.class);
                                    intent.putExtra("startFrag", true);

                                    startActivity(intent);
                                }
                            });

                    progressDialog.dismiss();
                    AlertDialog alert = builder.create();
                    alert.show();

                }
            });

//            firebaseDatabase
//                    .child("complaints")
//                    .child(complaint.complaint_id)
//                    .setValue(complaint)
//                    .addOnSuccessListener(new OnSuccessListener<Void>() {
//                        @Override
//                        public void onSuccess(Void aVoid) {
//
//
//                            progressDialog.dismiss();
//
//                            builder.setTitle("Successful!");
//                            builder.setMessage("Your Complaint has been lodged");
//                            builder.setCancelable(false);
//                            builder.setPositiveButton("OK",
//                                    new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int id) {
//
//                                            Intent intent = new Intent(PreviewActivity.this, HomeActivity.class);
//                                            intent.putExtra("startFrag", true);
//
//                                            startActivity(intent);
//                                        }
//                                    });
//                            builder.setNegativeButton("Cancel",
//                                    new DialogInterface.OnClickListener() {
//                                        @Override
//                                         public void onClick(DialogInterface dialog, int id) {
//                                            // code to run when Cancel is pressed
//                                        }
//                                    });
//                            AlertDialog alert = builder.create();
//                            alert.show();
//                        }
//
//
//                    })
//                    .addOnFailureListener(new OnFailureListener() {
//                        @Override
//                        public void onFailure(@NonNull Exception e) {
//                            progressDialog.dismiss();
//                            Toast.makeText(PreviewActivity.this, "" + e,
//                                    Toast.LENGTH_SHORT).show();
//
//                        }
//                    });
//        Toast.makeText(PreviewActivity.this, "Successful! Compliant Lodged.",
//                                Toast.LENGTH_SHORT).show();


        } else
            Toast.makeText(PreviewActivity.this, "No Internet Connection",
                    Toast.LENGTH_SHORT).show();
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

}
