package com.example.johnson.myapplication;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

public class Homepage_Student_Staff extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Toolbar toolbar = (Toolbar) findViewById(R.id.main_toolbar); // get the reference of Toolbar
        setSupportActionBar(toolbar); // Setting/replace toolbar as the ActionBar

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage__student__staff);
    }
}
