package com.example.johnson.myapplication;

/**
 * Created by Johnson on 22/04/2018.
 */


public class ObjectItem_floors {

    public String complaint_header;
    public String complaint_nature;
    public String complaint_number;
    public String complaint_number_overview;



    // constructor
    public ObjectItem_floors(String complaint_header, String complaint_nature, String complaint_number, String complaint_number_overview) {
        this.complaint_header = complaint_header;
        this.complaint_nature = complaint_nature;
        this.complaint_number = complaint_number;
        this.complaint_number_overview = complaint_number_overview;

    }


}
