package com.example.johnson.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

import stanford.androidlib.SimpleActivity;

public class Registration extends SimpleActivity {

    private static final String Staff_id = "cisstaffid";
    private static final String Student_id = "cisstudentid";
    private String user_info;
    private User user1;
    private View hideKeyboard;



    private EditText editText;
    private EditText editText2;

    private ProgressBar progressBar;

    private static final String TAG = "Registration Activity";
    private FirebaseAuth mAuth;
//    private FirebaseAuth.AuthStateListener mAuthListener;
    //ProgressBar mProgressBar= new ProgressBar(this, null, android.R.attr.progressBarStyleHorizontal);

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        mAuth = FirebaseAuth.getInstance();
//        mAuthListener = new FirebaseAuth.AuthStateListener() {
//            @Override
//            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
//                FirebaseUser user = firebaseAuth.getCurrentUser();
//                if (user != null) {
//                    // User is signed in
//                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
//                } else {
//                    // User is signed out
//                    Log.d(TAG, "onAuthStateChanged:signed_out");
//                }
//                // ...
//            }
//        };

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        editText = (EditText) findViewById(R.id.user_email);
        editText2 = (EditText) findViewById(R.id.user_password);
        progressBar = (ProgressBar) findViewById(R.id.home_progress_bar);

        editText.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                hideKeyboard = view;
                return false;
            }

        });

        editText2.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                hideKeyboard = view;
                return false;
            }

        });

    }

//    public void onStart() {
//        super.onStart();
//        mAuth.addAuthStateListener(mAuthListener);
//
//    }
//
//    @Override
//    public void onStop() {
//        super.onStop();
//        if (mAuthListener != null) {
//            mAuth.removeAuthStateListener(mAuthListener);
//        }
//
//    }

    public void registration_submit(final View view) {
        EditText username = $ET(R.id.user_email);
        EditText password = $ET(R.id.user_password);

        final String real_username = username.getText().toString();
        final String real_password = password.getText().toString();

        //do some strip lashes

        hideKeyboardMethod(hideKeyboard);
        progressBar.setVisibility(View.VISIBLE);

            mAuth.createUserWithEmailAndPassword(real_username, real_password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            //mProgressBar.setVisibility(View.VISIBLE);
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                FirebaseUser user = mAuth.getCurrentUser();
                                //Log.d(TAG, " " + task.isSuccessful());

////                                user1 = new User("john", "kolo", "tt@gmail.com", 7);
////                                user1.id =FirebaseAuth.getInstance().getCurrentUser().getUid();
//
//                                user1.id =user.getUid();
//
//                                FirebaseDatabase.getInstance()
//                                        .getReference()
//                                        .child("users")
//                                        .child(user1.id)
//                                        .setValue(user1);
//
//                                    toast("An account has been created");

                                Intent intent = new Intent(Registration.this, Profile.class);
                                intent.putExtra("password", real_password);
                                intent.putExtra("email", real_username);
                                startActivity(intent);

                            }

                            else {
                                // If sign in fails, display a message to the user.

                                progressBar.setVisibility(View.GONE);
                                Log.v(TAG, "createUserWithEmail:failure" + task.getException());
                                toast("Unsuccessful Registration!");
                            }

                            // ...

                        }
                    });
    }


    public void hideKeyboardMethod (View view) {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (view != null)
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}
