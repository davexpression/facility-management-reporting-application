package com.example.johnson.myapplication;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.johnson.myapplication.TabsPagerAdapter.TabsPagerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private FirebaseAuth mAuth;
    private ViewPager viewPager;
    private TabsPagerAdapter myAdapter;
    private Boolean bool = false;
    private Intent intent;
    private  NavigationView navigationView;
    private  AlertDialog.Builder builder;
    private ImageView imageView;
    private SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private String mode, phone_number;
    Dialog dialog;
    EditText editText;
    LayoutInflater layoutInflater;
    TextView textView, textView1, textView_title;
    Bundle bundle;
    Menu myMenu, menuGlobal;
    String name;

    DatabaseReference fb;
    DatabaseReference user;

    int prefix_value;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        mAuth = FirebaseAuth.getInstance();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sharedPreferences.edit();

        mode = sharedPreferences.getString("mode", null);

        fb = FirebaseDatabase.getInstance().getReference();
        user = fb.child("users");
        user.keepSynced(true);

        user.child("JxXnDb84v6bphQfKUAsDGv7QPRI2").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                User user = dataSnapshot.getValue(User.class);
                phone_number = user.phone_number;
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        layoutInflater = getLayoutInflater();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        intent = getIntent();
        bool = intent.getBooleanExtra("startFrag", false);

        String prank = null;
        String mode = sharedPreferences.getString("mode",null);
        textView_title = (TextView) findViewById(R.id.toobar_title);

//        if (mode.equals("staff")) {
//
//            String prefix = sharedPreferences.getString("staff_rank", null);
//
//            prefix_value = Integer.parseInt(prefix);
//
//            prefix_value = prefix_value - 1;
//
//            String[] texts = getResources().getStringArray(R.array.pref_staff_rank_lists);
//            prank = texts[prefix_value] + " ";
//
//            name = prank + sharedPreferences.getString("first_name", null) + " " +
//                    sharedPreferences.getString("last_name", null);
//        }
//
//        else
//            name = sharedPreferences.getString("first_name", null) + " " +
//                    sharedPreferences.getString("last_name", null);

        user.orderByKey().equalTo(mAuth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot data : dataSnapshot.getChildren()) {

                    User user = data.getValue(User.class);

                    if (user.type.equals("student"))
                        name = user.first_name + " " + user.last_name;
                    else
                        name = user.prefix + " " + user.first_name + " " + user.last_name;

                }

                textView.setText(name);
                textView1.setText(mAuth.getCurrentUser().getEmail());

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        if (savedInstanceState == null) {

            getSupportFragmentManager().beginTransaction().replace(R.id.content_home, new BlankFragment()).commit();
            navigationView.getMenu().getItem(0).setChecked(true);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.main1_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        View view = navigationView.getHeaderView(0);

        textView = (TextView) view.findViewById(R.id.nav_name);
        textView1  = (TextView) view.findViewById(R.id.nav_email);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (bool == true) {
            textView_title.setText("My Complaints");
            MyComplaintsFragment myComplaintsFragment = new MyComplaintsFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.content_home, myComplaintsFragment).commit();
            intent.removeExtra("startFrag");
            bool = false;

            navigationView.getMenu().getItem(1).setChecked(true);
        }


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (textView_title.getText().toString().equals("Lodge Complaint"))
            return;

        else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu-
        // ; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.homepage, menu);
        myMenu = menu;


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }

        if (id == R.id.action_new_facilty) {

            builder = new AlertDialog.Builder(this);
            final View view = layoutInflater.inflate(R.layout.new_facility_dialog, null);

            builder.setTitle("Facility Name");
            builder.setView(view);

            final EditText editText = (EditText) view.findViewById(R.id.new_facility_text);

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    Intent intent = new Intent(HomeActivity.this, FacilityScreen.class);
                    intent.putExtra("facility_name", editText.getText().toString());
                    editor.putBoolean("new_facility", true);
                    editor.commit();
                    startActivity(intent);

                }
            });

            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            AlertDialog alert = builder.create();
            alert.show();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

//      Fragment fragment = null;

//        imageView = (ImageView) findViewById(R.id.search_action_bar);

//        if (id == R.id.trending) {
//
//            textView.setText("Most Lodged Complaints");
//
//            TrendingFragment trendingFragment = new TrendingFragment();
//            getSupportFragmentManager().beginTransaction()
//                    //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
//                    .replace(R.id.content_home,trendingFragment)
//                    .addToBackStack(null)
//                    .commit();
//
//            imageView.setVisibility(View.GONE);
//        }

        if (id == R.id.lodge_complaint) {

            textView_title.setText("Lodge Complaint");
            myMenu.findItem(R.id.action_new_facilty).setVisible(true);

            BlankFragment blankFragment = new BlankFragment();
            getSupportFragmentManager().beginTransaction()
                    //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                    .replace(R.id.content_home,blankFragment)
                    .addToBackStack(null)
                    .commit();

//            imageView.setVisibility(View.VISIBLE);




        } else if (id == R.id.my_complaints) {

            textView_title.setText("My Complaints");
            MyComplaintsFragment myComplaintsFragment = new MyComplaintsFragment();

            getSupportFragmentManager().beginTransaction()
                    //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                    .replace(R.id.content_home,myComplaintsFragment)
                    .addToBackStack(null)
                    .commit();

//            imageView.setVisibility(View.GONE);
        }

        else if (id == R.id.fixed_complaints) {

            textView_title.setText("Fixed Complaints");
            myMenu.findItem(R.id.action_new_facilty).setVisible(false);

            FixedComplaintFragment fixedComplaintFragment = new FixedComplaintFragment();
            getSupportFragmentManager().beginTransaction()
                    //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                    .replace(R.id.content_home,fixedComplaintFragment)
                    .addToBackStack(null)
                    .commit();

//            imageView.setVisibility(View.GONE);
        }

        else if (id == R.id.logout) {
            builder = new AlertDialog.Builder(this);

            builder.setTitle("Confirm");
            builder.setMessage("Are you sure you want to log out");
            builder.setCancelable(false);
            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {

                            mAuth.signOut();
                            editor.clear();
                            editor.commit();

                            Intent intent = new Intent(HomeActivity.this, MainActivity.class);
                            startActivity(intent);

                        }
                    });
            builder.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            // code to run when Cancel is pressed
                        }
                    });

            AlertDialog alert = builder.create();
            alert.show();

        }

        else if (id == R.id.settings) {
            startActivity(new Intent(this, SettingsActivity.class));
        }

        else if (id == R.id.call_maintenance) {
            builder = new AlertDialog.Builder(this);

            final int MY_PERMISSION_ACCESS_CALL_PHONE= 100;
            // permission request in on_create
//            if (ContextCompat.checkSelfPermission(this,
//                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//                ActivityCompat.requestPermissions(
//                        this, new String[]{Manifest.permission.CALL_PHONE},
//                        MY_PERMISSION_ACCESS_CALL_PHONE);
//            }
//            else {
//
//                Intent intent = new Intent(Intent.ACTION_CALL);
//                intent.setData(Uri.parse("tel:08132137527"));
//                startActivity(intent);
//            }

            builder.setTitle("Confirm");
            builder.setMessage("You are about to make a phone call to the Maintenance Officer");
            builder.setCancelable(false);
            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            if (ContextCompat.checkSelfPermission(HomeActivity.this,
                                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions(
                                        HomeActivity.this, new String[]{Manifest.permission.CALL_PHONE},
                                        MY_PERMISSION_ACCESS_CALL_PHONE);
                            }
                            else {

                                Intent intent = new Intent(Intent.ACTION_CALL);
                                intent.setData(Uri.parse("tel:" + phone_number));
                                startActivity(intent);
                            }

                        }
                    });
            builder.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            // code to run when Cancel is pressed
                        }
                    });

            AlertDialog alert = builder.create();
            alert.show();

        } else if (id == R.id.text_maintenance) {

            builder = new AlertDialog.Builder(this);

//            Intent sendIntent = new Intent(Intent.ACTION_VIEW);
//            sendIntent.setData(Uri.parse("sms:" + "08132137527"));
//            startActivity(sendIntent);

            builder.setTitle("Confirm");
            builder.setMessage("You are about to send a text to the Maintenance Officer");
            builder.setCancelable(false);
            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {

                            Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                            sendIntent.setData(Uri.parse("sms:" + phone_number));
                            startActivity(sendIntent);
                        }
                    });
            builder.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            // code to run when Cancel is pressed
                        }
                    });

            AlertDialog alert = builder.create();
            alert.show();
        }





//        if (fragment != null)
//            FragmentManager fragmentManager = getSupportFragmentManager();
//            fragmentManager.beginTransaction()
//                    .replace(R.id.frame_container, fragment).commit();
//
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    public void facility_click (View view) {
        LinearLayout linearLayout = (LinearLayout) findViewById(view.getId());
        ImageView imageView = (ImageView) linearLayout.getChildAt(0);
        TextView textView = (TextView) linearLayout.getChildAt(1);

        String facility_name = textView.getText().toString();
        int img_id = imageView.getId();
        //use shared pref

        SharedPreferences.Editor editor =
                PreferenceManager.getDefaultSharedPreferences(this).edit();

        editor.putInt("img_id", img_id);
        editor.putBoolean("new_facility", false);
        editor.commit();

        Intent intent = new Intent(this, FacilityScreen.class);
        intent.putExtra("facility_name", facility_name);
        startActivity(intent);
    }
}
