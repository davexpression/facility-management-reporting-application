package com.example.johnson.myapplication;

/**
 * Created by Johnson on 22/04/2018.
 */


public class ObjectItem_real {

    public String header;
    public String detail;
    public String complaint_number;
    public String lecturer_number;
    public String student_number;
    public String location_text;
    public String date_text;



    // constructor
    public ObjectItem_real(String header, String detail, String more_detail, String complaint_number, String lecturer_number, String student_number, String location_text) {
        this.header = header;
        this.detail = detail;
//        this.more_detail = more_detail;
        this.complaint_number = complaint_number;
        this.lecturer_number = lecturer_number;
        this.student_number = student_number;
        this.location_text = location_text;

    }


}
