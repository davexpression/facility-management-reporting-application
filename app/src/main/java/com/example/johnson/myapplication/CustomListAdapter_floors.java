package com.example.johnson.myapplication;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Johnson on 15/04/2018.
 */

public class CustomListAdapter_floors extends ArrayAdapter <ObjectItem_floors> {

    //to reference the Activity
    private final Activity context;

    private ArrayList<ObjectItem_floors> data = null;
    private boolean[] viewVisibility = null;


    public CustomListAdapter_floors(Activity context, int layout, ArrayList<ObjectItem_floors> data) {

        super(context, layout, data);

        this.context = context;
        this.data = data;
        this.viewVisibility = new boolean[data.size()];

    }

    public View getView(final int position,  View view, ViewGroup parent) {


        if (view == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            view = inflater.inflate(R.layout.my_complaints, parent, false);
        }


        //this code sets the values of the objects to values from the arrays
        final ObjectItem_floors objectItem_floors = data.get(position);

        //this code gets references to objects in the listview_row.xml file
        TextView headerTextField = (TextView) view.findViewById(R.id.my_complaints_floors_header);
        TextView natureTextField = (TextView) view.findViewById(R.id.my_complaints_floors_nature);
        TextView numberTextField = (TextView) view.findViewById(R.id.my_complaints_floors_number);
        TextView numberOverviewTextField = (TextView) view.findViewById(R.id.my_complaints_floors_overview);

//        final TextView textView = (TextView) view.findViewById(R.id.my_complaints_list_detail2);
//
//
//        if(viewVisibility[position]) {
//            textView.setVisibility(View.VISIBLE);
//        } else {
//            textView.setVisibility(View.GONE);
//        }
//
//        ImageView imageView = (ImageView) view.findViewById(R.id.my_complaints_expand);

        headerTextField.setText(objectItem_floors.complaint_header);
        natureTextField.setText(objectItem_floors.complaint_nature);
        numberTextField.setText(objectItem_floors.complaint_number);
        numberOverviewTextField.setText(objectItem_floors.complaint_number_overview);

//        imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                    textView.setVisibility(View.VISIBLE);
//                    viewVisibility[position] = true;
//                }
//        });
        return view;

    };
}
