package com.example.johnson.myapplication;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

public class Profile extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

//    public static final String TYPE_OF_USER = "MyPrefsFile";
    private String mode;
    private SharedPreferences sharedPreferences;

    private Spinner spinner;
    private View hideKeyboard;
    private EditText editText;
    private EditText editText2;
    SharedPreferences.Editor editor;

    private  AlertDialog.Builder builder;

    private ProgressBar progressBar;


    private EditText office_identifier_editText;
    private EditText matric_editText;
    String selectedTextPosition;
    String selectedTextPosition2;

    private String fname, lname, email, password, matric, office_floorNo, office_location, office_identifier, prefix, prefixNo, user_id;

    private  Spinner spinner2;
//    private  EditText loc_editText;
    private FirebaseAuth mAuth;

    User user;



    private static final String[] staff_rank = {"Prefix", "Professor (Mr)", "Professor (Mrs)", "Dr. (Mr)",
            "Dr. (Mrs)", "Mr", "Mrs", "Miss"};

    private static final String[] floor = {"Select Office Floor", "Ground Floor", "1st Floor", "2nd Floor",
            "3rd Floor", "Lecture Theater Area"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        mAuth = FirebaseAuth.getInstance();


        Intent intent = getIntent();
        password = intent.getStringExtra("password");
        email = intent.getStringExtra("email");

        TextView textView = (TextView) findViewById(R.id.office_details);
        progressBar = (ProgressBar) findViewById(R.id.home_progress_bar);

        builder = new AlertDialog.Builder(this);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mode = sharedPreferences.getString("mode", null);
        editor = sharedPreferences.edit();
        editor.clear();


        editText = (EditText) findViewById(R.id.profile_fname);
        editText2 = (EditText) findViewById(R.id.profile_lname);

        spinner = (Spinner) findViewById(R.id.profile_spinner_rank);
        matric_editText = (EditText) findViewById(R.id.profile_matric_number);
        office_identifier_editText = (EditText) findViewById(R.id.location_identifier);

        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.staff_location_details);

        if (mode.equals("student")) {
            spinner.setVisibility(View.GONE);
            matric_editText.setVisibility(View.VISIBLE);
            linearLayout.setVisibility(View.GONE);
            textView.setVisibility(View.GONE);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(Profile.this,
                android.R.layout.simple_spinner_item, staff_rank) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else
                    return true;
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(this);

        spinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                hideKeyboardMethod(hideKeyboard);
                return  false;
            }

        });

        editText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                hideKeyboard = view;
                return false;
            }
        });

        editText2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                hideKeyboard = view;
                return false;
            }
        });



        spinner2 = (Spinner) findViewById(R.id.location_floor);
            ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(Profile.this,
                android.R.layout.simple_spinner_item, floor){
            @Override
            public boolean isEnabled(int position){
                if(position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    return true;
                }
            }
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };

        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter1);

        spinner2.setOnItemSelectedListener(this);

    }

    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {

        Spinner spinner = (Spinner)parent;
        if(spinner.getId() == R.id.profile_spinner_rank) {
            if(position > 0){

                prefixNo = selectedTextPosition = Integer.toString(position);
                prefix = spinner.getSelectedItem().toString();
            }
        }

        else if(spinner.getId() == R.id.location_floor) {
            if(position > 0){

                office_floorNo = selectedTextPosition2 = Integer.toString(position);
                office_location = spinner2.getSelectedItem().toString();
            }
        }

    }

    public void onNothingSelected(AdapterView<?> parent) {
        //
    }



    public void hideKeyboardMethod (View view) {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (view != null)
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void registration_submit (View view) {

//        SharedPreferences.Editor editor =
//                PreferenceManager.getDefaultSharedPreferences(this).edit();
//        Intent intent;
//
//        if(mode.equals("staff")) {
//            intent = new Intent(this, Location.class);
//            editor.putString("staff_rank",selectedTextPosition);
//            editor.commit();
//        }
//
//        else
//            intent = new Intent(this, HomeActivity.class);
//
//
//        String fname = editText.getText().toString();
//        String lname = editText2.getText().toString();
//        String matric_no = matric_editText.getText().toString();
//
//        editor.putString("first_name",fname);
//        editor.putString("last_name",lname);
//        editor.putString("matric_no", matric_no);
//        editor.commit();
//
//        startActivity(intent);


        FirebaseUser firebaseUser = mAuth.getCurrentUser();

        hideKeyboardMethod(hideKeyboard);
        progressBar.setVisibility(View.VISIBLE);

        fname = editText.getText().toString();
        lname = editText2.getText().toString();


        if (mode.equals("student")) {
           matric = matric_editText.getText().toString();
            user = new User(email, fname, lname, matric, null, null, null, password, null, null, null, "student", user_id);
            editor.putString("matric_no", user.matric);

        }

        else {
            office_identifier = office_identifier_editText.getText().toString();
            user = new User(email, fname, lname, null, office_floorNo, office_identifier, office_location, password, null, prefix, prefixNo, "staff", user_id);
            editor.putString("office_floor", user.office_floorNo);
            editor.putString("office_identifier", user.office_identifier);
            editor.putString("staff_rank", user.prefixNo);
        }

        editor.putString("first_name", user.first_name);
        editor.putString("last_name", user.last_name);
        editor.putString("mode", user.type);
        editor.commit();


        user.user_id = FirebaseAuth.getInstance().getCurrentUser().getUid();
        FirebaseDatabase.getInstance()
                .getReference()
                .child("users")
                .child(user.user_id)
                .setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                builder.setTitle("Successful!");
                builder.setMessage("Successful! Your Account has been Created");
                builder.setCancelable(false);
                builder.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {

                                Intent intent = new Intent(Profile.this, HomeActivity.class);
                                startActivity(intent);
                            }
                        });

                AlertDialog alert = builder.create();
                progressBar.setVisibility(View.GONE);
                alert.show();
            }
        });

        }
}
