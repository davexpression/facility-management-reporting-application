package com.example.johnson.myapplication;


import java.util.Comparator;

/**
 * Created by Johnson on 22/04/2018.
 */


public class ObjectItem2 implements Comparator<ObjectItem2> {

    public String complaint_nature;
    public int complaint_number;

    public String location_text;
    public String date_text;
    public String complaint_id;
    public String complaint_remark;
    public String location_floor;
    public String last_name;

    boolean seen_by;

    public String join_total;

    public String name;
    public  boolean new_facility;

    public String facility;
    boolean joined;
    boolean creator;

    boolean sent;
    boolean fixed;
    boolean noted;

    int img_id;
    int joined_int;
    int staff;
    int student;

//    public static final Comparator<ObjectItem2> DESCENDING_COMPARATOR = new Comparator<ObjectItem2>() {
//        // Overriding the compare method to sort the age
//
//            @Override
//            public int compare(ObjectItem2 o1, ObjectItem2 o2) {
//                return o2.complaint_number - o1.complaint_number;
//            }
//    };

    // constructor
    public ObjectItem2(String complaint_nature, int complaint_number, String location_text) {
        this.complaint_nature = complaint_nature;
        this.complaint_number = complaint_number;
        this.location_text = location_text;

    }

    public void setComplaint_remark(String input) {
        this.complaint_remark = input;
    }

    public String getComplaint_remark() {
        return complaint_remark;
    }

    public void setComplaint_nature(String input) {
        this.complaint_nature = input;
    }

    public String getComplaint_nature() {
        return complaint_nature;
    }

    public void setComplaint_id(String input) {
        this.complaint_id = input;
    }

    public String getComplaint_id() {
        return complaint_id;
    }

    public void setLocation_floor(String input) {
        this.location_floor = input;
    }

    public String getLocation_floor() {
        return location_floor;
    }

    public void setLocation_text(String input) {
        this.location_text = input;
    }

    public String getLocation_text() {
        return location_text;
    }

    public void setJoined(boolean input) {
        this.joined = input;
    }

    public boolean isJoined() {
        return joined;
    }

    public void setCreator(boolean input) {
        this.creator = input;
    }

    public boolean isCreator() {
        return joined;
    }

    public void setImg_id(int input) {
        this.img_id = input;
    }

    public int getImg_id() {
        return img_id;
    }

    public void setComplaint_number(int input) {
        this.complaint_number = input;
    }

    public int getComplaint_number() {
        return complaint_number;
    }


    public void setDate_text(String input) { this.date_text = input; }

    public String getDate_text() { return date_text; }

    public void setFacility(String input) { this.facility = input; }

    public String getFacility() { return facility; }


    public void setJoined_int(int input) {
        this.joined_int = input;
    }

    public int getJoined_int() {
        return joined_int;
    }


    public void setStaff(int input) {
        this.staff = input;
    }

    public int getStaff() {
        return staff;
    }


    public void setStudent(int input) {
        this.student = input;
    }

    public int getStudent() {
        return student;
    }

    public void setName(String input) { this.name = input; }

    public String getName() { return name; }

    public void setJoin_total(String input) { this.join_total = input; }

    public String getJoin_total() { return join_total; }

    public void setNew_facility(boolean input) { this.new_facility = input; }

    public boolean getNew_facility() { return new_facility; }


    public void setSeen_by(boolean input) { this.seen_by = input; }

    public boolean isSeen_by() { return seen_by; }


    public void setLast_name(String input) { this.last_name = input; }

    public String getLast_name() { return last_name; }


    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean input) {
        this.sent = input;
    }


    public boolean isNoted() {
        return noted;
    }

    public void setNoted(boolean input) {
        this.noted = input;
    }


    public boolean isFixed() {
        return fixed;
    }

    public void setFixed(boolean input) {
        this.fixed = input;
    }


    @Override
    public int compare(ObjectItem2 o1, ObjectItem2 o2) {
        return o2.complaint_number - o1.complaint_number;
    }
}
