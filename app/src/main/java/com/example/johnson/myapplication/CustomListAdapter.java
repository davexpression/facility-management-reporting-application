package com.example.johnson.myapplication;

import android.app.Activity;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Johnson on 15/04/2018.
 */

public class CustomListAdapter extends ArrayAdapter <ObjectItem2> {

    //to reference the Activity
    private final Activity context;

    private ArrayList<ObjectItem2> data = null;
    private boolean[] viewVisibility = null;


    public CustomListAdapter (Activity context, int layout, ArrayList<ObjectItem2> data) {

        super(context, layout, data);

        this.context = context;
        this.data = data;
        this.viewVisibility = new boolean[data.size()];

    }

    public View getView(final int position,  View view, ViewGroup parent) {


        if (view == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            view = inflater.inflate(R.layout.my_complaints, parent, false);
        }


        //this code sets the values of the objects to values from the arrays
        final ObjectItem2 objectItem = data.get(position);

        Date myDate = null;
//

        try {
            java.text.DateFormat format = new SimpleDateFormat("EEE LLL dd HH:mm:ss Z yyyy");
            myDate = format.parse(objectItem.date_text);
        }
        catch (ParseException e){ }
        //this code gets references to objects in the listview_row.xml file

        String string = DateUtils.getRelativeDateTimeString(getContext(), myDate.getTime(), DateUtils.MINUTE_IN_MILLIS,
                DateUtils.DAY_IN_MILLIS * 4, DateUtils.FORMAT_ABBREV_RELATIVE).toString();

        string = string.substring(0, string.indexOf(","));

        //this code gets references to objects in the listview_row.xml file

        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.text_background);

        TextView headerTextField = (TextView) view.findViewById(R.id.my_complaints_list_header);
        TextView numberTextField = (TextView) view.findViewById(R.id.my_complaints_number);
        TextView locationField = (TextView) view.findViewById(R.id.my_complaints_location_text);
        TextView dateField = (TextView) view.findViewById(R.id.my_complaints_date);

//        final TextView textView = (TextView) view.findViewById(R.id.my_complaints_list_detail2);
//
//
//        if(viewVisibility[position]) {
//            textView.setVisibility(View.VISIBLE);
//        } else {
//            textView.setVisibility(View.GONE);
//        }
//
//        ImageView imageView = (ImageView) view.findViewById(R.id.my_complaints_expand);

        headerTextField.setText(objectItem.complaint_nature);
        numberTextField.setText(Integer.toString(objectItem.complaint_number));

        dateField.setText(string);
        locationField.setText(objectItem.location_text);

        Log.v("JoinFragment", "Towoju " + objectItem.joined);
        Log.v("JoinFragment", "Towoju " + objectItem.creator);

        if (objectItem.joined == true | objectItem.isCreator() == true) {
            linearLayout.setBackgroundResource(R.drawable.border7);
        }

//        imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                    textView.setVisibility(View.VISIBLE);
//                    viewVisibility[position] = true;
//                }
//        });
        return view;

    };
}
