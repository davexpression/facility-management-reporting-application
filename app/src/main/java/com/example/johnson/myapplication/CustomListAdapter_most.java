package com.example.johnson.myapplication;

import android.app.Activity;
import android.content.res.ColorStateList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Johnson on 15/04/2018.
 */

public class CustomListAdapter_most extends ArrayAdapter <ObjectItem_most> {

    //to reference the Activity
    private final Activity context;

    private ArrayList<ObjectItem_most> data = null;

    public CustomListAdapter_most(Activity context, int layout, ArrayList<ObjectItem_most> data) {

        super(context, layout, data);

        this.context = context;
        this.data = data;

    }

    public View getView(final int position,  View view, ViewGroup parent) {


        if (view == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            view = inflater.inflate(R.layout.most_lodged_complaints_detail, parent, false);
        }


        //this code sets the values of the objects to values from the arrays
        final ObjectItem_most objectItem = data.get(position);

        //this code gets references to objects in the listview_row.xml file

        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.text_background);
        TextView headerTextField = (TextView) view.findViewById(R.id.most_complaints_list_header);
        TextView numberTextField = (TextView) view.findViewById(R.id.most_complaints_number);

//        final TextView textView = (TextView) view.findViewById(R.id.my_complaints_list_detail2);
//
//
//        if(viewVisibility[position]) {
//            textView.setVisibility(View.VISIBLE);
//        } else {
//            textView.setVisibility(View.GONE);
//        }
//
//        ImageView imageView = (ImageView) view.findViewById(R.id.my_complaints_expand);

        headerTextField.setText(objectItem.complaint_nature);
        numberTextField.setText(Integer.toString(objectItem.complaint_number));

        if (objectItem.isJoined() == true) {
            linearLayout.setBackgroundResource(R.drawable.copyboder4_joined);
        }

//        imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                    textView.setVisibility(View.VISIBLE);
//                    viewVisibility[position] = true;
//                }
//        });
        return view;

    };
}
