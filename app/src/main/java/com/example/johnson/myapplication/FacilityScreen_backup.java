package com.example.johnson.myapplication;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;


public class FacilityScreen_backup extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    private View hideKeyboard;
    private SharedPreferences sharedPreferences;
    private TextView textView2;
    private EditText editText;
    private ImageView imageView;
    private String office_floor;
    private String office_identifier;
    private ColorStateList oldColors;
    private Spinner spinner;
    private  Intent intent;
    private ProgressBar progressBar;
    private int mine;
    private int img_id;
    private  int i;
    private CountDownTimer mCountDownTimer;
    private static final String[] paths = {"Ground Floor", "1st Floor", "2nd Floor",
            "3rd Floor", "Lecture Theater Area", "Faculty Surrounding"};

    Switch switchButton;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_facilty_screen);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        i=0;

        progressBar = (ProgressBar) findViewById(R.id.location_progress_bar);
        user = new User();


        //use shared pref
//        intent = getIntent();
//        img_id = intent.getIntExtra("img_id", 1);


        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        office_floor = sharedPreferences.getString("office_floor", null);

        int office_floor_value = Integer.parseInt(office_floor);
        office_floor_value = office_floor_value - 1;

        office_identifier = sharedPreferences.getString("office_identifier", null);

        String[] texts = getResources().getStringArray(R.array.pref_office_floor);
        final String office_floor_text = texts[office_floor_value];


        spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(FacilityScreen_backup.this,
                android.R.layout.simple_spinner_item, paths);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        spinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                hideKeyboardMethod(hideKeyboard);
                return  false;
            }

        });

        //for changing location
        textView2 = (TextView) findViewById(R.id.location_selected);
        imageView = (ImageView) findViewById(R.id.send);

        TextView textView_color = (TextView) findViewById(R.id.testing_test);
        oldColors =  textView_color.getTextColors(); //save original colors

        editText = (EditText) findViewById(R.id.enter_location);
        editText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                hideKeyboard = view;
                view.setFocusable(true);
                view.setFocusableInTouchMode(true);

                imageView.setVisibility(View.VISIBLE);


//                if (MotionEvent.ACTION_UP == motionEvent.getAction()) {
                    LinearLayout linearLayout = (LinearLayout) findViewById(R.id.ground_floor);

                    for (int i = 0; i < linearLayout.getChildCount(); i++) {
                        TextView textView = (TextView) linearLayout.getChildAt(i);

                        //treat only the sick ones

                        textView.setBackgroundColor(Color.TRANSPARENT);
                        textView.setTextColor(oldColors);
                    }
                return false;
            }
        });

        switchButton = (Switch) findViewById(R.id.my_switch);
        final LinearLayout linearLayout = (LinearLayout) findViewById(R.id.after_switch);
        final LinearLayout linearLayout2 = (LinearLayout) findViewById(R.id.currently_set_to);


        switchButton.setTextColor(Color.GRAY);
        linearLayout2.setVisibility(View.GONE);
        linearLayout.setVisibility(View.VISIBLE);


        switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {


                if(isChecked){
                    switchButton.setTextColor(Color.BLACK);
                    linearLayout2.setVisibility(View.VISIBLE);
                    textView2.setText(office_floor_text + " | " + office_identifier);
                    linearLayout.setVisibility(View.GONE);
                }

                else {
                    switchButton.setTextColor(Color.GRAY);
                    linearLayout2.setVisibility(View.GONE);
                    linearLayout.setVisibility(View.VISIBLE);
                }

            }
        });
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        intent = getIntent();
//        img_id = intent.getIntExtra("img_id", 1);
//    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }

        if (id == android.R.id.home) {
            super.onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {

        LinearLayout parent_LinearLayout = (LinearLayout) findViewById(R.id.suggestions);

        switch (position) {
            case 0:

                shutPrevVisibility(parent_LinearLayout);
                LinearLayout linearLayout1 = (LinearLayout) findViewById(R.id.ground_floor);
                linearLayout1.setVisibility(View.VISIBLE);
                break;

            case 1:

                shutPrevVisibility(parent_LinearLayout);
                LinearLayout linearLayout2 = (LinearLayout) findViewById(R.id.first_floor);
                linearLayout2.setVisibility(View.VISIBLE);
                break;
            case 2:

                shutPrevVisibility(parent_LinearLayout);
                LinearLayout linearLayout3 = (LinearLayout) findViewById(R.id.second_floor);
                linearLayout3.setVisibility(View.VISIBLE);
                break;

            case 3:

                shutPrevVisibility(parent_LinearLayout);
                LinearLayout linearLayout4 = (LinearLayout) findViewById(R.id.third_floor);
                linearLayout4.setVisibility(View.VISIBLE);
                break;

            case 4:

                shutPrevVisibility(parent_LinearLayout);
                LinearLayout linearLayout5 = (LinearLayout) findViewById(R.id.theater);
                linearLayout5.setVisibility(View.VISIBLE);
                break;

            case 5:

                shutPrevVisibility(parent_LinearLayout);
                LinearLayout linearLayout6 = (LinearLayout) findViewById(R.id.surrounding);
                linearLayout6.setVisibility(View.VISIBLE);
                break;

        }
    }

    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void floor_button (View view) {
        Button button = (Button) findViewById(view.getId());
        int position = Integer.parseInt(button.getTag().toString());
        spinner.setSelection(position);
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.floor_house);
        linearLayout.setVisibility(View.GONE);
        LinearLayout linearLayout2 = (LinearLayout) findViewById(R.id.selected_floor);
        linearLayout2.setVisibility(View.VISIBLE);
    }

    public void shutPrevVisibility(LinearLayout parent_LinearLayout){

        parent_LinearLayout.setVisibility(View.VISIBLE);
        for (int i=0; i<=5; i++) {
            LinearLayout linearLayout = (LinearLayout) parent_LinearLayout.getChildAt(i);

            if(linearLayout.getVisibility() == View.VISIBLE) {
                linearLayout.setVisibility(View.GONE);
            }
        }
    }

    public void text_view(View view) {

        editText.setFocusable(false);
        if(imageView.getVisibility() == View.VISIBLE)
            imageView.setVisibility(View.GONE);

        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.ground_floor);
        for (int i = 0; i < linearLayout.getChildCount(); i++) {

            TextView textView = (TextView) linearLayout.getChildAt(i);
            if(textView.getId() == view.getId()) {
                textView.setBackgroundColor(Color.parseColor("#3F51B5"));
                textView.setTextColor(Color.WHITE);
                String location_value = (String) textView.getText();

                textView2.setText(location_value);
                continue;
            }
            textView.setBackgroundColor(Color.TRANSPARENT);
            textView.setTextColor(oldColors);

        }

        hideKeyboardMethod(hideKeyboard);

    }

    public void send (View view) {
        String value = editText.getText().toString();
        textView2.setText(value);
        hideKeyboardMethod(hideKeyboard);
    }

    public void hideKeyboardMethod (View view) {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (view != null)
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

     public void location_proceed (View view) {


         if (switchButton.isChecked()) {
             String office_floor = user.office_location;
             String office_identifier = user.office_identifier;
         }

         else {

         }

//        Intent intent = new Intent(this, FacilityComplaint.class);
//        startActivity(intent);

//         Button button = (Button) findViewById(R.id.location_proceed);
//         final Button button1 = (Button) findViewById(R.id.proceed_anyway);
//         final LinearLayout linearLayout = (LinearLayout) findViewById(R.id.complaints_existing);
//
//         button.setVisibility(View.GONE);
//
//
//         progressBar.setProgress(i);
//         mCountDownTimer=new CountDownTimer(3000,1000) {
//
//             @Override
//             public void onTick(long millisUntilFinished) {
//
//                 i++;
//                 progressBar.setVisibility(View.VISIBLE);
//                 progressBar.setProgress((int)i*100/(3000/1000));
//
//             }
//
//             @Override
//             public void onFinish() {
//                 progressBar.setVisibility(View.GONE);
//                 linearLayout.setVisibility(View.VISIBLE);
//                 button1.setVisibility(View.VISIBLE);
//
//             }
//         };
//         mCountDownTimer.start();
     }

//    public void proceed_anyway (View view) {
//
//        Intent intent = new Intent(this, FacilityComplaint.class);
//        startActivity(intent);
//    }

    public void complaints_existing (View view) {

        Intent intent = new Intent(this, ExistingComplaints.class);
        startActivity(intent);
    }
}

