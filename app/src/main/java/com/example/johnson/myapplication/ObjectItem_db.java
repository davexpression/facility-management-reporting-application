package com.example.johnson.myapplication;


/**
 * Created by Johnson on 22/04/2018.
 */


public class ObjectItem_db {

    public String complaint_nature;
    public int complaint_number;

    public String location_text;
    public String date_text;
    public String complaint_id;
    public String complaint_remark;
    public String location_floor;

    public String facility;
    boolean joined;
    boolean creator;

    int img_id;
    int joined_int;


    public void setComplaint_remark(String input) {
        this.complaint_remark = input;
    }

    public String getComplaint_remark() {
        return complaint_remark;
    }

    public void setComplaint_nature(String input) {
        this.complaint_nature = input;
    }

    public String getComplaint_nature() {
        return complaint_nature;
    }

    public void setComplaint_id(String input) {
        this.complaint_id = input;
    }

    public String getComplaint_id() {
        return complaint_id;
    }

    public void setLocation_floor(String input) {
        this.location_floor = input;
    }

    public String getLocation_floor() {
        return location_floor;
    }

    public void setLocation_text(String input) {
        this.location_text = input;
    }

    public String getLocation_text() {
        return location_text;
    }

    public void setJoined(boolean input) {
        this.joined = input;
    }

    public boolean isJoined() {
        return joined;
    }

    public void setCreator(boolean input) {
        this.creator = input;
    }

    public boolean isCreator() {
        return joined;
    }

    public void setImg_id(int input) {
        this.img_id = input;
    }

    public int getImg_id() {
        return img_id;
    }

    public void setComplaint_number(int input) {
        this.complaint_number = input;
    }

    public int getComplaint_number() {
        return complaint_number;
    }


    public void setDate_text(String input) { this.date_text = input; }

    public String getDate_text() { return date_text; }

    public void setFacility(String input) { this.facility = input; }

    public String getFacility() { return facility; }


    public void setJoined_int(int input) {
        this.joined_int = input;
    }

    public int getJoined_int() {
        return joined_int;
    }


}
