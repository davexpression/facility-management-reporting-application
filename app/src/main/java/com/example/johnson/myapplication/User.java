package com.example.johnson.myapplication;

/**
 * Created by Johnson on 08/05/2018.
 */

public class User {

    String email;
    String first_name;
    String last_name;
    String matric;
    String office_floorNo;
    String office_identifier;
    String office_location;
    String password;
    String phone_number;
    String prefix;
    String prefixNo;
    String type;
    String user_id;


    public User() {

    }

    public User(String email, String first_name, String last_name, String matric, String office_floorNo, String office_identifier, String office_location, String password, String phone_number, String prefix, String prefixNo, String type, String user_id) {
        this.user_id = user_id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.matric = matric;
        this.office_floorNo = office_floorNo;
        this.office_identifier = office_identifier;
        this.office_location = office_location;
        this.password = password;
        this.phone_number = phone_number;
        this.prefix = prefix;
        this.prefixNo = prefixNo;
        this.type = type;
    }

    public String getUser_id() {return user_id;}
    public void setUser_id(String input) {this.user_id = input;}

    public String getFirst_name() {return first_name;}
    public void setFname(String input) {this.first_name = input;}

    public String getLast_name() {return last_name;}
    public void setLname(String input) {this.last_name = input;}

    public String getEmail() {return email;}
    public void setEmail(String input) {this.email = input;}


    public String getMatric() {return matric;}
    public void setName(String input) {this.matric = input;}

    public String getOffice_floorNo() {return office_floorNo;}
    public void setOffice_floorNo(String input) {this.office_floorNo = input;}

    public String getOffice_identifier () {return office_identifier;}
    public void setOffice_identifier(String input) {this.office_identifier = input;}

    public String getOffice_location() {return office_location;}
    public void setOffice_location(String input) {this.office_location = input;}

    public String getPassword() {return password;}
    public void setPassword(String input) {this.password = input;}

    public String getPhone_number() {return phone_number;}
    public void setPhone_number(String input) {this.phone_number = input;}

    public String getPrefix() {return prefix;}
    public void setPrefix(String input) {this.prefix = input;}

    public String getPrefixNo() {return prefixNo;}
    public void setPrefixNo(String input) {this.prefixNo = input;}

    public String getType() {return type;}
    public void setType(String input) {this.type = input;}
}
