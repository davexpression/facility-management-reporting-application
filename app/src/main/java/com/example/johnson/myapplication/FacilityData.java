package com.example.johnson.myapplication;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Johnson on 12/05/2018.
 */

public class FacilityData {

    private static final Map<String, String> facilityMap;

    static {
        Map<String, String> myMap = new HashMap<String, String>();
        myMap.put("Refrigerator", "hos_electrical");
        myMap.put("Alarm clock", "hos_mechanical");
        myMap.put("Dish Washer", "hos_mechanical");

        myMap.put("Door Handle", "hos_civil");
        myMap.put("Light Switch", "hos_electrical");
        myMap.put("Socket", "hos_electrical");
        myMap.put("Shelves", "hos_civil");
        myMap.put("Copper Wire", "hos_electrical");
        myMap.put("Light Bulb", "hos_electrical");
        myMap.put("Fan", "hos_mechanical");
        myMap.put("Air Conditioner", "hos_electrical");
        myMap.put("Staircase", "hos_civil");
        myMap.put("Water Tap", "hos_mechanical");
        myMap.put("WhiteBoard", "hos_civil");
        myMap.put("Door", "hos_mechanical");
        myMap.put("Wall", "hos_civil");
        myMap.put("Pipe", "hos_mechanical");
        myMap.put("Printer", "hos_electrical");
        myMap.put("Window", "hos_mechanical");
        myMap.put("Projector", "hos_electrical");
        myMap.put("Couch", "hos_mechanical");
        myMap.put("Drawers", "hos_civil");

        myMap.put("Peeler", "hos_electrical");
        myMap.put("Arm Chair", "hos_mechanical");
        myMap.put("Glass", "hos_mechanical");
        myMap.put("WC", "hos_mechanical");
        myMap.put("Bath Tub", "hos_mechanical");
        myMap.put("Blade", "hos_electrical");
        myMap.put("Chair", "hos_mechanical");
        myMap.put("Spray", "hos_electrical");
        myMap.put("Extractor","hos_electrical");
        myMap.put("Fence", "hos_electrical");
        myMap.put("Stool", "hos_electrical");
        myMap.put("CorkScrew", "hos_electrical");
        myMap.put("Epilator", "hos_electrical" );
        myMap.put("Lamp", "hos_electrical");
        myMap.put("Kettle", "hos_mechanical");
        myMap.put("Juicer", "hos_electrical");
        myMap.put("Vacuum Cleaner", "hos_electrical");
        myMap.put("Telephone", "hos_electrical");
        myMap.put("Television", "hos_electrical");
        myMap.put("Projector Stand", "hos_mechanical");
        myMap.put("Coffee Machine", "hos_electrical");
        myMap.put("Roof", "hos_mechanical");
        myMap.put("Roller","hos_electrical");
        myMap.put("Tent", "hos_civil");
        myMap.put("Tiles", "hos_civil");
        myMap.put("Mixer", "hos_electrical");
        myMap.put("Opener", "hos_electrical");
        myMap.put("Sound System", "hos_electrical");
        myMap.put("Dust Pan", "hos_electrical");
        myMap.put("Oven", "hos_electrical");
        myMap.put("Cupboard", "hos_civil");
        myMap.put("Multicooker", "hos_electrical");
        myMap.put("Toaster", "hos_electrical");
        myMap.put("Iron", "hos_electrical");
        myMap.put("Broom", "hos_electrical");
        myMap.put("Scale", "hos_electrical");
        myMap.put("Plunger", "hos_electrical");
        myMap.put("Boiler", "hos_electrical");
        myMap.put("new_facility", "hos_electrical");


        facilityMap = Collections.unmodifiableMap(myMap);
    }

    public Map<String, String> getFacilityMap() {
        return facilityMap;
    }

}
