package com.example.johnson.myapplication;

/**
 * Created by Johnson on 22/04/2018.
 */


public class ObjectItem {

    public String facility_name;
    public String complaint_number;
    public String location_text;
    public String location_floor;
    public String location_area;
    public String staff;
    public String student;

    int img_id;


    // constructor
    public ObjectItem(String facility_name, String complaint_number) {

        this.facility_name = facility_name;
        this.complaint_number = complaint_number;

        this.location_text = location_text;

    }

    public void setLocation_floor(String input) {
        this.location_floor = input;
    }

    public String getLocation_floor() {
        return location_floor;
    }

    public void setLocation_area(String input) {
        this.location_area = input;
    }

    public String getLocation_area() {
        return location_area;
    }

    public void setLocation_text(String input) {
        this.location_text = input;
    }

    public String getLocation_text() {
        return location_text;
    }

    public void setImg_id(int input) {
        this.img_id = input;
    }

    public int getImg_id() { return img_id; }

    public void setStaff(String input) {
        this.staff = input;
    }

    public String getStaff() {
        return staff;
    }

    public void setStudent(String input) {
        this.student = input;
    }

    public String getStudent() {
        return student;
    }
}
