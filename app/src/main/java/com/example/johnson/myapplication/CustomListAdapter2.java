package com.example.johnson.myapplication;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Johnson on 15/04/2018.
 */

public class CustomListAdapter2 extends ArrayAdapter<ObjectItem> {

    //to reference the Activity
    private final Activity context;

    private ArrayList<ObjectItem> data = null;

//    private boolean[] viewVisibility = null;


    public CustomListAdapter2(Activity context, int layout, ArrayList<ObjectItem> data) {

        super(context, layout, data);

        this.context = context;
        this.data = data;
//        this.viewVisibility = new boolean[data.size()];

    }

    public View getView(final int position,  View view, ViewGroup parent) {


        if (view == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            view = inflater.inflate(R.layout.most_lodged_complaints, parent, false);
        }


        //this code sets the values of the objects to values from the arrays
        final ObjectItem objectItem = data.get(position);

        //this code gets references to objects in the listview_row.xml file
        TextView headerTextField = (TextView) view.findViewById(R.id.list_header);
        TextView numberTextField = (TextView) view.findViewById(R.id.most_lodged_number);

        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.most_lodged_staff);
//        TextView lecturerField = (TextView) view.findViewById(R.id.lecturer_number);
//        TextView studentField = (TextView) view.findViewById(R.id.student_number);
        TextView locationField = (TextView) view.findViewById(R.id.location_text);


//        ImageView imageView = (ImageView) view.findViewById(R.id.most_complaints_expand);


        headerTextField.setText(objectItem.facility_name);
        numberTextField.setText(objectItem.complaint_number);


        return view;

    };

}
