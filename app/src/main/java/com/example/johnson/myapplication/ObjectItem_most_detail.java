package com.example.johnson.myapplication;

/**
 * Created by Johnson on 22/04/2018.
 */


public class ObjectItem_most_detail {

    public String facility_name;
    public String complaint_nature;
    public int complaint_number;
    public String complaint_id;
    public String location_text;
    public String location_floor;
    public String location_area;
    public String name;
    public boolean new_facility;

    String date;

    boolean seen;

    int img_id;


    // constructor
    public ObjectItem_most_detail(String complaint_nature, String complaint_id, String facility_name, int complaint_number) {

        this.complaint_nature = complaint_nature;
        this.complaint_id = complaint_id;
        this.facility_name = facility_name;
        this.complaint_number = complaint_number;
        this.location_text = location_text;

    }

    public void setLocation_floor(String input) {
        this.location_floor = input;
    }

    public String getLocation_floor() {
        return location_floor;
    }

    public void setLocation_area(String input) {
        this.location_area = input;
    }

    public String getLocation_area() {
        return location_area;
    }

    public void setLocation_text(String input) {
        this.location_text = input;
    }

    public String getLocation_text() {
        return location_text;
    }

    public void setImg_id(int input) {
        this.img_id = input;
    }

    public int getImg_id() { return img_id; }

    public String getName() {
        return name;
    }

    public void setDate (String input) {
        this.date = input;
    }

    public String getDate() {
        return date;
    }

    public void setName(String input) {
        this.name = input;
    }

    public  void setSeen (boolean input) {this.seen = input;}

    public  boolean isSeen (boolean input) {return seen;}

    public boolean getNew_facility() {
        return new_facility;
    }

    public void setNew_facility(boolean input) {
        this.new_facility = input;
    }
}

