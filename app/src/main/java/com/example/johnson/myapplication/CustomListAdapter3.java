package com.example.johnson.myapplication;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Johnson on 15/04/2018.
 */

public class CustomListAdapter3 extends ArrayAdapter<ObjectItem3> {

    //to reference the Activity
    private final Activity context;

    private ObjectItem3[] data = null;
    private boolean[] viewVisibility = null;


    public CustomListAdapter3(Activity context, ObjectItem3[] data) {

        super(context, R.layout.existing_complaint, data);

        this.context = context;
        this.data = data;
        this.viewVisibility = new boolean[data.length];

    }

    public View getView(final int position,  View view, ViewGroup parent) {


        if (view == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            view = inflater.inflate(R.layout.existing_complaint, parent, false);
        }


        //this code sets the values of the objects to values from the arrays
        final ObjectItem3 objectItem = data[position];

        //this code gets references to objects in the listview_row.xml file
        TextView headerTextField = (TextView) view.findViewById(R.id.existing_complaints_list_header);
        TextView detailTextField = (TextView) view.findViewById(R.id.existing_complaints_list_detail);
        TextView numberTextField = (TextView) view.findViewById(R.id.existing_complaints_number);
        TextView locationField = (TextView) view.findViewById(R.id.existing_complaints_location_text);

        final TextView textView = (TextView) view.findViewById(R.id.existing_complaints_lists_detail2);


        if(viewVisibility[position]) {
            textView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.GONE);
        }

        ImageView imageView = (ImageView) view.findViewById(R.id.existing_complaints_expand);

        headerTextField.setText(objectItem.header);
        detailTextField.setText(objectItem.detail);
        numberTextField.setText(objectItem.complaint_number);
        numberTextField.setText(objectItem.complaint_number);
        locationField.setText(objectItem.location_text);


        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textView.setVisibility(View.VISIBLE);
                viewVisibility[position] = true;
            }
        });
        return view;

    };

}
