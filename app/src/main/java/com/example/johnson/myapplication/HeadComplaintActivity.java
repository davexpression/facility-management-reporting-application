package com.example.johnson.myapplication;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class HeadComplaintActivity extends AppCompatActivity {

    private  AlertDialog.Builder builder;
    private DatabaseReference firebaseDatabase;
    Intent intent;
    String complaint, complaint_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_head_complaint);

        Toolbar toolbar = (Toolbar) findViewById(R.id.hos_toolbar_complaint);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        intent = getIntent();
        complaint = intent.getStringExtra("complaint");
        complaint_id = intent.getStringExtra("complaint_id");

        firebaseDatabase = FirebaseDatabase.getInstance().getReference();
        builder = new AlertDialog.Builder(this);
    }


    public void hos_complaint_noted(View view) {

        firebaseDatabase.child("complaints").child(complaint_id)
                .child("noted_by_hos").setValue(true);
    }


    public void hos_complaint_back(View view) {
        super.onBackPressed();
    }


    public void hos_complaint_call(View view) {

        final int MY_PERMISSION_ACCESS_CALL_PHONE= 100;

        builder.setTitle("Confirm");
        builder.setMessage("You are about to make a call to the Maintenance Officer");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if (ContextCompat.checkSelfPermission(HeadComplaintActivity.this,
                                android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(
                                    HeadComplaintActivity.this, new String[]{Manifest.permission.CALL_PHONE},
                                    MY_PERMISSION_ACCESS_CALL_PHONE);
                        }
                        else {

                            Intent intent = new Intent(Intent.ACTION_CALL);
                            intent.setData(Uri.parse("tel:08132137527"));
                            startActivity(intent);
                        }

                    }
                });
        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // code to run when Cancel is pressed
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }
}
