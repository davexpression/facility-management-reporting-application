package com.example.johnson.myapplication;

import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MostLodgedComplaint extends AppCompatActivity {

    ListView listView;
    ArrayList<ObjectItem_most_detail> objectItems = new ArrayList<>();
    SingleComplaint singleComplaint;
    private SharedPreferences sharedPreferences;

    DialogFragment dialogFragment;
    Dialog dialog;
    String complaint_desc, loc, name, join, mode;
    private FirebaseAuth mAuth;
    boolean seen;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mode = sharedPreferences.getString("mode", null);

        if (mode.equals("mofficer"))
            setTheme(R.style.AppTheme_NoActionBar6);
        else
            setTheme(R.style.AppTheme_NoActionBar7);


        setContentView(R.layout.activity_most_lodged_complaint);

        Toolbar toolbar = (Toolbar) findViewById(R.id.mo_toolbar_complaint_most);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mAuth = FirebaseAuth.getInstance();

        final Bundle bundle = new Bundle();
        singleComplaint = new SingleComplaint();

        final CustomListAdapter_most_detail customListAdapter = new CustomListAdapter_most_detail(this, R.layout.most_lodged_complaints_detail, objectItems);
        listView = (ListView) findViewById(R.id.my_complaints_lists);
        listView.setAdapter(customListAdapter);

        Intent intent = getIntent();
        String loc_area = intent.getStringExtra("loc_area");
        final String facility_name = intent.getStringExtra("facility_name");

        DatabaseReference fb;
        final DatabaseReference complaint;
        Query query;

        fb = FirebaseDatabase.getInstance().getReference();
        complaint = fb.child("complaints");
        final DatabaseReference users = fb.child("users");

        complaint.keepSynced(true);

        FacilityImage facilityImage = new FacilityImage();
        final Map<String, Integer> facilityType = facilityImage.getFacilityMap();

        FacilityColor facilityColor = new FacilityColor();
        final Map<String, String> facilityMap = facilityColor.getFacilityMap();

        query = complaint.orderByChild("location_area").equalTo(loc_area);

        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Complaint complaint1 = dataSnapshot.getValue(Complaint.class);


                if (complaint1.facility_name.equals(facility_name)){

                    HashMap<String, String> joined = (HashMap<String, String>) complaint1.joined;
                    final HashMap<String, String> seen_by = (HashMap<String, String>) complaint1.seen_by;

                    if (seen_by.containsKey(mAuth.getCurrentUser().getUid()));

                    final ObjectItem_most_detail objectItem = new ObjectItem_most_detail(complaint1.complaint_nature, complaint1.complaint_id, complaint1.facility_name, joined.size());
                    objectItem.setLocation_area(complaint1.location_area);
                    objectItem.setLocation_floor(complaint1.location_floor);

                    if (complaint1.new_facility == false)
                        objectItem.setImg_id(facilityType.get(complaint1.facility_name));

                    objectItem.setNew_facility(complaint1.new_facility);

                    objectItem.setDate(complaint1.date);
                    objectItem.setSeen(seen);

                    users.orderByKey().equalTo(complaint1.user_id).addListenerForSingleValueEvent(new ValueEventListener() {

                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            for (DataSnapshot data : dataSnapshot.getChildren()) {

                                User user = data.getValue(User.class);

                                if (user.type.equals("student"))
                                    objectItem.setName(user.first_name + " " + user.last_name);
                                else
                                objectItem.setName(user.prefix +  " " + user.first_name + " " + user.last_name);

                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                    objectItems.add(0, objectItem);
                    customListAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                String complaint_id = objectItems.get(position).complaint_id;
                complaint.child(complaint_id).child("seen_by").child(mAuth.getCurrentUser().getUid()).setValue("seen");

                LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.list_highlight);
                linearLayout.setBackgroundColor(Color.WHITE);

                String date, use_date = null;

                complaint_desc = objectItems.get(position).complaint_nature;
                loc = objectItems.get(position).location_floor + " | " +objectItems.get(position).location_area;
                loc = objectItems.get(position).facility_name + " at " + loc;
                name = objectItems.get(position).name;
                join = Integer.toString(objectItems.get(position).complaint_number);
                date = objectItems.get(position).date;

                bundle.putString("complaint", complaint_desc);
                bundle.putString("complaint_id", complaint_id);
                bundle.putString("loc", loc);
                bundle.putString("name", name);
                bundle.putString("join", join);
                bundle.putString("date", use_date);

                try {

                    java.text.DateFormat format = new SimpleDateFormat("EEE LLL dd yyyy");
                    Date myDate = format.parse(date);
                    use_date = myDate.toString();


                }
                catch (ParseException e){ }



                singleComplaint = new SingleComplaint();
                singleComplaint.setArguments(bundle);

                listView.setVisibility(View.GONE);

                getSupportFragmentManager().beginTransaction()
                        //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)

                        .add(R.id.most_in_complaints,singleComplaint, "officer_view")
                        .addToBackStack("officer_view")
                        .commit();

            }
        });
    }

//
//    public void showDrawerToggle(boolean showDrawerIndicator) {
//        toggle.setDrawerIndicatorEnabled(showDrawerIndicator);
//        toggle.setHomeAsUpIndicator(R.drawable.baseline_arrow_back_black_18);
//
//        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (!toggle.isDrawerIndicatorEnabled()) {
//
//                    toggle.setDrawerIndicatorEnabled(true);
//
//                    listView.setVisibility(View.VISIBLE);
//
//                    HeadComplaintsFragment headComplaintsFragment = (HeadComplaintsFragment) getSupportFragmentManager().findFragmentByTag("cover");
//
//                    HeadComplaintView headComplaintView = (HeadComplaintView) getSupportFragmentManager().findFragmentByTag("view");
//                    headComplaintView.closeFrag();
//
//                    headComplaintsFragment.visible();
//
//                }
//            }
//        });
//    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.home, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();


        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity2.class));
            return true;
        }



        return super.onOptionsItemSelected(item);
    }

    public void mo_complaint_back_most(View view) {

        super.onBackPressed();

        if(listView.getVisibility() == View.GONE)
            listView.setVisibility(View.VISIBLE);
    }

    public void close_dialog(View view) {
        dialog.dismiss();
    }

//    public void showDrawerToggle(boolean showDrawerIndicator) {
//
//    }

//
//    @Override
//    public void onBackPressed() {
//
//        Toast.makeText(MostLodgedComplaint.this, "Authentication Successful.",
//                Toast.LENGTH_SHORT).show();
//
//        super.onBackPressed();
//    }

}

