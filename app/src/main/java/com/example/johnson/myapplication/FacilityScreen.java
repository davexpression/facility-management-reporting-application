package com.example.johnson.myapplication;



import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.drm.DrmManagerClient;
import android.graphics.Color;
import android.os.Build;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Map;


public class FacilityScreen extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    private View hideKeyboard;

    private SharedPreferences sharedPreferences;

    private TextView textView2, loc1, loc2, loc3, loc4, loc5, loc6, loc7, loc8, loc9, loc10;
    private EditText editText;
    private ImageView imageView;
    private CheckBox checkBox;
    private String office_floor, office_identifier, suggestion_text, use_office_floor, use_office_identifier, facility_name, facility_type;
    private ColorStateList oldColors;
    private Spinner spinner;
    private  Intent intent;
    private ProgressBar progressBar;

    private  LinearLayout linearLayout, linearLayout2;

    private int mine;
    private int img_id;
    private  int i;
    private CountDownTimer mCountDownTimer;
    private static final String[] paths = {"Ground Floor", "1st Floor", "2nd Floor",
            "3rd Floor", "Lecture Theater Area", "Faculty Surrounding"};

    Switch switchButton;
    String mode;

    Button proceed_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_facilty_screen);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mode = sharedPreferences.getString("mode", null);
        switchButton = (Switch) findViewById(R.id.my_switch);


        if (mode.equals("student"))
            switchButton.setVisibility(View.GONE);

        Intent intent = getIntent();
        facility_name = intent.getStringExtra("facility_name");

        linearLayout = (LinearLayout) findViewById(R.id.floor_house);
        linearLayout2 = (LinearLayout) findViewById(R.id.selected_floor);

        proceed_button = (Button) findViewById(R.id.location_proceed);
        FacilityData facilityData = new FacilityData();
        Map<String, String> facilityType = facilityData.getFacilityMap();
//        String use = facility_name.replace(" ", "_");

        boolean isNew = sharedPreferences.getBoolean("new_facility", false);

        if (isNew == true){
            facility_type = facilityType.get("new_facility").toString();
        }
        else
            facility_type = facilityType.get(facility_name).toString();

        i=0;

        loc1 = (TextView) findViewById(R.id.textview_1);
        loc2 = (TextView) findViewById(R.id.textview_2);
        loc3 = (TextView) findViewById(R.id.textview_3);
        loc4 = (TextView) findViewById(R.id.textview_4);
        loc5 = (TextView) findViewById(R.id.textview_5);
        loc6 = (TextView) findViewById(R.id.textview_6);
        loc7 = (TextView) findViewById(R.id.textview_7);
        loc8 = (TextView) findViewById(R.id.textview_8);
        loc9 = (TextView) findViewById(R.id.textview_9);
        loc10 = (TextView) findViewById(R.id.textview_10);


        if (mode.equals("staff")){

            final String office_floor_text = sharedPreferences.getString("office_floor", null);
            int office_floor_value = Integer.parseInt(office_floor_text);
            office_floor_value = office_floor_value - 1;


            String[] texts = getResources().getStringArray(R.array.pref_office_floor);
            office_floor = texts[office_floor_value];

            office_identifier = sharedPreferences.getString("office_identifier", null);

        }



        progressBar = (ProgressBar) findViewById(R.id.location_progress_bar);
        checkBox = (CheckBox) findViewById(R.id.loc_checkbox);


        //use shared pref
//        intent = getIntent();
//        img_id = intent.getIntExtra("img_id", 1);



        spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(FacilityScreen.this,
                android.R.layout.simple_spinner_item, paths);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        spinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                hideKeyboardMethod(hideKeyboard);
                return  false;
            }

        });

        //for changing location
        textView2 = (TextView) findViewById(R.id.location_selected);
//        imageView = (ImageView) findViewById(R.id.send);

        TextView textView_color = (TextView) findViewById(R.id.testing_test);

        oldColors =  textView_color.getTextColors(); //save original colors

        editText = (EditText) findViewById(R.id.enter_location);

        editText.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                hideKeyboard = view;
                view.setFocusable(true);
                view.setFocusableInTouchMode(true);

//                imageView.setVisibility(View.VISIBLE);


//                if (MotionEvent.ACTION_UP == motionEvent.getAction()) {
                    LinearLayout linearLayout = (LinearLayout) findViewById(R.id.ground_floor);

                    for (int i = 0; i < linearLayout.getChildCount(); i++) {
                        TextView textView = (TextView) linearLayout.getChildAt(i);

                        //treat only the sick ones

                        textView.setBackgroundColor(Color.TRANSPARENT);
                        textView.setTextColor(oldColors);
                    }
                return false;
            }

        });

        final LinearLayout linearLayout = (LinearLayout) findViewById(R.id.after_switch);
        final LinearLayout linearLayout2 = (LinearLayout) findViewById(R.id.currently_set_to);
        final LinearLayout groundfloor_linearLayout = (LinearLayout) findViewById(R.id.ground_floor);
        final LinearLayout enter_loc_area_linearLayout = (LinearLayout) findViewById(R.id.enter_location_area);

        final LinearLayout suggestions = (LinearLayout) findViewById(R.id.suggestions);


        switchButton.setTextColor(Color.GRAY);
        linearLayout2.setVisibility(View.GONE);
        linearLayout.setVisibility(View.VISIBLE);


        switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {


                if(isChecked){
                    switchButton.setTextColor(Color.BLACK);
                    linearLayout2.setVisibility(View.VISIBLE);
                    textView2.setText(office_floor + " | " + office_identifier);
                    linearLayout.setVisibility(View.GONE);
                    proceed_button.setVisibility(View.VISIBLE);

                }

                else {
                    switchButton.setTextColor(Color.GRAY);
                    linearLayout2.setVisibility(View.GONE);
                    linearLayout.setVisibility(View.VISIBLE);
                    proceed_button.setVisibility(View.GONE);
                }

            }
        });


        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked){

//                    imageView.setVisibility(View.GONE);
                    editText.setEnabled(false);
                    groundfloor_linearLayout.setVisibility(View.VISIBLE);

                }
                else {
                    groundfloor_linearLayout.setVisibility(View.GONE);
//                    imageView.setVisibility(View.VISIBLE);
                    editText.setEnabled(true);

                }
            }
        });

    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        intent = getIntent();
//        img_id = intent.getIntExtra("img_id", 1);
//    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }

        if (id == android.R.id.home) {

            if (linearLayout.getVisibility() == View.GONE) {

                if (mode.equals("staff"))
                    switchButton.setVisibility(View.VISIBLE);

                linearLayout2.setVisibility(View.GONE);
                linearLayout.setVisibility(View.VISIBLE);
                proceed_button.setVisibility(View.GONE);

            }
            else
//                super.onBackPressed();
                finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {

        switch (position) {
            case 0:
                checkBox.setVisibility(View.VISIBLE);
                loc1.setVisibility(View.VISIBLE);
                loc1.setText("Conference room");

                loc2.setVisibility(View.VISIBLE);
                loc2.setText("Rest room male (staff)");

                loc3.setVisibility(View.VISIBLE);
                loc3.setText("Rest room female (staff)");

                loc4.setVisibility(View.GONE);
                loc5.setVisibility(View.GONE);
                loc6.setVisibility(View.GONE);
                loc7.setVisibility(View.GONE);
                loc8.setVisibility(View.GONE);
                loc9.setVisibility(View.GONE);
                loc10.setVisibility(View.GONE);
                break;

            case 1:
                checkBox.setVisibility(View.VISIBLE);
                loc1.setVisibility(View.VISIBLE);
                loc1.setText("ICS Lab");

                loc2.setVisibility(View.VISIBLE);
                loc2.setText("ICS Secretariat");

                loc3.setVisibility(View.VISIBLE);
                loc3.setText("TCS Seminar room");

                loc4.setVisibility(View.VISIBLE);
                loc4.setText("TCS Secretariat");

                loc5.setVisibility(View.VISIBLE);
                loc5.setText("Mass Comm News Room");

                loc6.setVisibility(View.VISIBLE);
                loc6.setText("Mass Comm Secretariat");

                loc7.setVisibility(View.GONE);
                loc8.setVisibility(View.GONE);
                loc9.setVisibility(View.GONE);
                loc10.setVisibility(View.GONE);
                break;

            case 2:
                checkBox.setVisibility(View.VISIBLE);
                loc1.setVisibility(View.VISIBLE);
                loc1.setText("ICS Seminar Room");

                loc2.setVisibility(View.VISIBLE);
                loc2.setText("Cat. and Class Lab");

                loc3.setVisibility(View.VISIBLE);
                loc3.setText("Rest room male");

                loc4.setVisibility(View.VISIBLE);
                loc4.setText("Rest room female");

                loc5.setVisibility(View.GONE);
                loc6.setVisibility(View.GONE);
                loc7.setVisibility(View.GONE);
                loc8.setVisibility(View.GONE);
                loc9.setVisibility(View.GONE);
                loc10.setVisibility(View.GONE);
                break;

            case 3:
                checkBox.setVisibility(View.VISIBLE);
                loc1.setVisibility(View.VISIBLE);
                loc1.setText("LR 1");

                loc2.setVisibility(View.VISIBLE);
                loc2.setText("LR 2");

                loc3.setVisibility(View.VISIBLE);
                loc3.setText("LR 3");

                loc4.setVisibility(View.VISIBLE);
                loc4.setText("LR 4");

                loc5.setVisibility(View.VISIBLE);
                loc5.setText("LR 5");

                loc6.setVisibility(View.VISIBLE);
                loc6.setText("TCS Lab");

                loc7.setVisibility(View.VISIBLE);
                loc7.setText("Rest room male (Beside LR 1)");

                loc8.setVisibility(View.VISIBLE);
                loc8.setText("Rest room female (Beside LR 1)");

                loc9.setVisibility(View.VISIBLE);
                loc9.setText("Rest room male (Beside Radio unit)");

                loc10.setVisibility(View.VISIBLE);
                loc10.setText("Rest room female(Beside Radio unit)");
                break;

            case 4:
                checkBox.setVisibility(View.VISIBLE);
                loc1.setVisibility(View.VISIBLE);
                loc1.setText("Lecture Theater");

                loc2.setVisibility(View.GONE);
                loc3.setVisibility(View.GONE);
                loc4.setVisibility(View.GONE);
                loc5.setVisibility(View.GONE);
                loc6.setVisibility(View.GONE);
                loc7.setVisibility(View.GONE);
                loc8.setVisibility(View.GONE);
                loc9.setVisibility(View.GONE);
                loc10.setVisibility(View.GONE);
                break;

            case 5:
                checkBox.setVisibility(View.GONE);
                loc1.setVisibility(View.GONE);
                loc2.setVisibility(View.GONE);
                loc3.setVisibility(View.GONE);
                loc4.setVisibility(View.GONE);
                loc5.setVisibility(View.GONE);
                loc6.setVisibility(View.GONE);
                loc7.setVisibility(View.GONE);
                loc8.setVisibility(View.GONE);
                loc9.setVisibility(View.GONE);
                loc10.setVisibility(View.GONE);
//                loc1.setText("LR A");
//                loc2.setText("LR B");
//                loc3.setText("LR C");
//                loc4.setText("LR D");
                break;
        }
    }

    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void floor_button (View view) {

        switchButton.setVisibility(View.GONE);
        Button button = (Button) findViewById(view.getId());
        int position = Integer.parseInt(button.getTag().toString());
        spinner.setSelection(position);
        linearLayout.setVisibility(View.GONE);
        linearLayout2 = (LinearLayout) findViewById(R.id.selected_floor);
        linearLayout2.setVisibility(View.VISIBLE);

        proceed_button.setVisibility(View.VISIBLE);

    }

//    public void shutPrevVisibility(LinearLayout parent_LinearLayout){
//
//        parent_LinearLayout.setVisibility(View.VISIBLE);
//        for (int i=0; i<=5; i++) {
//            LinearLayout linearLayout = (LinearLayout) parent_LinearLayout.getChildAt(i);
//
//            if(linearLayout.getVisibility() == View.VISIBLE) {
//                linearLayout.setVisibility(View.GONE);
//            }
//        }
//    }





    public void text_view(View view) {

        editText.setFocusable(false);
//        if(imageView.getVisibility() == View.VISIBLE)
//            imageView.setVisibility(View.GONE);

        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.ground_floor);
        for (int i = 0; i < linearLayout.getChildCount(); i++) {

            TextView textView = (TextView) linearLayout.getChildAt(i);
            if(textView.getId() == view.getId()) {
                textView.setBackgroundColor(Color.parseColor("#3F51B5"));
                textView.setTextColor(Color.WHITE);
                suggestion_text = textView.getText().toString();
                continue;
            }
            textView.setBackgroundColor(Color.TRANSPARENT);
            textView.setTextColor(oldColors);

        }
        hideKeyboardMethod(hideKeyboard);

    }

    public void send (View view) {
//        String value = editText.getText().toString();
//        textView2.setText(value);
        editText.setFocusable(false);
        imageView.setVisibility(View.GONE);
        hideKeyboardMethod(hideKeyboard);
    }

    public void hideKeyboardMethod (View view) {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (view != null)
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

     public void location_proceed (View view) {

         Intent intent = new Intent(this, FacilityComplaint.class);

         if (mode.equals("student")) {

             use_office_floor = spinner.getSelectedItem().toString();

             if ( checkBox.isChecked()) {
                 use_office_identifier = suggestion_text;
             }

             else
                 use_office_identifier = editText.getText().toString();
         }

         if (switchButton.isChecked()) {
             use_office_floor = office_floor;
             use_office_identifier = office_identifier;
             intent.putExtra("office_floor", use_office_floor);
             intent.putExtra("office_identifier", use_office_identifier);
         }

         else {
             use_office_floor = spinner.getSelectedItem().toString();
         }


         if ( (switchButton.isChecked() == false) && checkBox.isChecked()) {
             use_office_identifier = suggestion_text;
         }

         else if ((switchButton.isChecked() == false) && (checkBox.isChecked() == false) ) {
             use_office_identifier = editText.getText().toString();
         }

         intent.putExtra("office_floor", use_office_floor);
         intent.putExtra("office_identifier", use_office_identifier);
         intent.putExtra("facility_name", facility_name);
         intent.putExtra("facility_type", facility_type);
         startActivity(intent);


//
//        Intent intent = new Intent(this, FacilityComplaint.class);
//        startActivity(intent);

//         Button button = (Button) findViewById(R.id.location_proceed);
//         final Button button1 = (Button) findViewById(R.id.proceed_anyway);
//         final LinearLayout linearLayout = (LinearLayout) findViewById(R.id.complaints_existing);
//
//         button.setVisibility(View.GONE);
//
//
//         progressBar.setProgress(i);
//         mCountDownTimer=new CountDownTimer(3000,1000) {
//
//             @Override
//             public void onTick(long millisUntilFinished) {
//
//                 i++;
//                 progressBar.setVisibility(View.VISIBLE);
//                 progressBar.setProgress((int)i*100/(3000/1000));
//
//             }
//
//             @Override
//             public void onFinish() {
//                 progressBar.setVisibility(View.GONE);
//                 linearLayout.setVisibility(View.VISIBLE);
//                 button1.setVisibility(View.VISIBLE);
//
//             }
//         };
//         mCountDownTimer.start();
     }

//    public void proceed_anyway (View view) {
//
//        Intent intent = new Intent(this, FacilityComplaint.class);
//        startActivity(intent);
//    }

    public void complaints_existing (View view) {

        Intent intent = new Intent(this, ExistingComplaints.class);
        startActivity(intent);
    }
}

