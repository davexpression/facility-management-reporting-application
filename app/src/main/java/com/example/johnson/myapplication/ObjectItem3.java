package com.example.johnson.myapplication;

/**
 * Created by Johnson on 22/04/2018.
 */


public class ObjectItem3 {

    public String header;
    public String detail;
    public String more_detail;
    public String complaint_number;
    public String location_text;


    // constructor
    public ObjectItem3(String header, String detail, String more_detail, String complaint_number, String location_text) {
        this.header = header;
        this.detail = detail;
        this.more_detail = more_detail;
        this.complaint_number = complaint_number;
        this.location_text = location_text;
    }


}
