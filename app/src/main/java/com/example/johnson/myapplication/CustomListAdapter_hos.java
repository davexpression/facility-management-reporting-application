package com.example.johnson.myapplication;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

/**
 * Created by Johnson on 15/04/2018.
 */

public class CustomListAdapter_hos extends ArrayAdapter <ObjectItem2> {

    //to reference the Activity
    private final Activity context;

    private ArrayList<ObjectItem2> data = null;
    private boolean[] viewVisibility = null;


    public CustomListAdapter_hos(Activity context, int layout, ArrayList<ObjectItem2> data) {

        super(context, layout, data);

        this.context = context;
        this.data = data;
        this.viewVisibility = new boolean[data.size()];

    }

    public View getView(final int position,  View view, ViewGroup parent) {


        if (view == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            view = inflater.inflate(R.layout.hos_complaints, parent, false);
        }


        //this code sets the values of the objects to values from the arrays
        final ObjectItem2 objectItem = data.get(position);

        Date myDate = null;
//

        try {
            java.text.DateFormat format = new SimpleDateFormat("EEE LLL dd HH:mm:ss Z yyyy");
            myDate = format.parse(objectItem.date_text);
        }
        catch (ParseException e){ }
        //this code gets references to objects in the listview_row.xml file

        String string = DateUtils.getRelativeDateTimeString(getContext(), myDate.getTime(), DateUtils.MINUTE_IN_MILLIS,
                DateUtils.DAY_IN_MILLIS * 4, DateUtils.FORMAT_ABBREV_RELATIVE).toString();

        string = string.substring(0, string.indexOf(","));


        FacilityColor facilityColor = new FacilityColor();
        final Map<String, String> facilityMap = facilityColor.getFacilityMap();

        //this code gets references to objects in the listview_row.xml file

        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.text_background);
        LinearLayout linearLayout2 = (LinearLayout) view.findViewById(R.id.list_highlight);

//        TextView numberTextField = (TextView) view.findViewById(R.id.hos_single_num);
        TextView headerTextField = (TextView) view.findViewById(R.id.hos_complaints_list_header);
        TextView complaint_remark = (TextView) view.findViewById(R.id.hos_complaints_list_remark);

        TextView locationField = (TextView) view.findViewById(R.id.hos_complaints_location_text);
        TextView dateField = (TextView) view.findViewById(R.id.hos_complaints_time);

        ImageView imageView = (ImageView) view.findViewById(R.id.complaint_image);

//        final TextView textView = (TextView) view.findViewById(R.id.my_complaints_list_detail2);
//
//
//        if(viewVisibility[position]) {
//            textView.setVisibility(View.VISIBLE);
//        } else {
//            textView.setVisibility(View.GONE);
//        }
//
//        ImageView imageView = (ImageView) view.findViewById(R.id.my_complaints_expand);

        if (objectItem.new_facility == false) {
            imageView.setVisibility(View.VISIBLE);
            imageView.setImageResource(objectItem.getImg_id());

            GradientDrawable drawable = (GradientDrawable) linearLayout.getBackground();
            drawable.setStroke(1, Color.parseColor(facilityMap.get(objectItem.facility))); // set stroke width and stroke color
        }

        else if (objectItem.new_facility == true) {
            imageView.setVisibility(View.INVISIBLE);
            linearLayout.setBackgroundResource(R.drawable.copyborder4_one);
        }

        if (!objectItem.complaint_remark.equals("null")) {
            complaint_remark.setVisibility(View.VISIBLE);
            complaint_remark.setText(objectItem.getComplaint_remark());
        }
        else
            complaint_remark.setVisibility(View.GONE);

        if (objectItem.seen_by == false)
            linearLayout2.setBackgroundColor(Color.parseColor("#ecf3ff"));

        else
            linearLayout2.setBackgroundColor(Color.WHITE);

        headerTextField.setText(objectItem.complaint_nature);
        dateField.setText(string);
        locationField.setText(objectItem.location_text);
//        numberTextField.setText(objectItem.join_total);


//        imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                    textView.setVisibility(View.VISIBLE);
//                    viewVisibility[position] = true;
//                }
//        });
        return view;

    };
}
