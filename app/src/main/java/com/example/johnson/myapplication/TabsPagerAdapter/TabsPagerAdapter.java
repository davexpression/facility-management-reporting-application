package com.example.johnson.myapplication.TabsPagerAdapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.johnson.myapplication.FirstFragment;
import com.example.johnson.myapplication.FourthFragment;
import com.example.johnson.myapplication.SecondFragment;
import com.example.johnson.myapplication.ThirdFragment;


/**
 * Created by Johnson on 27/03/2018.
 */


public class TabsPagerAdapter extends FragmentPagerAdapter {

    private int NUM_ITEMS = 3;
//    private String[] titles= new String[]{"First Fragment", "Second Fragment","Third Fragment"};
    private String[] titles= new String[]{"First Fragment", "Second Fragment"};

    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    // Returns total number of pages
    @Override
    public int getCount() {
        return  NUM_ITEMS ;
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FirstFragment();
            case 1:
                return new SecondFragment();
            case 2:
                return new ThirdFragment();
            default:
                return null;
        }
    }

    // Returns the page title for the top indicator
//    @Override
//    public CharSequence getPageTitle(int position) {
//        return  titles[position];
//    }

}
