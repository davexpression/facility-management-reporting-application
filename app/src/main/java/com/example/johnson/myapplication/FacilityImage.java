package com.example.johnson.myapplication;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Johnson on 12/05/2018.
 */

public class FacilityImage {

    private static final Map<String, Integer> facilityMap;

    static {
        Map<String, Integer> myMap = new HashMap<String, Integer>();
        myMap.put("Alarm clock", Integer.valueOf(R.mipmap.alarmclock));
        myMap.put("Dish Washer", Integer.valueOf(R.mipmap.dishwasher1));
        myMap.put("Light Bulb", Integer.valueOf(R.mipmap.lightbulb));
        myMap.put("Copper Wire", Integer.valueOf(R.mipmap.brokencable));
        myMap.put("Shelves", Integer.valueOf(R.mipmap.shelves));
        myMap.put("Socket", Integer.valueOf(R.mipmap.socket));
        myMap.put("Light Switch", Integer.valueOf(R.mipmap.lightswitch));
        myMap.put("Door Handle", Integer.valueOf(R.mipmap.doorhandle));
        myMap.put("Fan", Integer.valueOf(R.mipmap.fan1));
        myMap.put("Air Conditioner", Integer.valueOf(R.mipmap.cooling));
        myMap.put("Staircase", Integer.valueOf(R.mipmap.stairs));
        myMap.put("Water Tap", Integer.valueOf(R.mipmap.watertap));
        myMap.put("WhiteBoard", Integer.valueOf(R.mipmap.whiteboard));
        myMap.put("Door", Integer.valueOf(R.mipmap.door));
        myMap.put("Wall", Integer.valueOf(R.mipmap.mansory));
        myMap.put("Pipe", Integer.valueOf(R.mipmap.pipe1));
        myMap.put("Printer", Integer.valueOf(R.mipmap.printer));
        myMap.put("Refrigerator", Integer.valueOf(R.mipmap.fridge));
        myMap.put("Window", Integer.valueOf(R.mipmap.window));
        myMap.put("Projector", Integer.valueOf(R.mipmap.projector));
        myMap.put("Couch", Integer.valueOf(R.mipmap.couch));
        myMap.put("Drawers", Integer.valueOf(R.mipmap.drawers));
        myMap.put("Peeler", Integer.valueOf(R.mipmap.peeler));
        myMap.put("Arm Chair", Integer.valueOf(R.mipmap.armchair));
        myMap.put("Glass", Integer.valueOf(R.mipmap.glass));
        myMap.put("WC", Integer.valueOf(R.mipmap.wc));
        myMap.put("Bath Tub", Integer.valueOf(R.mipmap.bathtub));
        myMap.put("Blade", Integer.valueOf(R.mipmap.blade));
        myMap.put("Chair", Integer.valueOf(R.mipmap.chair));
        myMap.put("Spray", Integer.valueOf(R.mipmap.cleaningspray));
        myMap.put("Extractor", Integer.valueOf(R.mipmap.extractor));
        myMap.put("Fence", Integer.valueOf(R.mipmap.fence));
        myMap.put("Stool", Integer.valueOf(R.mipmap.stool));
        myMap.put("CorkScrew", Integer.valueOf(R.mipmap.corkscrew));
        myMap.put("Epilator", Integer.valueOf(R.mipmap.epilator));
        myMap.put("Lamp", Integer.valueOf(R.mipmap.lamp));
        myMap.put("Kettle", Integer.valueOf(R.mipmap.kettle));
        myMap.put("Juicer", Integer.valueOf(R.mipmap.juicer));
        myMap.put("Vacuum Cleaner", Integer.valueOf(R.mipmap.vacuumcleaner));
        myMap.put("Telephone", Integer.valueOf(R.mipmap.telephone));
        myMap.put("Television", Integer.valueOf(R.mipmap.television));
        myMap.put("Projector Stand", Integer.valueOf(R.mipmap.camerastand));
        myMap.put("Coffee Machine", Integer.valueOf(R.mipmap.coffeemachine));
        myMap.put("Roof", Integer.valueOf(R.mipmap.roof));
        myMap.put("Roller", Integer.valueOf(R.mipmap.roller));
        myMap.put("Tent", Integer.valueOf(R.mipmap.tent));
        myMap.put("Tiles", Integer.valueOf(R.mipmap.tiles));
        myMap.put("Mixer", Integer.valueOf(R.mipmap.mixer1));
        myMap.put("Opener", Integer.valueOf(R.mipmap.opener));
        myMap.put("Sound System", Integer.valueOf(R.mipmap.musicspeaker));
        myMap.put("Dust Pan", Integer.valueOf(R.mipmap.dustpan));
        myMap.put("Oven", Integer.valueOf(R.mipmap.oven));
        myMap.put("Cupboard", Integer.valueOf(R.mipmap.cupboard));
        myMap.put("Multicooker", Integer.valueOf(R.mipmap.multicooker));
        myMap.put("Toaster", Integer.valueOf(R.mipmap.toaster));
        myMap.put("Iron", Integer.valueOf(R.mipmap.iron));
        myMap.put("Broom", Integer.valueOf(R.mipmap.broom));
        myMap.put("Scale", Integer.valueOf(R.mipmap.scale));
        myMap.put("Plunger", Integer.valueOf(R.mipmap.plunger));
        myMap.put("Boiler", Integer.valueOf(R.mipmap.boiler));

        facilityMap = Collections.unmodifiableMap(myMap);
    }

    public Map<String, Integer> getFacilityMap() {
        return facilityMap;
    }

}