package com.example.johnson.myapplication;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.example.johnson.myapplication.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class HeadNotesFragment extends Fragment {

    Activity mContext;
    ArrayList<ObjectItem2> objectItems = new ArrayList<>();
    CustomListAdapter_hos customListAdapter;
    ListView listView;
    String complaint_id, complaint_desc, loc, name, join, last_name;
    private FirebaseAuth mAuth;


    DatabaseReference fb;
    DatabaseReference complaint;
    Query query;
    DatabaseReference users;
    boolean seen = false;

    SharedPreferences sharedPreferences;

    HeadComplaintView headComplaintView;


    public HeadNotesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach (Context context) {
        super.onAttach(context);

        if(context instanceof Activity)
            mContext = (Activity) context;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_head_notes, container, false);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        final String mode = sharedPreferences.getString("mode", null);

        mAuth = FirebaseAuth.getInstance();

        final Bundle bundle = new Bundle();

        fb = FirebaseDatabase.getInstance().getReference();
        complaint = fb.child("complaints");
        complaint.keepSynced(true);

        users = fb.child("users");
        users.keepSynced(true);

        FacilityImage facilityImage = new FacilityImage();
        final Map<String, Integer> facilityType = facilityImage.getFacilityMap();

        FacilityColor facilityColor = new FacilityColor();
        final Map<String, String> facilityMap = facilityColor.getFacilityMap();

        customListAdapter = new CustomListAdapter_hos(mContext, R.layout.fragment_head_notes, objectItems);
        listView = (ListView) view.findViewById(R.id.hos_complaints_lists_noted);
        listView.setAdapter(customListAdapter);

        complaint.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Complaint complaint = dataSnapshot.getValue(Complaint.class);

                if (complaint.facility_type.equals(mode) | complaint.new_facility == true) {

                    if (complaint.noted_by_hos == true) {

                        final HashMap<String, String> joined = (HashMap<String, String>) complaint.joined;
                        final HashMap<String, String> seen_by = (HashMap<String, String>) complaint.seen_by;


                        final ObjectItem2 objectItem2 = new ObjectItem2(complaint.complaint_nature, joined.size(), complaint.location_area);
                        objectItem2.setDate_text(complaint.date);

                        if (seen_by.containsKey(mAuth.getCurrentUser().getUid()))
                            seen = true;
                        else
                            seen = false;

                        objectItem2.setComplaint_id(complaint.complaint_id);
                        objectItem2.setSeen_by(seen);
                        objectItem2.setDate_text(complaint.date);

                        objectItem2.setComplaint_remark(complaint.mofficer_remark);
                        objectItem2.setLocation_floor(complaint.location_floor);
                        objectItem2.setFacility(complaint.facility_name);
                        objectItem2.setJoin_total(Integer.toString(joined.size()));
                        objectItem2.setNew_facility(complaint.new_facility);

                        objectItem2.setNoted(complaint.noted_by_mofficer);
                        objectItem2.setFixed(complaint.fixed);

                        if (complaint.new_facility == false)
                            objectItem2.setImg_id(facilityType.get(complaint.facility_name));


                        users.orderByKey().equalTo(complaint.user_id).addListenerForSingleValueEvent(new ValueEventListener() {

                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                for (DataSnapshot data : dataSnapshot.getChildren()) {
                                    User user = data.getValue(User.class);

                                    if (user.type.equals("student")) {
                                        objectItem2.setName(user.first_name + " " + user.last_name);
                                        last_name = user.last_name;
                                        objectItem2.setLast_name(last_name);

                                    } else {
                                        objectItem2.setName(user.prefix + " " + user.first_name + " " + user.last_name);
                                        last_name = user.last_name;
                                        objectItem2.setLast_name(last_name);
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });


                        objectItems.add(0, objectItem2);
                        customListAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                complaint_id = objectItems.get(position).complaint_id;

                if (objectItems.get(position).seen_by == false)
                    complaint.child(complaint_id).child("seen_by").child(mAuth.getCurrentUser().getUid()).setValue("seen");

                LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.list_highlight);
                linearLayout.setBackgroundColor(Color.WHITE);


                complaint_desc = objectItems.get(position).complaint_nature;
                loc = objectItems.get(position).location_floor + " | " + objectItems.get(position).location_text;
                loc = objectItems.get(position).facility + " at " + loc;
                name = objectItems.get(position).name;
                join = Integer.toString(objectItems.get(position).complaint_number);

                bundle.putString("complaint", complaint_desc);
                bundle.putString("complaint_id", complaint_id);
                bundle.putString("complaint_loc", loc);
                bundle.putString("complaint_name", name);
                bundle.putString("complaint_num", join);
                bundle.putString("last_name", objectItems.get(position).getLast_name());
                bundle.putString("date", objectItems.get(position).date_text);

                bundle.putString("remark", objectItems.get(position).complaint_remark);
                bundle.putString("fragment", "noted");

                headComplaintView = new HeadComplaintView();
                headComplaintView.setArguments(bundle);

                listView.setVisibility(View.GONE);

                getFragmentManager().beginTransaction()
                        //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)

                        .add(R.id.hos_complaints,headComplaintView, "view")
                        .addToBackStack("view")
                        .commit();
            }
        });

        return view;
    }

    public void visible() {
        listView.setVisibility(View.VISIBLE);
    }

}
