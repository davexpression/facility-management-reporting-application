package com.example.johnson.myapplication;

/**
 * Created by Johnson on 22/04/2018.
 */


public class ObjectItem_most {

    public int complaint_number;
    public String complaint_nature;
    public String date;
    public String complaint_id;

    boolean joined;
    boolean creator;

    // constructor
    public ObjectItem_most(int complaint_number, String complaint_nature, String date) {

        this.complaint_number = complaint_number;
        this.complaint_nature = complaint_nature;
        this.date = date;
    }

    public void setComplaint_id(String input) {
        this.complaint_id = input;
    }

    public String getComplaint_id() {
        return complaint_id;
    }

    public void setJoined(boolean input) {
        this.joined = input;
    }

    public boolean isJoined() {
        return joined;
    }

    public void setCreator(boolean input) {
        this.creator = input;
    }

    public boolean isCreator() {
        return joined;
    }

}
