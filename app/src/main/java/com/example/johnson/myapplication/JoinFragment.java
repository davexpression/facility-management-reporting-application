package com.example.johnson.myapplication;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class JoinFragment extends DialogFragment {

    private FirebaseAuth mAuth;
    boolean joined, creator;
    private  AlertDialog.Builder builder;
    private SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public JoinFragment() {
        // Required empty public constructor
    }




//    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//
//        mContext = activity;
//
//
//        try {
//            this.mListener = (Listener) activity;
//
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString() + " must implement OnFragmentInteractionListener");
//        }
//    }

    Activity mContext;
    ArrayList<ObjectItem2> objectItems = new ArrayList<>();

    ListView listView;
    MostComplaintView2 mostComplaintView2;


    String complaint_desc, complaint_id, name, join_total;

    Dialog dialog;
    DialogFragment dialogFragment;

    DatabaseReference fb;
    DatabaseReference complaint;
    DatabaseReference users;

    Query query;

    int count = 0;

    ScrollView scrollView;



//    public void setListener(Listener listener) {
//        mListener = listener;
//    }

    @Override
    public void onAttach (Context context) {
        super.onAttach(context);

        if(context instanceof Activity)
            mContext = (Activity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_join, container, false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);


        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        editor = sharedPreferences.edit();
        mAuth = FirebaseAuth.getInstance();

        final String loc = getArguments().getString("location");
        final String facility_name = getArguments().getString("facility");
        TextView textView = (TextView) view.findViewById(R.id.join_location_text);
        textView.setText(facility_name + " at " + loc);

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setHomeAsUpIndicator(android.R.drawable.ic_menu_close_clear_cancel);
        }
        setHasOptionsMenu(true);

        final HashMap<String, Integer> map = new HashMap<>();


        fb = FirebaseDatabase.getInstance().getReference();
        complaint = fb.child("complaints");
        users = fb.child("users");

        complaint.keepSynced(true);
        users.keepSynced(true);

        final CustomListAdapter_join customListAdapter = new CustomListAdapter_join(mContext, R.layout.my_complaints, objectItems);

        listView = (ListView) view.findViewById(R.id.my_complaints_lists);
        listView.setAdapter(customListAdapter);

        Query query;
        query = complaint.orderByChild("location_area").equalTo(loc);
        query.addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Complaint complaint = dataSnapshot.getValue(Complaint.class);

                if (complaint.facility_name.equals(facility_name) & complaint.fixed == false) {

                    map.put(dataSnapshot.getKey(), count);
                    count++;

                    HashMap<String, String> joined = (HashMap<String, String>) complaint.joined;
                    final ObjectItem2 objectItem2 = new ObjectItem2(complaint.complaint_nature, joined.size(), complaint.location_area);

                    objectItem2.setDate_text(complaint.date);
                    objectItem2.setLocation_floor(complaint.location_floor);
                    objectItem2.setComplaint_id(complaint.complaint_id);
                    objectItem2.setFacility(complaint.facility_name);
                    objectItem2.setStudent(complaint.student);
                    objectItem2.setStaff(complaint.staff);


                    if (joined.containsKey(mAuth.getCurrentUser().getUid()))
                        objectItem2.setJoined(true);


                    if (complaint.user_id.equals(mAuth.getCurrentUser().getUid()))
                        objectItem2.setCreator(true);



                    users.orderByKey().equalTo(complaint.user_id).addValueEventListener(new ValueEventListener() {

                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            for (DataSnapshot data : dataSnapshot.getChildren()) {

                                User user = data.getValue(User.class);
                                //
                                if (user.type.equals("student")) {
                                    name = user.first_name + " " + user.last_name;
                                } else {
                                    name = user.prefix + " " + user.first_name + " " + user.last_name;
                                }

                                objectItem2.setName(name);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
//
                    objectItems.add(objectItem2);
                    Collections.sort(objectItems, objectItem2);
                    customListAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                final Complaint complaint = dataSnapshot.getValue(Complaint.class);

                int index = map.get(dataSnapshot.getKey());

                HashMap<String,String> joined =  (HashMap<String, String>) complaint.joined;

                ObjectItem2 objectItem2 = new ObjectItem2(complaint.complaint_nature, joined.size(), complaint.location_area);

                objectItem2.setDate_text(complaint.date);
                objectItem2.setLocation_floor(complaint.location_floor);
                objectItem2.setComplaint_id(complaint.complaint_id);
                objectItem2.setJoined(true);

                objectItems.set(index, objectItem2);
                Collections.sort(objectItems, objectItem2);
                customListAdapter.notifyDataSetChanged();

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

//                Intent intent = new Intent(MostLodgedComplaint.this, MaintenanceComplaintActivity.class);
//                String complaint = objectItems.get(position).complaint_nature;
//                String complaint_id = objectItems.get(position).getComplaint_id();
//
//                intent.putExtra("complaint", complaint);
//                intent.putExtra("complaint_id", complaint_id);
//                startActivity(intent);

                Bundle bundle = new Bundle();
                String location;

                complaint_desc = objectItems.get(position).complaint_nature;
                complaint_id = objectItems.get(position).complaint_id;
                name = objectItems.get(position).name;
                join_total = Integer.toString(objectItems.get(position).complaint_number);
                location = objectItems.get(position).facility + "_" + objectItems.get(position).location_text;


                joined = objectItems.get(position).joined;
                creator = objectItems.get(position).creator;

                bundle.putString("complaint", complaint_desc);
                bundle.putString("complaint_id", complaint_id);
                bundle.putString("name", name);
                bundle.putString("location", location);
                bundle.putInt("staff", objectItems.get(position).staff);
                bundle.putInt("student", objectItems.get(position).student);
                bundle.putString("join_total", join_total);

                bundle.putString("from", "user");

                bundle.putBoolean("joined", joined);
                bundle.putBoolean("creator", creator);


                mostComplaintView2 = new MostComplaintView2();
                mostComplaintView2.setArguments(bundle);

//                listView.setVisibility(View.GONE);

//                getSupportFragmentManager().beginTransaction()
//                        //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
//
//                        .replace(R.id.most_complaints_lists,mostComplaintView2,"view2")
//                        .addToBackStack("view2")
//                        .commit();
//
                android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction();

                dialogFragment = mostComplaintView2;
                dialogFragment.show(ft, "dialog");


//                AlertDialog.Builder builder = new AlertDialog.Builder(MostLodgedComplaint.this);
//                View alertView = getLayoutInflater().inflate(R.layout.fragment_most_complaint_view2, null);
//
//
//                builder.setView(alertView);
//                builder.show();

//                dialog = new Dialog(FacilityComplaint.this);
//                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
//                dialog.setContentView(R.layout.fragment_most_complaint_view2);
//                dialog.show();
            }
        });


        return view;
    }

//    @NonNull
//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        Dialog dialog = super.onCreateDialog(savedInstanceState);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//
//        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//            @Override
//            public void onDismiss(DialogInterface dialog) {
//
//                Toast.makeText(getActivity(), "Authentication Successful.",
//                        Toast.LENGTH_SHORT).show();
//            }
//        });
//
//
//        return dialog;
//    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        getActivity().getMenuInflater().inflate(R.menu.join_menu, menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            // handle close button click here


//            LayoutInflater layoutInflater = getActivity().getLayoutInflater();
//            final View view1 = layoutInflater.inflate(R.layout.activity_facility_complaint, null);
//
//            ScrollView scrollView = (ScrollView) view1.findViewById(R.id.facility_complaint_whole);
//            scrollView.setVisibility(View.VISIBLE);
//
            dismiss();

            return true;
        }

        if (id == R.id.action_mycomplaints) {
            Intent intent = new Intent(getActivity(), HomeActivity.class);
            intent.putExtra("startFrag", true);
            startActivity(intent);
            return true;
        }

        if (id == R.id.action_settings) {
            startActivity(new Intent(getActivity(), SettingsActivity.class));
            return true;
        }

        if (id == R.id.action_logout) {

            builder = new AlertDialog.Builder(getActivity());

            builder.setTitle("Confirm");
            builder.setMessage("Are you sure you want to log out");
            builder.setCancelable(false);
            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {

                            mAuth.signOut();
                            editor.clear();
                            editor.commit();

                            Intent intent = new Intent(getActivity(), MainActivity.class);
                            startActivity(intent);

                        }
                    });
            builder.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            // code to run when Cancel is pressed
                        }
                    });

            AlertDialog alert = builder.create();
            alert.show();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
