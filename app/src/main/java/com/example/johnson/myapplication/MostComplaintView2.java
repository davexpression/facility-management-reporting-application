package com.example.johnson.myapplication;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.lang.Object;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class MostComplaintView2 extends DialogFragment {

    private DatabaseReference firebaseDatabase;
    DatabaseReference most_lodged;
    private SharedPreferences sharedPreferences;

    private FirebaseAuth mAuth;

    Button join_button, noted_button;
    String complaint, complaint_id, name, join_number, noted_by, location, mode, remarks;
    int staff, student;

    Complaint_User complaint_user;

    boolean creator, passJoined, isStaff;
    HashMap<String, Object> map, complaint_input, user_input;


//    public MostComplaintView2() {
//        // Required empty public constructor
//    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
// Inflate the layout for this fragment

        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f2f2f2")));

        View view = inflater.inflate(R.layout.fragment_most_complaint_view2, container, false);

        mAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance().getReference();
        most_lodged = firebaseDatabase.child("most_lodged");

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mode = sharedPreferences.getString("mode", null);

        complaint = getArguments().getString("complaint");
        complaint_id = getArguments().getString("complaint_id");
        name = getArguments().getString("name");
        join_number = getArguments().getString("join_total");
        location = getArguments().getString("location");

        staff = getArguments().getInt("staff");
        student = getArguments().getInt("student");

        passJoined = getArguments().getBoolean("joined");
        creator = getArguments().getBoolean("creator");

        String user_id = mAuth.getCurrentUser().getUid();
        final TextView textView = (TextView) view.findViewById(R.id.joined_text);

        TextView textView_desc = (TextView) view.findViewById(R.id.hos_complaint_description);
        TextView textView1 = (TextView) view.findViewById(R.id.join_dialog_name);
        TextView textView2 = (TextView) view.findViewById(R.id.join_dialog_total);
        TextView textView3 = (TextView) view.findViewById(R.id.mo_complaint_remarks);

        textView_desc.setText(complaint);
        textView1.setText(name);
        textView2.setText(join_number);

        final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.join_progress);

        complaint_user = new Complaint_User(user_id);

        complaint_input = new HashMap<>();
        complaint_input.put(mAuth.getCurrentUser().getUid(), "joined");

        user_input = new HashMap<>();
        user_input.put(complaint_id, "joined");


//
//        firebaseDatabase.child("most_lodged").addValueEventListener(new ValueEventListener() {
//        @Override
//        public void onDataChange(DataSnapshot dataSnapshot) {
//
//            for (DataSnapshot child : dataSnapshot.getChildren()) {
//
//                if( child.getKey().equals(location)) {
//
//                    Most_Lodged most_lodged = child.getValue(Most_Lodged.class);
//
//                    Log.v("FacilityComplaint", "Towoju " + most_lodged.total_no);
//
//                    total_no = 1 + most_lodged.total_no;
//
//                    if (mode.equals("student")) {
//                        isStaff = false;
//                        student = 1 + most_lodged.student;
//                    }
//                    else {
//                        isStaff = true;
//                        staff = 1 + most_lodged.staff;
//                    }
//                }
//            }
//        }
//
//        @Override
//        public void onCancelled(DatabaseError databaseError) {
//            }
//        });

        String key = firebaseDatabase.child("user_complaints").child(mAuth.getCurrentUser().getUid()).push().getKey();
        map = new HashMap<>();
//
//            map.put("complaints/" + complaint_id + "/staff/", staff++);
//            map.put("complaints/" + complaint_id + "/student/", student++);

        if (mode.equals("staff"))
            map.put("complaints/" + complaint_id + "/staff/", staff + 1);
        else
            map.put("complaints/" + complaint_id + "/staff/", student + 1);

        map.put("complaints/" + complaint_id + "/joined" + "/" + mAuth.getCurrentUser().getUid(), "joined");
        map.put("user_complaints/" + mAuth.getCurrentUser().getUid() + "/" + key, complaint_id);


////        map.put("most_lodged/" + location + "/staff", staff);
////        map.put("most_lodged/" + location + "/student", student);
//        map.put("most_lodged/" + location + "/total_no", total_no);

        join_button = (Button) view.findViewById(R.id.join_button);

        join_button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                progressBar.setVisibility(View.VISIBLE);
                firebaseDatabase.updateChildren(map, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                        progressBar.setVisibility(View.GONE);
                        join_button.setVisibility(View.GONE);
                        textView.setVisibility(View.VISIBLE);
                    }
                });


            }
        });

        if (passJoined == true) {

            join_button.setVisibility(View.GONE);


            if (creator == true)
                textView.setText("You lodged this complaint");
            else
                textView.setText("You joined in Complaining");

            textView.setVisibility(View.VISIBLE);

        }

        else
            join_button.setVisibility(View.VISIBLE);

        ImageView imageView = (ImageView) view.findViewById(R.id.dialog_complaint);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return view;
    }

//    public void closeFrag() {
//
////        Toast.makeText(getActivity(), "Authentication Successful.",
////        Toast.LENGTH_SHORT).show();
//
//        MostComplaintView2 mostComplaintView2 = (MostComplaintView2) getFragmentManager().findFragmentByTag("view2");
//
//        getFragmentManager().beginTransaction()
//                //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
//                .remove(mostComplaintView2)
//                .commit();
//    }


}
