package com.example.johnson.myapplication;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.ArrayList;

public class DataBaseHandler extends SQLiteOpenHelper {


    public String table_name = "complaints";
    public String complaint_nature = "complaint_nature";
    public String complaint_number = "complaint_number";

    public String location_text = "location_text";
    public String date_text = "date_text";
    public String complaint_id = "complaint_id";
    public String complaint_remark;
    public String location_floor = "location_floor";

    public String facility = "facility";
    public String joined = "joined";

    public String img_id = "img_id";

    private  final int DB_Version=1;
    public DataBaseHandler(Context context,String DBName,int DBVersion){
        super(context, DBName,null,DBVersion);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("Create Table " + table_name + "(" + complaint_id + " TEXT PRIMARY KEY, "
                + complaint_nature + " Text, "  + complaint_number + " integer, "  + location_text + " Text, "
                + date_text + " Text, "  + location_floor + " Text, "  + facility + " Text, "
                + joined + " integer, "  + img_id + " integer);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
    public void addObject(ObjectItem2 objectItem2){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues cv=new ContentValues();
        cv.put(complaint_id,objectItem2.getComplaint_id());
        cv.put(complaint_nature,objectItem2.getComplaint_nature());
        cv.put(complaint_number,objectItem2.getComplaint_number());
        cv.put(location_text,objectItem2.getLocation_text());
        cv.put(date_text,objectItem2.getDate_text());
        cv.put(location_floor,objectItem2.getLocation_floor());
        cv.put(facility,objectItem2.getFacility());
        cv.put(joined,objectItem2.getJoined_int());
        cv.put(img_id,objectItem2.getImg_id());

        db.insert(table_name, null, cv);
        db.close();
    }

//    public ArrayList<ObjectItem2> fetchAllBooks(){
//        ArrayList<ObjectItem2> booksList = new ArrayList<>();
//        SQLiteDatabase db=this.getReadableDatabase();
//        Cursor c= db.query(table_name, null, null, null, null, null, null);
//
//        c.moveToFirst();
//        while (!c.isAfterLast()){
//            ObjectItem2 objectItem2 = new ObjectItem2();
//
//            objectItem2.setComplaint_id(c.getString(0));
//            objectItem2.setComplaint_nature(c.getString(1));
//            objectItem2.setComplaint_number(c.getInt(2));
//            objectItem2.setLocation_text(c.getString(3));
//            objectItem2.setDate_text(c.getString(4));
//            objectItem2.setLocation_floor(c.getString(5));
//            objectItem2.setFacility(c.getString(6));
//            objectItem2.setJoined_int(c.getInt(7));
//            objectItem2.setImg_id(c.getInt(8));
//
//            booksList.add(objectItem2);
//            c.moveToNext();
//        }
//        db.close();
//        return booksList;
//    }
}
