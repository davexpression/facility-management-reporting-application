package com.example.johnson.myapplication;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by Johnson on 14/05/2018.
 */

public class Complaint implements Serializable  {

    String complaint_id;
    String complaint_nature;
    String date;

    String facility_name;
    String facility_type;
    boolean fixed;


    HashMap <String,String> joined;

    boolean new_facility;

    boolean noted_by_mofficer;
    boolean noted_by_hos;


    String location_area;
    String location_floor;
    String lodged_by;

    String mofficer_remark;

    HashMap <String,String> seen_by;
    int staff;
    int student;

    String user_id;


    public Complaint() {

    }


    public Complaint(String complaint_id, String complaint_nature, String date, String facility_name, String facility_type, boolean fixed, HashMap <String,String> joined, boolean new_facility, boolean noted_by_mofficer, boolean noted_by_hos, String location_area, String location_floor, String lodged_by, String mofficer_remark, HashMap <String,String> seen_by, int staff, int student, String user_id) {

        this.complaint_id = complaint_id;
        this.complaint_nature = complaint_nature;
        this.date = date;

        this.facility_name = facility_name;

        this.facility_type = facility_type;
        this.fixed = fixed;

        this.joined = joined;
        this.new_facility = new_facility;

        this.noted_by_mofficer = noted_by_mofficer;
        this.noted_by_hos = noted_by_hos;

        this.location_area = location_area;
        this.location_floor = location_floor;
        this.lodged_by = lodged_by;

        this.mofficer_remark = mofficer_remark;

        this.seen_by = seen_by;
        this.staff = staff;
        this.student = student;

        this.user_id = user_id;
    }

    public String getComplaint_id() {return complaint_id;}
    public void setComplaint_id(String input) {this.complaint_id = input;}

    public String getComplaint_nature() {return complaint_nature;}
    public void setComplaint_nature(String input) {this.complaint_nature = input;}

    public String getFacility_name() {return facility_name;}
    public void setFacility_name(String input) {this.facility_name = input;}

    public String getFacility_type() {return facility_type;}
    public void setFacility_type(String input) {this.facility_type = input;}

    public boolean isFixed() {return fixed;}
    public void setFixed(boolean input) {this.fixed = input;}

    public HashMap<String, String> getJoined() {return joined;}
    public void setJoined(HashMap<String, String> input) {this.joined = input;}


    public String getLocation_area() {return location_area;}
    public void setLocation_area(String input) {this.location_area = input;}

    public String getLocation_floor() {return location_floor;}
    public void setLocation_floor(String input) {this.location_floor = input;}

    public String getLodged_by() {return lodged_by;}
    public void setLodged_by (String input) {this.lodged_by = input;}

    public boolean getNoted_by_mofficer() {return noted_by_mofficer;}
    public void setNoted_by_mofficer(boolean input) {this.noted_by_mofficer = input;}

    public boolean getNoted_by_hos() {return noted_by_hos;}
    public void setNoted_by_hos (boolean input) {this.noted_by_hos = input;}


    public String getUser_id() {return user_id;}
    public void setUser_id(String input) {this.user_id = input;}


}


