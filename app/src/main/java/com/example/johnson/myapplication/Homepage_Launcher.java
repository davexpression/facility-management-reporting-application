package com.example.johnson.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import stanford.androidlib.SimpleActivity;

public class Homepage_Launcher extends SimpleActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage__launcher);

        Intent intent = getIntent();
        String email = intent.getStringExtra("email");
        $TV(R.id.welcome_id).setText("Welcome " + email);
    }

        public void proceed (View view){
        Intent intent = new Intent(this, Homepage_Student_Staff.class);
        startActivity(intent);
    }

}
