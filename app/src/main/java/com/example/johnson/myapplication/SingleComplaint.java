package com.example.johnson.myapplication;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class SingleComplaint extends Fragment {

    LinearLayout send, noted, fixed, linearLayout, linearLayout1, hos_noted;
    private DatabaseReference firebaseDatabase;
    ProgressBar progressBar;
    boolean isSent;
    String fragment, remark_text;

    public SingleComplaint() {
        // Required empty public constructor
    }

    public interface OnFragmentInteractionListener {
        public void showDrawerToggle(boolean showDrawerToggle, String lname, String fragment);
    }

    private SingleComplaint.OnFragmentInteractionListener mListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.mListener = (SingleComplaint.OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnFragmentInteractionListener");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_single_complaint, container, false);
//        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

//        linearLayout = (LinearLayout) view.findViewById(R.id.hos_single);

        final String complaint = getArguments().getString("complaint");
        final String complaint_id = getArguments().getString("complaint_id");
        final String loc = getArguments().getString("complaint_loc");
        final String name = getArguments().getString("complaint_name");
        final String total = getArguments().getString("complaint_num");
        final String student = Integer.toString(getArguments().getInt("student"));
        final String staff = Integer.toString(getArguments().getInt("staff"));

        boolean fixed_status = getArguments().getBoolean("fixed");
        boolean noted_status = getArguments().getBoolean("noted");

        final String date = getArguments().getString("date");

//        fragment = getArguments().getString("fragment");
        remark_text = getArguments().getString("remark");

        linearLayout1 = (LinearLayout) view.findViewById(R.id.mo_single);

        final TextView textView5 = (TextView) view.findViewById(R.id.mo_complaint_forward_text);
        final TextView textView6 = (TextView) view.findViewById(R.id.mo_complaint_noted_text);
        final TextView textView7 = (TextView) view.findViewById(R.id.mo_complaint_fixed_text);
        final ImageView imageView = (ImageView) view.findViewById(R.id.mo_complaint_forward_image);
        final ImageView imageView1 = (ImageView) view.findViewById(R.id.mo_complaint_noted_image);
        final ImageView imageView2 = (ImageView) view.findViewById(R.id.mo_complaint_fixed_image);


//        linearLayout1.setVisibility(View.VISIBLE);
//        linearLayout.setVisibility(View.GONE);

        send = (LinearLayout) view.findViewById(R.id.mo_complaint_send);
        noted = (LinearLayout) view.findViewById(R.id.mo_complaint_noted);
        fixed = (LinearLayout) view.findViewById(R.id.mo_complaint_fixed);

//        firebaseDatabase.child("complaints").child(complaint_id).addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//                Complaint complaint1 = dataSnapshot.getValue(Complaint.class);
                if (!remark_text.equals("null")) {

                    // imageView.setImageTintMode();
                    textView5.setTextColor(Color.parseColor("#905048"));
                    textView5.setText("Sent");
                }

                if (fixed_status == true) {
                    textView7.setTextColor(Color.parseColor("#905048"));
                    //                    imageView2.setImageTintMode();
                }

                if (noted_status == true) {
                    //                    imageView1.setImageTintMode();
                    textView6.setTextColor(Color.parseColor("#905048"));
                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });

//        else {
//            linearLayout1.setVisibility(View.GONE);
//            linearLayout.setVisibility(View.VISIBLE);
//            hos_noted = (LinearLayout) view.findViewById(R.id.hos_single);
//            progressBar = (ProgressBar) view.findViewById(R.id.noted_progress);
//
//            firebaseDatabase.child("complaints").child(complaint_id).addValueEventListener(new ValueEventListener() {
//                @Override
//                public void onDataChange(DataSnapshot dataSnapshot) {
//                    Complaint complaint1 = dataSnapshot.getValue(Complaint.class);
//
//                    if (complaint1.noted_by_hos)
//                        noted.setBackgroundColor(Color.parseColor("#6060a8"));
//                }
//
//                @Override
//                public void onCancelled(DatabaseError databaseError) {
//
//                }
//            });
//        }
//


//        final String complaint = getArguments().getString("complaint");
//        final String loc = getArguments().getString("loc");
//        final String name = getArguments().getString("name");
//        final String join = getArguments().getString("join");
//        final String date = getArguments().getString("date");

        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.trending_mo_remark);
        TextView textView_remarks = (TextView) view.findViewById(R.id.mo_complaint_remarks);


        if (remark_text.equals("null"))
            linearLayout.setVisibility(View.GONE);
        else {
            textView_remarks.setText(remark_text);
            linearLayout.setVisibility(View.VISIBLE);
        }

        TextView textView = (TextView) view.findViewById(R.id.single_complaint_description);
        TextView textView1 = (TextView) view.findViewById(R.id.single_complaint_location);
        TextView textView2 = (TextView) view.findViewById(R.id.single_complaint_detail_name);
        TextView textView4 = (TextView) view.findViewById(R.id.detail_date);

        TextView total_text = (TextView) view.findViewById(R.id.join_total);
        TextView student_text = (TextView) view.findViewById(R.id.join_student);
        TextView staff_text = (TextView) view.findViewById(R.id.join_staff);

        textView.setText(complaint);
        textView1.setText(loc);
        textView2.setText(name);
        total_text.setText(total);
        student_text.setText(student);
        staff_text.setText(staff);
        textView4.setText(date);

        final Intent intent = new Intent(getActivity(), MaintenanceSendActivity.class);
        firebaseDatabase = FirebaseDatabase.getInstance().getReference();


//        if (mode.equals("mofficer")) {


            send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    intent.putExtra("complaints", complaint);
                    intent.putExtra("complaints_id", complaint_id);
                    intent.putExtra("loc", loc);
                    intent.putExtra("name", name);
                    intent.putExtra("join", total);
                    startActivity(intent);
                }
            });
//        }

        noted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //                    imageView1.setImageTintMode();
                textView6.setTextColor(Color.parseColor("#905048"));

                firebaseDatabase.child("complaints").child(complaint_id)
                        .child("noted_by_mofficer").setValue(true);

            }
        });

        fixed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                textView7.setTextColor(Color.parseColor("#905048"));
                //                    imageView2.setImageTintMode();

                firebaseDatabase.child("complaints").child(complaint_id)
                        .child("fixed").setValue(true);
            }
        });

//        if (!mode.equals("mofficer")) {
//            hos_noted.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    firebaseDatabase.child("complaints").child(complaint_id)
//                            .child("noted_by_hos").setValue(true).addOnCompleteListener(new OnCompleteListener<Void>() {
//                        @Override
//                        public void onComplete(@NonNull Task<Void> task) {
//                            hos_noted.setBackgroundColor(Color.parseColor("#6060a8"));
//
//                        }
//                    });
//
//                }
//            });
//        }

        return  view;
    }

    public void closeFrag() {

//        Toast.makeText(getActivity(), "Authentication Successful.",
//        Toast.LENGTH_SHORT).show();

        SingleComplaint singleComplaint = (SingleComplaint) getFragmentManager().findFragmentByTag("office_view");

        getFragmentManager().beginTransaction()
                //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                .remove(singleComplaint)
                .commit();

    }

}
