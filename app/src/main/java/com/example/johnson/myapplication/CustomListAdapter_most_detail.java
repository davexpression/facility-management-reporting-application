package com.example.johnson.myapplication;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Johnson on 15/04/2018.
 */

public class CustomListAdapter_most_detail extends ArrayAdapter <ObjectItem_most_detail> {

    //to reference the Activity
    private final Activity context;

    private ArrayList<ObjectItem_most_detail> data = null;

    public CustomListAdapter_most_detail(Activity context, int layout, ArrayList<ObjectItem_most_detail> data) {

        super(context, layout, data);

        this.context = context;
        this.data = data;
    }

    public View getView(final int position,  View view, ViewGroup parent) {


        if (view == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            view = inflater.inflate(R.layout.most_lodged_complaints_detail, parent, false);
        }


        //this code sets the values of the objects to values from the arrays
        final ObjectItem_most_detail objectItem = data.get(position);

        FacilityColor facilityColor = new FacilityColor();
        final Map<String, String> facilityMap = facilityColor.getFacilityMap();


        //this code gets references to objects in the listview_row.xml file

        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.text_background);
        LinearLayout linearLayout1 = (LinearLayout) view.findViewById(R.id.join_layout);
        LinearLayout linearLayout2 = (LinearLayout) view.findViewById(R.id.list_highlight);


        ImageView imageView = (ImageView) view.findViewById(R.id.most_complaint_image);

        TextView headerTextField = (TextView) view.findViewById(R.id.most_complaints_list_header);
        TextView numberTextField = (TextView) view.findViewById(R.id.most_complaints_number);

//        final TextView textView = (TextView) view.findViewById(R.id.my_complaints_list_detail2);
//
//
//        if(viewVisibility[position]) {
//            textView.setVisibility(View.VISIBLE);
//        } else {
//            textView.setVisibility(View.GONE);
//        }
//
//        ImageView imageView = (ImageView) view.findViewById(R.id.my_complaints_expand);


        if (objectItem.new_facility == false) {
            imageView.setVisibility(View.VISIBLE);
            imageView.setImageResource(objectItem.getImg_id());

            GradientDrawable drawable = (GradientDrawable) linearLayout.getBackground();
            drawable.setStroke(1, Color.parseColor(facilityMap.get(objectItem.facility_name))); // set stroke width and stroke color
        }

        else if (objectItem.new_facility == true) {
            imageView.setVisibility(View.INVISIBLE);
            linearLayout.setBackgroundResource(R.drawable.copyborder4_one);
        }

//        if (objectItem.complaint_number > 1) {
//            linearLayout1.setVisibility(View.VISIBLE);
//        }
//
//        else
//            linearLayout1.setVisibility(View.GONE);

        if (objectItem.seen == false)
            linearLayout2.setBackgroundColor(Color.parseColor("#ecf3ff"));
        else
            linearLayout2.setBackgroundColor(Color.WHITE);

        imageView.setImageResource(objectItem.getImg_id());
        headerTextField.setText(objectItem.complaint_nature);
        numberTextField.setText(Integer.toString(objectItem.complaint_number));


//        imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                    textView.setVisibility(View.VISIBLE);
//                    viewVisibility[position] = true;
//                }
//        });
        return view;

    };
}
