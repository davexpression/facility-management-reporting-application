package com.example.johnson.myapplication;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.CountDownTimer;
import android.os.Handler;
import android.preference.EditTextPreference;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FacilityComplaint extends AppCompatActivity{
    private Spinner spinner1;
    private EditText editText, editText2;
    private View hideKeyboard;
    private SharedPreferences sharedPreferences;
    private ProgressBar progressBar;
    private CountDownTimer mCountDownTimer;
    private  int i;
    private  AlertDialog.Builder builder1;
    private Button button;
    private FirebaseAuth mAuth;
    boolean joined, creator;
    int num_count = 0, count = 0, general_count = 0;

    TextView textView, textView_facility, active_text;
    LinearLayout linearLayout_active;
    ListView listView;

    ArrayList<ObjectItem_most> objectItems = new ArrayList<>();

    Dialog dialog;
    DialogFragment dialogFragment;
    MostComplaintView2 mostComplaintView2;
    String mode, office_identifier = "null";


    private ScrollView scrollView;
    String location_floor, location_area, facility_name, facility_type, complaint_nature;

    private static final String[] suggestions = {" Switch just faulty", " Switch not working",
            " Switch faulty", " Switch light not working", " Switch handle faulty", " None"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facility_complaint);

        mAuth = FirebaseAuth.getInstance();

        Toolbar toolbar = (Toolbar) findViewById(R.id.main1_toolbar_complaint);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mode = sharedPreferences.getString("mode", null);

        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.no_new_facility_layout1);
        LinearLayout linearLayout1 = (LinearLayout) findViewById(R.id.no_new_facility_layout2);
        LinearLayout linearLayout2 = (LinearLayout) findViewById(R.id.new_facility_layout);
//        final LinearLayout linearLayout3 = (LinearLayout) findViewById(R.id.complaint_see_all);

        linearLayout_active = (LinearLayout) findViewById(R.id.linear_click);




        if (mode.equals("staff")) {
            office_identifier = sharedPreferences.getString("office_identifier", null);
        }

        scrollView = (ScrollView) findViewById(R.id.facility_complaint_whole);
        FacilityImage facilityImage = new FacilityImage();
        Map<String, Integer> facilityMap = facilityImage.getFacilityMap();


        Intent intent = getIntent();
        location_floor = intent.getStringExtra("office_floor");
        location_area = intent.getStringExtra("office_identifier");
        facility_name = intent.getStringExtra("facility_name");
        facility_type = intent.getStringExtra("facility_type");

        textView_facility = (TextView) findViewById(R.id.complaint_facility_name);

        TextView textView_color = (TextView) findViewById(R.id.complaint_subject);
        final int color = textView_color.getCurrentTextColor();


        String use_location_floor = location_floor + " " + "|" + " ";

//        Log.v("FacilityComplaint", "Towoju " + location_area);

        if (sharedPreferences.getBoolean("new_facility", false) == false) {
            textView = (TextView) findViewById(R.id.location_in_complaint);
            ImageView currentImageView = (ImageView) findViewById(R.id.complaint_image);
            currentImageView.setImageResource( facilityMap.get(facility_name).intValue() );

        }

        else {
            linearLayout.setVisibility(View.GONE);
            linearLayout1.setVisibility(View.GONE);

            linearLayout2.setVisibility(View.VISIBLE);

            textView = (TextView) findViewById(R.id.complaint_facility_location);
            textView_facility.setText(facility_name);

        }


        textView.setText(use_location_floor + location_area);

//        int img_id = intent.getIntExtra("img_id", 1);

        builder1 = new AlertDialog.Builder(this);


        progressBar = (ProgressBar) findViewById(R.id.complaint_progress_bar);
//        button = (Button) findViewById(R.id.complaint_proceed);



//        int img_id = sharedPreferences.getInt("img_id", 1);

//            View inflatedView = getLayoutInflater().inflate(R.layout.fragment_first, null);
//            ImageView imageView = (ImageView) inflatedView.findViewById(img_id);

//
//            Drawable drawable = imageView.getDrawable();
//            currentImageView.setImageDrawable(drawable);

//        spinner1 = (Spinner) findViewById(R.id.spinner1);
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(FacilityComplaint.this,
//                android.R.layout.simple_spinner_dropdown_item, suggestions);
//
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinner1.setAdapter(adapter);
//        spinner1.setOnItemSelectedListener(this);
//        spinner1.setSelection(5);

        editText = (EditText) findViewById(R.id.complaint_nature);

        editText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                hideKeyboard = view;
                return false;
            }
        });

//        editText2 = (EditText) findViewById(R.id.complaint_description);
//
//        editText.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                hideKeyboard = view;
//                return false;
//            }
//        });

//        linearLayout_active = (LinearLayout) findViewById(R.id.active_complaint);

//        final HashMap<String, Integer> map = new HashMap<>();

//        final CustomListAdapter_most customListAdapter = new CustomListAdapter_most(this, R.layout.most_lodged_complaints, objectItems);
//        listView.setAdapter(customListAdapter);

        DatabaseReference fb = FirebaseDatabase.getInstance().getReference();
        DatabaseReference complaint = fb.child("complaints");
        complaint.keepSynced(true);

        Query query;
        query = complaint.orderByChild("facility_name").equalTo(facility_name);

        query.addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                final Complaint complaint = dataSnapshot.getValue(Complaint.class);

                if (complaint.location_area.equals(location_area) & complaint.fixed == false) {
                    count++;
                }

                if (count > 0) {
                    active_text.setText("Active Complaints (" + count + ")");
                    active_text.setTextColor(color);
                    active_text.setTypeface(Typeface.DEFAULT_BOLD);
                    linearLayout_active.setVisibility(View.VISIBLE);
                }
//
//                    //user.orderByChild("office_identifier").equalTo(complaint.location_area).addValueEventListener(new ValueEventListener() {
//                    //if (mode.equals("staff") & office_identifier.equals(complaint.location_area)
//
//
//
//                        map.put(dataSnapshot.getKey(), count);
//                        count++;
//                        HashMap<String, String> joined = (HashMap<String, String>) complaint.joined;
//
//                        ObjectItem_most objectItem_most = new ObjectItem_most(joined.size(), complaint.complaint_nature, "4 Jul");
//                        objectItem_most.setComplaint_id(complaint.complaint_id);
//
//                        for (String key : joined.keySet()) {
//
//                            if (key.equals(mAuth.getCurrentUser().getUid())) {
//                                objectItem_most.setJoined(true);
//                            }
//                        }
//
//                        if (complaint.user_id.equals(mAuth.getCurrentUser().getUid()))
//                            objectItem_most.setCreator(true);
//
//                        objectItems.add(objectItem_most);
//
//                        if (num_count >= 2){
//                            return;
//                        }
//
//                        customListAdapter.notifyDataSetChanged();
//                        num_count++;
//
//
//                            if (count == 0) {
//                                linearLayout4.setVisibility(View.GONE);
//
//                            }
//                            else{
//                                active_text.setText("Active Complaints (" + count + ")");
//                                active_text.setTextColor(color);
//                                active_text.setTypeface(active_text.getTypeface(), Typeface.BOLD);
//                            }
//
//                            if ( count >=3 )
//                                linearLayout3.setVisibility(View.VISIBLE);
//                        }


            }


            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

//                    final Complaint complaint = dataSnapshot.getValue(Complaint.class);
//
//                    int index = map.get(dataSnapshot.getKey());
//
//                    HashMap<String,String> joined =  (HashMap<String, String>) complaint.joined;
//
//                    ObjectItem_most objectItem_most = new ObjectItem_most(joined.size(), complaint.complaint_nature, "4 Jul");
//                    objectItem_most.setComplaint_id(complaint.complaint_id);
//
//                    objectItem_most.setJoined(true);
//
//                    objectItems.set(index, objectItem_most);
//                    customListAdapter.notifyDataSetChanged();

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        active_text = (TextView) findViewById(R.id.complaint_active);



//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
////                Intent intent = new Intent(MostLodgedComplaint.this, MaintenanceComplaintActivity.class);
////                String complaint = objectItems.get(position).complaint_nature;
////                String complaint_id = objectItems.get(position).getComplaint_id();
////
////                intent.putExtra("complaint", complaint);
////                intent.putExtra("complaint_id", complaint_id);
////                startActivity(intent);
//
//                Bundle bundle = new Bundle();
//
//                String complaint = objectItems.get(position).complaint_nature;
//                String complaint_id = objectItems.get(position).complaint_id;
//
//                joined = objectItems.get(position).joined;
//                creator = objectItems.get(position).creator;
//
//                bundle.putString("complaint", complaint);
//                bundle.putString("complaint_id", complaint_id);
//
//                bundle.putBoolean("joined", joined);
//                bundle.putBoolean("creator", creator);
//
//                mostComplaintView2 = new MostComplaintView2();
//                mostComplaintView2.setArguments(bundle);
//
////                listView.setVisibility(View.GONE);
//
////                getSupportFragmentManager().beginTransaction()
////                        //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
////
////                        .replace(R.id.most_complaints_lists,mostComplaintView2,"view2")
////                        .addToBackStack("view2")
////                        .commit();
////
//                android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//
//                dialogFragment = mostComplaintView2;
//                dialogFragment.show(ft, "dialog");
//
//
////                AlertDialog.Builder builder = new AlertDialog.Builder(MostLodgedComplaint.this);
////                View alertView = getLayoutInflater().inflate(R.layout.fragment_most_complaint_view2, null);
////
////
////                builder.setView(alertView);
////                builder.show();
//
////                dialog = new Dialog(FacilityComplaint.this);
////                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
////                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
////                dialog.setContentView(R.layout.fragment_most_complaint_view2);
////                dialog.show();
//            }
//        });

    }

    @Override
    public void onResume() {
        super.onResume();
        scrollView.setVisibility(View.VISIBLE);
    }
//        super.onResume();
//        Intent intent = getIntent();
//        int img_id = intent.getIntExtra("img_id", 1);
//
//
//        View inflatedView = getLayoutInflater().inflate(R.layout.fragment_first, null);
//        ImageView imageView = (ImageView) inflatedView.findViewById(img_id);
//        ImageView currentImageView = (ImageView) findViewById(R.id.complaint_image);
//
//        Drawable drawable = imageView.getDrawable();
//        currentImageView.setImageDrawable(drawable);
//    }


//    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
//        String suggestion_selected;
//
//        switch (position) {
//            case 0:
//                suggestion_selected = spinner1.getSelectedItem().toString();
//                editText.setText(suggestion_selected);
//                hideKeyboardMethod(hideKeyboard);
//                editText.setEnabled(false);
//                editText.setTextColor(Color.GRAY);
//                editText.setTypeface(Typeface.DEFAULT_BOLD);
//                break;
//
//            case 1:
//                suggestion_selected = spinner1.getSelectedItem().toString();
//                editText.setText(suggestion_selected);
//                hideKeyboardMethod(hideKeyboard);
//                editText.setEnabled(false);
//                editText.setTextColor(Color.GRAY);
//                editText.setTypeface(Typeface.DEFAULT_BOLD);
//
//                break;
//
//            case 2:
//                suggestion_selected = spinner1.getSelectedItem().toString();
//                editText.setText(suggestion_selected);
//                hideKeyboardMethod(hideKeyboard);
//                editText.setEnabled(false);
//                editText.setTextColor(Color.GRAY);
//                editText.setTypeface(Typeface.DEFAULT_BOLD);
//                break;
//
//            case 3:
//                suggestion_selected = spinner1.getSelectedItem().toString();
//                editText.setText(suggestion_selected);
//                hideKeyboardMethod(hideKeyboard);
//                editText.setEnabled(false);
//                editText.setTextColor(Color.GRAY);
//                editText.setTypeface(Typeface.DEFAULT_BOLD);
//                break;
//
//            case 4:
//                suggestion_selected = spinner1.getSelectedItem().toString();
//                editText.setText(suggestion_selected);
//                hideKeyboardMethod(hideKeyboard);
//                editText.setEnabled(false);
//                editText.setTextColor(Color.GRAY);
//                editText.setTypeface(Typeface.DEFAULT_BOLD);
//                break;
//
//            case 5:
//                editText.getText().clear();
//                editText.setEnabled(true);
//                editText.setTextColor(Color.BLACK);
//                editText.setTypeface(Typeface.DEFAULT);
//                break;
//        }
//    }
//
//    public void onNothingSelected(AdapterView<?> parent) {
//
//    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.home, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//
//        if (id == R.id.action_settings) {
//            startActivity(new Intent(this, SettingsActivity.class));
//            return true;
//        }
//
//        if (id == android.R.id.home) {
//            super.onBackPressed();
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }



//    public void light_bulb (View view) {
//        spinner1.performClick();
//
//    }


    public void hideKeyboardMethod (View view) {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (view != null)
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

    }

    public void complaint_proceed (View view) {
//        SharedPreferences.Editor editor =
//                PreferenceManager.getDefaultSharedPreferences(this).edit();
//
//        editor.putString("sync_frequency2","hello");
//        editor.commit();
//        secondBuilder();

//        String complaint_description;
//
//        if (spinner1.getSelectedItemPosition() != 5 )
//            complaint_subject = spinner1.getSelectedItem().toString();
//        else
//            complaint_subject = editText.getText().toString();
//
//        String complaint_nature = editText.getText().toString();
//
//        Intent intent = new Intent(this, PreviewActivity.class);
//        intent.putExtra("facility_name", facility_name);
//        intent.putExtra("facility_type", facility_type);
//        intent.putExtra("location_floor", location_floor);
//        intent.putExtra("location_area", location_area);
//        intent.putExtra("complaint_nature", complaint_nature);
//
//        startActivity(intent);

        hideKeyboardMethod(hideKeyboard);
        scrollView.setVisibility(View.GONE);
        progressBar.setProgress(i);
        mCountDownTimer=new CountDownTimer(3000,1000) {

            @Override
            public void onTick(long millisUntilFinished) {

                i++;
                progressBar.setProgress((int)i*100/(3000/1000));
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {

                complaint_nature = editText.getText().toString();

                Intent intent = new Intent(FacilityComplaint.this, PreviewActivity.class);
                intent.putExtra("facility_name", facility_name);
                intent.putExtra("facility_type", facility_type);
                intent.putExtra("location_floor", location_floor);
                intent.putExtra("location_area", location_area);
                intent.putExtra("complaint_nature", complaint_nature);

                progressBar.setVisibility(View.GONE);
                startActivity(intent);

            }
        };
        mCountDownTimer.start();



    }

//    public void secondBuilder() {
//
//        LayoutInflater layoutInflater = getLayoutInflater();
//        final View view1 = layoutInflater.inflate(R.layout.sign_in, null);
//        EditText editText = (EditText) view1.findViewById(R.id.dialog_editText);
//        editText.setSelection(editText.getText().length());
//
//        builder1.setMessage("How many switches are their light not working?");
//        builder1.setCancelable(false);
//        builder1.setView(view1);
//        builder1.setPositiveButton("OK",
//                new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int id) {
//                        progressAgain();
//                    }
//                });
//        builder1.setNegativeButton("Cancel",
//                new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int id) {
//                        // code to run when Cancel is pressed
//                    }
//                });
//
//        AlertDialog alert = builder1.create();
//        alert.show();
//
//    }
//

//Toast.makeText(FacilityComplaint.this, "Authentication Successful.",
//    Toast.LENGTH_SHORT).show();


    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }

    public void complaint_back (View view) {
        super.onBackPressed();
    }


//    public void active_complaint(View view) {
//
//        if (linearLayout_active.getVisibility() == View.VISIBLE)
//            linearLayout_active.setVisibility(View.GONE);
//        else
//            linearLayout_active.setVisibility(View.VISIBLE);
//    }

//    public void close_dialog(View view) {
//
//        dialogFragment.dismiss();
//    }

    public void see_all_join(View view) {

//        scrollView.setVisibility(View.INVISIBLE);



        Bundle bundle = new Bundle();
        bundle.putString("location", location_area);
        bundle.putString("facility", facility_name);

        FragmentManager fragmentManager = getSupportFragmentManager();
        JoinFragment newFragment = new JoinFragment();
        newFragment.setArguments(bundle);

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.replace(android.R.id.content, newFragment).addToBackStack(null).commit();

    }

}
