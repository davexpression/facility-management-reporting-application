package com.example.johnson.myapplication;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class ExistingComplaints extends AppCompatActivity {

    String header = "The Switch at the Lecture room is faulty, come and fix as soon as possible";
    String header1 = "The Light bulb at the Lab faulty, come and fix as soon as possible";
    String header2 = "The Air conditioner at the News room is faulty, come and fix as soon as possible";
    String header3 = "The Ceiling Fans at the LR is faulty, come and fix as soon as possible";
    String header4 = "The Pipe at the LR is faulty, come and fix as soon as possible";

    String detail = "I can perceive that the switch is faulty.. ";
    String detail1 = "I can perceive that the bulb is faulty..";
    String detail2 = "I can perceive that the A.C is faulty..";
    String detail3 = "I can perceive that the fans are faulty..";
    String detail4 = "I can perceive that the pipe is faulty..";

    String more_detail = "I can perceive that the switch is faulty..I can perceive that the switch is faulty.. I can perceive that the switch is vertyy.. I can perceive that the switch is vertyy..";
    String more_detail1 = "I can perceive that the bulb is faulty..I can perceive that the switch is vertyy.. I can perceive that the switch is vertyy.. I can perceive that the switch is vertyy..";
    String more_detail2 = "I can perceive that the A.C is faulty..I can perceive that the switch is vertyy.. I can perceive that the switch is vertyy.. I can perceive that the switch is vertyy..";
    String more_detail3 = "I can perceive that the fans are faulty..I can perceive that the switch is vertyy.. I can perceive that the switch is vertyy.. I can perceive that the switch is vertyy..";
    String more_detail4 = "I can perceive that the pipe is faulty..I can perceive that the switch is vertyy.. I can perceive that the switch is vertyy.. I can perceive that the switch is vertyy..";

    ListView listView;

    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_existing_complaints);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final ObjectItem3[] objectItemData = new ObjectItem3[8];
        objectItemData[0] = new ObjectItem3(header, detail, more_detail, "12", "Beside LR 5");
        objectItemData[1] = new ObjectItem3(header1, detail1, more_detail1, "18", "Beside LR 4");
        objectItemData[2] = new ObjectItem3(header2, detail2, more_detail2, "41", "Beside LR 2");
        objectItemData[3] = new ObjectItem3(header3, detail3, more_detail3, "10", "Beside LR 1");
        objectItemData[4] = new ObjectItem3(header, detail, more_detail, "22", "Beside LR 2");
        objectItemData[5] = new ObjectItem3(header4, detail4, more_detail4, "12", "Beside LR 5");
        objectItemData[6] = new ObjectItem3(header1, detail1, more_detail1, "35", "Beside LR 3");
        objectItemData[7] = new ObjectItem3(header3, detail, more_detail3, "15", "Beside LR 2");

        CustomListAdapter3 customListAdapter = new CustomListAdapter3(this,objectItemData);
        listView = (ListView) findViewById(R.id.existing_complaint_lists);

        listView.setAdapter(customListAdapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Intent intent = new Intent(ExistingComplaints.this, TrendingDetail.class);
                String header = objectItemData[position].header;
                String detail = objectItemData[position].detail;

//                intent.putExtra("list_header", header);
//                intent.putExtra("list_detail", detail);

                startActivity(intent);
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }

        if (id == android.R.id.home) {
            super.onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
