package com.example.johnson.myapplication;


import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class ComplaintView extends DialogFragment {

    String complaint_id;

    public ComplaintView() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_most_complaint_officer, container, false);

        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#cccccc")));

        String complaint = getArguments().getString("complaint");
        complaint_id = getArguments().getString("complaint_id");

        TextView textView = (TextView) view.findViewById(R.id.hos_complaint_description);
        textView.setText(complaint);


        ImageView imageView = (ImageView) view.findViewById(R.id.dialog_complaint);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        return view;



    }
}
