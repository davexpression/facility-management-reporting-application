package com.example.johnson.myapplication;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Johnson on 12/05/2018.
 */

public class FacilityColor {

    private static final Map<String, String> facilityMap;

    static {
        Map<String, String> myMap = new HashMap<String, String>();
        myMap.put("Alarm clock", "#aff5a0");
        myMap.put("Dish Washer", "#afb9d2");
        myMap.put("new_facility", "#cccccc");
        myMap.put("Door Handle", "#ebebec");
        myMap.put("Light Switch", "#cfd8dc");
        myMap.put("Socket", "#e8edee");
        myMap.put("Shelves", "#bfa993");
        myMap.put("Copper Wire", "#ac92eb");
        myMap.put("Light Bulb", "#d5d3d8");
        myMap.put("Fan", "#ebebeb");
        myMap.put("Air Conditioner", "#bac6cc");
        myMap.put("Staircase", "#e1dcd8");
        myMap.put("Water Tap", "#cfdce5");
        myMap.put("WhiteBoard", "#ffffff");
        myMap.put("Door", "#dde4ed");
        myMap.put("Wall", "#eef3fa");
        myMap.put("Pipe", "#ccf2fe");
        myMap.put("Printer", "#eef3fa");
        myMap.put("Refrigerator", "#e4eaf6");
        myMap.put("Window", "#eef3fa");
        myMap.put("Projector", "#eef3fa");
        myMap.put("Couch", "#eef3fa");
        myMap.put("Drawers", "#d1b99f");

        myMap.put("Peeler", "#a3cad3");
        myMap.put("Arm Chair", "#8fbac1");
        myMap.put("Glass", "#daefef");
        myMap.put("WC", "#cfdce5");
        myMap.put("Bath Tub", "#b3d9e2");
        myMap.put("Blade", "#b6e0e0");
        myMap.put("Chair", "#d8e4ee");
        myMap.put("Spray", "#afccd8");
        myMap.put("Extractor","#cbebef");
        myMap.put("Fence", "#eef3fa");
        myMap.put("Stool", "#eef3fa");
        myMap.put("CorkScrew", "#b7cacd");
        myMap.put("Epilator", "#e4eaf6" );
        myMap.put("Lamp", "#d8e4ee");
        myMap.put("Kettle", "#ffea82");
        myMap.put("Juicer", "#c7cfe2");
        myMap.put("Vacuum Cleaner", "#d7deed");
        myMap.put("Telephone", "#e4eaf6");
        myMap.put("Television", "#eef3fa");
        myMap.put("Projector Stand", "#eef3fa");
        myMap.put("Coffee Machine", "#dfd7d5");
        myMap.put("Roof", "#eef3fa");
        myMap.put("Roller","#eef3fa");
        myMap.put("Tent", "#eef3fa");
        myMap.put("Tiles", "#eef3fa");
        myMap.put("Mixer", "#eef3fa");
        myMap.put("Opener", "#eef3fa");
        myMap.put("Sound System", "#d7deed");
        myMap.put("Dust Pan", "#eef3fa");
        myMap.put("Oven", "#a3d8d6");
        myMap.put("Cupboard", "#eef3fa");
        myMap.put("Multicooker", "#c7cfe2");
        myMap.put("Toaster", "#fff096");
        myMap.put("Iron", "#eef3fa");
        myMap.put("Broom", "#eef3fa");
        myMap.put("Scale", "#d8edef");
        myMap.put("Plunger", "#eef3fa");
        myMap.put("Boiler", "#93b6c1");


        facilityMap = Collections.unmodifiableMap(myMap);
    }

    public Map<String, String> getFacilityMap() {
        return facilityMap;
    }

}
