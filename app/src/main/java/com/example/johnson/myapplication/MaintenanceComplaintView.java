package com.example.johnson.myapplication;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 */
public class MaintenanceComplaintView extends Fragment {

    LinearLayout send, noted, fixed;
    private DatabaseReference firebaseDatabase;
    private DatabaseReference complaints;
    boolean isSent;

    String last_name, fragment, remark_text, remarks;


    public MaintenanceComplaintView() {
        // Required empty public constructor
    }

    public interface OnFragmentInteractionListener {
        public void showDrawerToggle(boolean showDrawerToggle, String lname, String fragment);
    }

    private OnFragmentInteractionListener mListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mListener.showDrawerToggle(false, last_name, fragment);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_maintenance_complaint_view, container, false);

        firebaseDatabase = FirebaseDatabase.getInstance().getReference();

        final String complaint = getArguments().getString("complaint");
        final String complaint_id = getArguments().getString("complaint_id");
        final String loc = getArguments().getString("complaint_loc");
        final String name = getArguments().getString("complaint_name");
        final String total = getArguments().getString("complaint_num");
        final String date = getArguments().getString("date");

        final String student = Integer.toString(getArguments().getInt("student"));
        final String staff = Integer.toString(getArguments().getInt("staff"));

        boolean fixed_status = getArguments().getBoolean("fixed");
        boolean noted_status = getArguments().getBoolean("noted");

        Date myDate = null;
//

        try {
            java.text.DateFormat format = new SimpleDateFormat("EEE LLL dd HH:mm:ss Z yyyy");
            myDate = format.parse(date);
        }
        catch (ParseException e){ }
        //this code gets references to objects in the listview_row.xml file

        String string = DateUtils.getRelativeDateTimeString(getActivity(), myDate.getTime(), DateUtils.MINUTE_IN_MILLIS,
                DateUtils.DAY_IN_MILLIS * 4, DateUtils.FORMAT_ABBREV_RELATIVE).toString();

        string = string.substring(0, string.indexOf(","));

        fragment = getArguments().getString("fragment");
        remark_text = getArguments().getString("remark");


        TextView total_text = (TextView) view.findViewById(R.id.join_total);
//        TextView student_text = (TextView) view.findViewById(R.id.join_student);
//        TextView staff_text = (TextView) view.findViewById(R.id.join_staff);

        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.remarks_layout);
        TextView remark = (TextView) view.findViewById(R.id.mo_complaint_remarks);
        remark.setText(remark_text);

        last_name = getArguments().getString("last_name");
        final boolean new_facility = getArguments().getBoolean("new");

        final TextView textView5 = (TextView) view.findViewById(R.id.mo_complaint_forward_text);

        if (fragment.equals("sent") | fragment.equals("noted") | fragment.equals("fixed") | fragment.equals("most")) {
            linearLayout.setVisibility(View.VISIBLE);
            if (remark_text.equals("null"))
                remark.setText("none");

//            else {
//                textView5.setTextColor(Color.parseColor("#905048"));
//                textView5.setText("Sent");
//                // imageView.setImageTintMode();
//            }
        }

//        if (fragment.equals("most"))  {
//            total_text.setText("Total:" + total");
//            student_text.setVisibility(View.VISIBLE);
//            staff_text.setVisibility(View.VISIBLE);
//
//
//        }



            if (Integer.parseInt(total) > 1)
                total_text.setText("Total:" + total);
            else
                total_text.setText("None");




        TextView textView = (TextView) view.findViewById(R.id.mo_complaint_description);
        TextView textView1 = (TextView) view.findViewById(R.id.mo_complaint_view_loc);
        TextView textView2 = (TextView) view.findViewById(R.id.mo_complaint_view_name);
        TextView textView4 = (TextView) view.findViewById(R.id.detail_date);

        final TextView textView6 = (TextView) view.findViewById(R.id.mo_complaint_noted_text);
        final TextView textView7 = (TextView) view.findViewById(R.id.mo_complaint_fixed_text);

        final ImageView imageView = (ImageView) view.findViewById(R.id.mo_complaint_forward_image);
        final ImageView imageView1 = (ImageView) view.findViewById(R.id.mo_complaint_noted_image);
        final ImageView imageView2 = (ImageView) view.findViewById(R.id.mo_complaint_fixed_image);

        textView.setText(complaint);
        textView1.setText(loc);
        textView2.setText(name);
        textView4.setText(string);


        final Intent intent = new Intent(getActivity(), MaintenanceSendActivity.class);

        send = (LinearLayout) view.findViewById(R.id.mo_complaint_send);
        noted = (LinearLayout) view.findViewById(R.id.mo_complaint_noted);
        fixed = (LinearLayout) view.findViewById(R.id.mo_complaint_fixed);

        firebaseDatabase.child("complaints").child(complaint_id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                    Complaint complaint1 = dataSnapshot.getValue(Complaint.class);

                    if (!complaint1.mofficer_remark.equals("null")) {
                        textView5.setTextColor(Color.parseColor("#905048"));
                        textView5.setText("Sent");
                        imageView.setColorFilter(Color.parseColor("#905048"));
                    }

                    if (complaint1.fixed == true) {
                        textView7.setTextColor(Color.parseColor("#905048"));
                        imageView2.setColorFilter(Color.parseColor("#905048"));
                    }

                    if (complaint1.noted_by_mofficer == true) {
                        imageView1.setColorFilter(Color.parseColor("#905048"));
                        textView6.setTextColor(Color.parseColor("#905048"));
                    }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                intent.putExtra("complaints", complaint);
                intent.putExtra("complaints_id", complaint_id);
                intent.putExtra("loc", loc);
                intent.putExtra("name", name);
                intent.putExtra("join", total);
                intent.putExtra("new", new_facility);
                startActivity(intent);
            }
        });

        noted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                imageView1.setImageTintMode();
                textView6.setTextColor(Color.parseColor("#905048"));
                imageView1.setColorFilter(Color.parseColor("#905048"));
                firebaseDatabase.child("complaints").child(complaint_id)
                        .child("noted_by_mofficer").setValue(true);

            }
        });

        fixed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                textView7.setTextColor(Color.parseColor("#905048"));
                imageView2.setColorFilter(Color.parseColor("#905048"));
                firebaseDatabase.child("complaints").child(complaint_id)
                        .child("fixed").setValue(true);
            }
        });

        return  view;
    }

    public void closeFrag() {

//        Toast.makeText(getActivity(), "Authentication Successful.",
//        Toast.LENGTH_SHORT).show();

        MaintenanceComplaintView maintenanceComplaintView = (MaintenanceComplaintView) getFragmentManager().findFragmentByTag("mo_view");

        getFragmentManager().beginTransaction()
                //.setCustomAnimations(R.anim.enter,R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                .remove(maintenanceComplaintView)
                .commit();
    }

}
