package com.example.johnson.myapplication;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions and extra parameters.
 */
public class NotificationsIntentService extends Service {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS

    public static final String ACTION_FOO = "com.example.johnson.myapplication.action.FOO";
    public static final String ACTION_BAZ = "com.example.johnson.myapplication.action.BAZ";

    // TODO: Rename parameters
    public static final String EXTRA_PARAM1 = "com.example.johnson.myapplication.extra.PARAM1";
    public static final String EXTRA_PARAM2 = "com.example.johnson.myapplication.extra.PARAM2";
    public static final int id = 1234;

    public NotificationsIntentService() {

    }


//    @Override
//    public void onCreate() {
//        super.onCreate();
//        startForeground(1, );
//    }

    @Override
    public int onStartCommand (Intent intent, int flags, int startId) {
        if (intent.getAction().equals("action")) {
//            final String action = intent.getAction();

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                    .setContentTitle("Complaint Update")
                    .setContentText("New User joined in complaining")
                    .setAutoCancel(true)
                    .setSmallIcon(R.drawable.ic_menu_send)
                    .setWhen(System.currentTimeMillis());

            Intent launch = new Intent(this, HomeActivity.class);
            launch.putExtra("start_frag", true);

            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, launch, 0);

            Notification notification = builder.getNotification();
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            manager.notify(id, notification);
        }
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
//    private void handleActionFoo(String param1, String param2) {
//        // TODO: Handle action Foo
//        throw new UnsupportedOperationException("Not yet implemented");
//    }
//
//    /**
//     * Handle action Baz in the provided background thread with the provided
//     * parameters.
//     */
//    private void handleActionBaz(String param1, String param2) {
//        // TODO: Handle action Baz
//        throw new UnsupportedOperationException("Not yet implemented");
//    }
}
