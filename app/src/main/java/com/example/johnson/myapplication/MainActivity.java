package com.example.johnson.myapplication;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.vistrav.pop.Pop;

import java.util.HashMap;
import java.util.Map;

import stanford.androidlib.SimpleActivity;




public class MainActivity extends SimpleActivity {

    private EditText editText;
    private EditText editText2;
    private ProgressBar progressBar;
    private CountDownTimer mCountDownTimer;
    private  int i;
    private View hideKeyboard;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

//    private static FirebaseDatabase firebaseDatabase;
    private FirebaseAuth mAuth;
    private DatabaseReference firebaseDatabase;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private static final String TAG = "Main Activity";
    private Intent intent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

//        firebaseDatabase = FirebaseDatabase.getInstance();
//        firebaseDatabase.setPersistenceEnabled(true);
        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();

                if (user != null) {

                    String mode = sharedPreferences.getString("mode", null);
                    if( mode != null) {

                        if (mode.equals("staff") | mode.equals("student"))
                            intent = new Intent(MainActivity.this, HomeActivity.class);

                        if (mode.equals("mofficer"))
                            intent = new Intent(MainActivity.this, MaintenanceDrawer.class);

                        if(mode.equals("hos_civil") | mode.equals("hos_mechanical") | mode.equals("hos_electrical"))
                            intent = new Intent(MainActivity.this, HeadOfServicesDrawer.class);

                        if(intent != null)
                            startActivity(intent);
                    }

                    // User is signed in
//                  Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());


                } else {
                    // User is signed out
//                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };

        getSupportActionBar().hide();

        i=0;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sharedPreferences.edit();
        editor.clear();

        editText = (EditText) findViewById(R.id.login_email);
        editText2 = (EditText) findViewById(R.id.login_password);
        progressBar = (ProgressBar) findViewById(R.id.home_progress_bar);

        editText.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                hideKeyboard = view;
                return false;
            }

        });

        editText2.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                hideKeyboard = view;
                return false;
            }

        });

    }

    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);

    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }

    }


    public void sign_in(View view) {

        String email = editText.getText().toString();
        String password = editText2.getText().toString();

        hideKeyboardMethod(hideKeyboard);
        progressBar.setVisibility(View.VISIBLE);

            mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if(task.isSuccessful()) {
                            //Toast.makeText(Login.this, "Authentication Successful.",
                            //Toast.LENGTH_SHORT).show();

                            DatabaseReference fb = FirebaseDatabase.getInstance().getReference();
                            DatabaseReference user = fb.child("users/" + mAuth.getCurrentUser().getUid());
                            user.addListenerForSingleValueEvent(new ValueEventListener() {

                            @Override
                            public void onDataChange(DataSnapshot data) {

                                User user = data.getValue(User.class);
                                editor.putString("mode", user.type);


                                if ( (user.type.equals("staff")) | (user.type.equals("student")) ) {
                                    editor.putString("first_name", user.first_name);
                                    editor.putString("last_name", user.last_name);
                                    intent = new Intent(MainActivity.this, HomeActivity.class);

                                }

                                if (user.type.equals("student"))
                                    editor.putString("matric_no", user.matric);

                                else if (user.type.equals("staff")) {
                                    editor.putString("office_floor", user.office_floorNo);
                                    editor.putString("office_identifier", user.office_identifier);
                                    editor.putString("staff_rank", user.prefixNo);
                                }

                                else if (user.type.equals("mofficer")){
                                    editor.putString("mo_first_name", user.first_name);
                                    editor.putString("mo_last_name", user.last_name);
                                    editor.putString("mo_phone_number", user.phone_number);
                                    intent = new Intent(MainActivity.this, MaintenanceDrawer.class);

                                }

                                else if ( (user.type.equals("hos_civil")) | (user.type.equals("hos_electrical")) | (user.type.equals("hos_mechanical")) ) {
                                    editor.putString("hos_first_name", user.first_name);
                                    editor.putString("hos_last_name", user.last_name);
                                    intent = new Intent(MainActivity.this, HeadOfServicesDrawer.class);
                                }

                                else
                                    Toast.makeText(MainActivity.this, "Invalid Login Parameters.",
                                            Toast.LENGTH_SHORT).show();

                                editor.commit();


                                progressBar.setVisibility(View.GONE);


                                if (intent != null)
                                    startActivity(intent);

    //                                    Log.v(TAG, "Towoju " + user.type);
    //                                    Log.v(TAG, "Towoju " + user.getEmail() + user.getFirst_name() + user.getId() + user.getLast_name() +
    //                                            user.getMatric() + user.getOffice_floorNo() + user.getOffice_identifier() + user.getOffice_location() +
    //                                            user.getPassword() + user.getPrefix() + user.getPrefixNo() + user.getType());
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Log.v(TAG, "Towoju " + databaseError);

                                }
                            });
//



                        }
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        else {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(MainActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                });
        }

//        mAuth.signInWithEmailAndPassword(email, password)
//                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task<AuthResult> task) {
//
//                        if(task.isSuccessful()) {
//                            //Toast.makeText(Login.this, "Authentication Successful.",
//                            //Toast.LENGTH_SHORT).show();
//
//                            DatabaseReference fb = FirebaseDatabase.getInstance().getReference();
//                            DatabaseReference user = fb.child("users/" + mAuth.getCurrentUser().getUid());
//                            user.addListenerForSingleValueEvent(new ValueEventListener() {
//
//                            @Override
//                            public void onDataChange(DataSnapshot data) {
//
//                                User user = data.getValue(User.class);
//                                editor.putString("mode", user.type);
//
//
//                                if ( (user.type.equals("staff")) | (user.type.equals("student")) ) {
//                                    editor.putString("first_name", user.first_name);
//                                    editor.putString("last_name", user.last_name);
//                                    intent = new Intent(MainActivity.this, HomeActivity.class);
//
//                                }
//
//
//                                if (user.type.equals("student"))
//                                    editor.putString("matric_no", user.matric);
//
//                                else if (user.type.equals("staff")) {
//                                    editor.putString("office_floor", user.office_floorNo);
//                                    editor.putString("office_identifier", user.office_identifier);
//                                    editor.putString("staff_rank", user.prefixNo);
//                                }
//
//                                else if (user.type.equals("mofficer")){
//                                    editor.putString("mo_first_name", user.first_name);
//                                    editor.putString("mo_last_name", user.last_name);
//                                    editor.putString("mo_phone_number", user.phone_number);
//                                    intent = new Intent(MainActivity.this, MaintenanceDrawer.class);
//
//                                }
//
//                                else if ( (user.type.equals("hos_civil")) | (user.type.equals("hos_electrical")) | (user.type.equals("hos_mechanical")) ) {
//                                    editor.putString("hos_first_name", user.first_name);
//                                    editor.putString("hos_last_name", user.last_name);
//                                    intent = new Intent(MainActivity.this, HeadOfServicesDrawer.class);
//                                }
//
//                                else
//                                    Toast.makeText(MainActivity.this, "Invalid Login Parameters.",
//                                            Toast.LENGTH_SHORT).show();
//
//                                editor.commit();
//
//
//                                progressBar.setVisibility(View.GONE);
//
//
//                                if (intent != null)
//                                    startActivity(intent);
//
//    //                                    Log.v(TAG, "Towoju " + user.type);
//    //                                    Log.v(TAG, "Towoju " + user.getEmail() + user.getFirst_name() + user.getId() + user.getLast_name() +
//    //                                            user.getMatric() + user.getOffice_floorNo() + user.getOffice_identifier() + user.getOffice_location() +
//    //                                            user.getPassword() + user.getPrefix() + user.getPrefixNo() + user.getType());
//                                }
//
//                                @Override
//                                public void onCancelled(DatabaseError databaseError) {
//                                    Log.v(TAG, "Towoju " + databaseError);
//
//                                }
//                            });
////
//
//
//
//                        }
//                        // If sign in fails, display a message to the user. If sign in succeeds
//                        // the auth state listener will be notified and logic to handle the
//                        // signed in user can be handled in the listener.
//                        else {
//                            progressBar.setVisibility(View.GONE);
//                            Toast.makeText(MainActivity.this, "Authentication failed.",
//                                    Toast.LENGTH_SHORT).show();
//                        }
//
//                        // ...
//                    }
//                });


//        progressBar.setProgress(i);
//        mCountDownTimer=new CountDownTimer(2000,1000) {
//
//            @Override
//            public void onTick(long millisUntilFinished) {
//
//                i++;
//                progressBar.setVisibility(View.VISIBLE);
//                progressBar.setProgress((int)i*100/(2000/1000));
//
//            }
//
//            @Override
//            public void onFinish() {
//                progressBar.setVisibility(View.GONE);
//                Intent intent = null;
//                if ((editText.getText().toString().equals("student") && editText2.getText().toString().equals("student"))) {
//                    intent = new Intent(MainActivity.this, HomeActivity.class);
//                    editor.putString("mode", "student");
//                }
//
//                else if ((editText.getText().toString().equals("staff") && editText2.getText().toString().equals("staff"))) {
//                    intent = new Intent(MainActivity.this, HomeActivity.class);
//                    editor.putString("mode", "staff");
//
//                }
//
//                else if ((editText.getText().toString().equals("mo") && editText2.getText().toString().equals("mo"))) {
//                    intent = new Intent(MainActivity.this, MaintenanceDrawer.class);
//                }
//
//                else {
//                    intent = new Intent(MainActivity.this, HeadOfServicesDrawer.class);
//                }
//
//                editor.commit();
//                startActivity(intent);
//
//            }
//        };
//        mCountDownTimer.start();


    public void register(View view) {
        Intent intent = new Intent(this, Register_Launcher.class);
        startActivity(intent);

//        FacilityData facilityData = new FacilityData();
//        Map<String, String> facilityType = facilityData.getFacilityMap();
//        facilityType.get("ff").toString();
    }

    public void hideKeyboardMethod (View view) {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (view != null)
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    @Override
    public void onBackPressed() {

       return;

    }
//    public void mo(View view) {
//        Intent intent = new Intent(this, MaintenanceDrawer.class);
//        startActivity(intent);
//    }
//
//    public void hos(View view) {
//        Intent intent = new Intent(this, HeadOfServicesDrawer.class);
//        startActivity(intent);
//    }


//    public void homepage(View view) {
//        Intent intent = new Intent(this, HomeActivity.class);
//        startActivity(intent);
//    }

//    public void facility(View view) {
//        Intent intent = new Intent(this, FacilityScreen.class);
//        startActivity(intent);
//    }
}







































